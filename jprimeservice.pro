-injars jprime-server.jar 
-outjars jprime-server-pro.jar 
-libraryjars  <java.home>/lib/rt.jar
-libraryjars  lib(**.jar;)
-printmapping jprimeservice.map
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod
-keepdirectories
-keep class ru.navilab.jprime.data.** {
  public protected private *;
}
-keep class ru.navilab.jprime.service.** {
  public protected private *;
}
