import java.io.IOException;
import java.io.InputStream;

public final class MyInputStream extends InputStream {
    private final int count;
    private byte[] bytes;
    private int position;
    private int mark;
    private long fileOffset;

    public MyInputStream(long fileOffset, byte[] bytes) {
        this.fileOffset = fileOffset;
        this.bytes = bytes;
        count = bytes.length;
    }

    @Override
    public int read() throws IOException {
        return (position < count) ? (bytes[position++] & 0xff) : -1;
    }

    @Override
    public boolean markSupported() {
        return true;
    }

    @Override
    public long skip(long n) throws IOException {
        return position += n;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        }

        if (position >= count) {
            return -1;
        }

        int avail = count - position;
        if (len > avail) {
            len = avail;
        }
        if (len <= 0) {
            return 0;
        }
        System.arraycopy(bytes, position, b, off, len);
        position += len;
        return len;
    }

    @Override
    public int available() throws IOException {
        return super.available();
    }

    @Override
    public synchronized void mark(int readlimit) {
       mark = position;
    }

    @Override
    public synchronized void reset() throws IOException {
        position = mark;
    }

    public long getFilePointer() {
        return position + fileOffset;
    }
}
