package ru.navilab.jprime.data;

import java.io.Serializable;

/**
 * Created by Mikhailov_KG on 13.04.2016.
 */
public class WsFile implements IWsFile, Serializable {
    public static final long serialVersionUID = 2L;

    private String file;
    private String section;
    private String date;

    @Override
    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    @Override
    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    @Override
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
