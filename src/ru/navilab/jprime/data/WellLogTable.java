package ru.navilab.jprime.data;

import ru.navilab.matrix.engine.Matrix;

import java.io.Serializable;

/**
 * Created by Mikhailov_KG on 01.03.2016.
 */
public class WellLogTable implements IWellLogTable, Serializable {
    public static final long serialVersionUID = 2L;

    private final String wsfile;
    private final String tableName;
    private final Matrix matrix;
    private ILogProperties properties;

    public WellLogTable(String wsfile, String tableName, Matrix matrix, ILogProperties properties) {
        this.tableName = tableName;
        this.wsfile = wsfile;
        this.matrix = matrix;
        this.properties = properties;
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public String getWsfile() {
        return wsfile;
    }

    @Override
    public Matrix getMatrix() {
        return matrix;
    }

    public ILogProperties getProperties() {
        return properties;
    }
}
