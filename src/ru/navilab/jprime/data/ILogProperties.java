package ru.navilab.jprime.data;

import java.io.Serializable;

/**
 * Created by Mikhailov_KG on 18.04.2016.
 */
public interface ILogProperties extends Serializable {
    String getFieldComment(String fieldName);
    String getFieldUnit(String fieldName);
    ILogProperties getNestedProperties(int rowIndex, String fieldName);
}
