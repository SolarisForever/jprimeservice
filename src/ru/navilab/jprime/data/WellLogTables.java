package ru.navilab.jprime.data;

import ru.navilab.matrix.engine.Matrix;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mikhailov_KG on 01.03.2016.
 */
public class WellLogTables implements IWellLogTables,Serializable {
    public static final long serialVersionUID = 2L;

    final List<IWellLogTable> wellLogTableList = new ArrayList<IWellLogTable>();


    public void addLogTable(String wsfile, String tableName, Matrix matrix, ILogProperties properties) {
        wellLogTableList.add(new WellLogTable(wsfile, tableName, matrix, properties));
    }

    public void addLogTable(IWellLogTable table) {
        wellLogTableList.add(table);
    }

    @Override
    public List<IWellLogTable> getWellLogTableList() {
        return wellLogTableList;
    }
}
