package ru.navilab.jprime.data;

import java.io.Serializable;

/**
 * Created by Mikhailov_KG on 13.04.2016.
 */
public interface IWsFile extends Serializable {
    String getFile();

    String getSection();

    String getDate();
}
