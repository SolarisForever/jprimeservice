package ru.navilab.jprime.data;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by Mikhailov_KG on 18.04.2016.
 */
public class LogProperties implements ILogProperties, Serializable {
    public static final long serialVersionUID = 2L;

    private static final class LogProps implements Serializable {
        String fieldUnit;
        String fieldComment;
    }
    private Map<String,LogProps> propertiesMap = new Hashtable<String, LogProps>();
    private Map<String,ILogProperties> nestedPropMap = new Hashtable<String, ILogProperties>();

    public void addProperties(String fieldName, String fieldUnit, String fieldComment) {
        LogProps logProps = new LogProps();
        logProps.fieldUnit = fieldUnit;
        logProps.fieldComment = fieldComment;
        propertiesMap.put(fieldName, logProps);
    }

    public void addNestedProperties(int rowIndex, String fieldName, ILogProperties logProperties) {
        nestedPropMap.put(fieldName + rowIndex, logProperties);
    }

    public String getFieldComment(String fieldName) {
        LogProps logProps = propertiesMap.get(fieldName);
        return logProps != null ? logProps.fieldComment : null;
    }

    public String getFieldUnit(String fieldName) {
        LogProps logProps = propertiesMap.get(fieldName);
        return logProps != null ? logProps.fieldUnit : null;
    }

    public ILogProperties getNestedProperties(int rowIndex, String fieldName) {
        return nestedPropMap.get(fieldName + rowIndex);
    }
}
