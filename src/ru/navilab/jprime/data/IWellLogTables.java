package ru.navilab.jprime.data;

import ru.navilab.jprime.data.IWellLogTable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mikhailov_KG on 01.03.2016.
 */
public interface IWellLogTables extends Serializable {
    List<IWellLogTable> getWellLogTableList();


}
