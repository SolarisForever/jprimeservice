package ru.navilab.jprime.data;

import ru.navilab.matrix.engine.Matrix;

import java.io.Serializable;

/**
 * Created by Mikhailov_KG on 01.03.2016.
 */
public interface IWellLogTable extends Serializable {
    String getTableName();

    String getWsfile();

    Matrix getMatrix();

    ILogProperties getProperties();
}
