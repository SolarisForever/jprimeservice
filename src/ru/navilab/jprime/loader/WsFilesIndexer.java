package ru.navilab.jprime.loader;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.FloatByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import ru.navilab.common.utils.StopWatch;
import ru.navilab.jprime.service.LogInsertObj;
import ru.navilab.jprime.service.PrimeApi;
import ru.navilab.jprime.service.PrimeReadException;
import ru.navilab.jprime.service.WellPrimeLogCurveInserter;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Mikhailov_KG on 29.06.2015.
 */
public class WsFilesIndexer {
    private final WellPrimeLogCurveInserter inserter;
    FilenameFilter wsFilenameFilter = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.toUpperCase().endsWith(".WS");
        }
    };
    private PrimeApi primeApi;

    public WsFilesIndexer() throws SQLException {
        inserter = new WellPrimeLogCurveInserter();
    }

    public static void main(String[] args) {
        try {
            WsFilesIndexer wsFilesIndexer = new WsFilesIndexer();
            wsFilesIndexer.index(Integer.parseInt(args[0]), args[1]);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void index(int dbId, String dir) throws SQLException, IOException, PrimeReadException {
        StopWatch stopWatch = new StopWatch("all files");
        inserter.setPrimeDatabaseId(dbId);
        primeApi = (PrimeApi) Native.loadLibrary("c:\\prime\\DatServ1", PrimeApi.class);
        File rootDir = new File(dir);
        scanDir(rootDir);
        inserter.close();
        stopWatch.stop();
    }

    private void scanDir(File file) throws SQLException, IOException, PrimeReadException {
        File[] files = file.listFiles(wsFilenameFilter);
        for (File wsFile : files) {
            System.err.println(wsFile.getAbsolutePath());
            String wsFileAbsolutePath = wsFile.getAbsolutePath();
            inserter.delete(wsFileAbsolutePath);
            indexWsFile(wsFileAbsolutePath);
        }
        File[] dirFiles = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return (pathname.isDirectory());
            }
        });
        for (File dirFile : dirFiles) {
            scanDir(dirFile);
        }
    }

    private void indexWsFile(String wsFilename) throws SQLException, IOException, PrimeReadException {
        IntByReference errorCode = new IntByReference();
//        Pointer curveListObj = primeApi.DSFindCurvesMatchingConditionsByWSName(wsFilename,
//                "LAS", "МЕТОД_ГИС=*", 128, errorCode, 0, Pointer.NULL, Pointer.NULL);
//        int count = primeApi.DSListCount(curveListObj);
//        if (count > 0) {
//            for (int i = 0; i < count; i++) {
//                processCurve(primeApi, curveListObj, i, wsFilename);
//            }
//            inserter.executeBatch();
//            inserter.mayCommit();
//        }
//        primeApi.DSDoneObject(curveListObj);
    }

    private void processCurve(PrimeApi primeApi, Pointer curveListObj, int curveIndex, String wsFilename) throws PrimeReadException, IOException, SQLException {
        PointerByReference primeObjectRef = new PointerByReference();
        PointerByReference arrayRef = new PointerByReference();
        IntByReference columnNumberRef = new IntByReference();
        FloatByReference startRef = new FloatByReference();
        FloatByReference stopRef = new FloatByReference();
        FloatByReference stepRef = new FloatByReference();
        IntByReference dataTypeRef = new IntByReference();
        IntByReference versionRef = new IntByReference();
        IntByReference planNamesLen = new IntByReference();
        IntByReference tableNameLen = new IntByReference();
        IntByReference curveNameLen = new IntByReference();
        IntByReference keyStrLen = new IntByReference();
        PointerByReference planNamesRef = new PointerByReference();
        PointerByReference tableNameRef= new PointerByReference();
        PointerByReference curveNameRef= new PointerByReference();
        PointerByReference keyStrRef = new PointerByReference();

        primeApi.DSGetCurveDataFromList(curveListObj, curveIndex, primeObjectRef, arrayRef, columnNumberRef, startRef, stopRef, stepRef, dataTypeRef, versionRef,
                planNamesLen, tableNameLen, curveNameLen, keyStrLen, planNamesRef, tableNameRef, curveNameRef, keyStrRef);

        String curveName = PrimeNativeHelper.getString(curveNameRef);

        int arrayLen = primeApi.ArrayGetLen(arrayRef.getValue());
        float[] floatValues = new float[arrayLen];
        for (int j = 0; j < arrayLen; j++) {
            floatValues[j] = primeApi.ArrayGetSingleColumnData(arrayRef.getValue(), (short) columnNumberRef.getValue(), j);
        }

        inserter.insert(new LogInsertObj(primeApi, primeObjectRef.getValue(), curveName, floatValues, wsFilename, versionRef.getValue(), startRef.getValue(), stopRef.getValue(), stepRef.getValue(), dataTypeRef.getValue()));

        primeApi.DoneHandle(planNamesRef);
        primeApi.DoneHandle(tableNameRef);
        primeApi.DoneHandle(curveNameRef);
        primeApi.DoneHandle(keyStrRef);
        primeApi.DoneHandle(primeObjectRef);
        primeApi.DoneHandle(arrayRef);
    }
}
