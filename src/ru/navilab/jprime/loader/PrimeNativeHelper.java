package ru.navilab.jprime.loader;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * Created by Mikhailov_KG on 17.06.2015.
 */
public class PrimeNativeHelper {
    public static final String CP866 = "CP866";
    public static final int FLOAT_SIZE = 4;


    public static Pointer createPointer(String string, String encoding) {
        byte[] bytes = string.getBytes(Charset.forName(encoding));
        ByteBuffer buffer = ByteBuffer.allocateDirect(bytes.length);
        buffer.put(bytes);
        return Native.getDirectBufferPointer(buffer);
    }

    public static Pointer createPointer(String string) {
        byte[] bytes = string.getBytes();
        ByteBuffer buffer = ByteBuffer.allocateDirect(bytes.length);
        buffer.put(bytes);
        return Native.getDirectBufferPointer(buffer);
    }

    public static Pointer createPointer(int size) {
        return Native.getDirectBufferPointer(ByteBuffer.allocateDirect(size));
    }

    public static String getString(PointerByReference pointerByReference) {
        Pointer pvalue = pointerByReference.getValue();
        if (pvalue == null) return null;
        return pvalue.getString(0);
    }

    public static byte[] extractZeroTerminatedArray(byte[] byteArray) {
        for (int i = 0; i < byteArray.length; i++) {
            if (byteArray[i] == 0) return Arrays.copyOf(byteArray, i);
        }
        return byteArray;
    }
}
