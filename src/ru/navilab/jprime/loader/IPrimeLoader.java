package ru.navilab.jprime.loader;

import ru.navilab.jprime.data.WellLogTables;
import ru.navilab.jprime.service.PrimeReadException;
import ru.navilab.matrix.engine.Matrix;

import java.io.IOException;
import java.sql.SQLException;

public interface IPrimeLoader {
    WellLogTables load(String wsFilename) throws PrimeReadException, SQLException, IOException;
}
