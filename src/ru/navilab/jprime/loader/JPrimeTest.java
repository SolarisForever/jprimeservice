package ru.navilab.jprime.loader;


import com.sun.jna.*;
import com.sun.jna.ptr.FloatByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import ru.navilab.jprime.service.JDBCDeleteHelper;
import ru.navilab.jprime.service.PrimeApi;
import ru.navilab.jprime.service.PrimeReadException;

import java.io.*;
import java.sql.*;
import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by Mikhailov_KG on 11.06.2015.
 */
public class JPrimeTest {
    public static final String NIPI42_WELL_PRIME_LOG_CURVE = "NIPI42.WELL_PRIME_LOG_CURVE";
    private final Connection connection;
    private final PreparedStatement pstmt;
    private final int primeDatabaseId;
    private int index;
    static FilenameFilter wsFilenameFilter = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.toUpperCase().endsWith(".WS");
        }
    };
    private PrimeApi primeApi;
    private JDBCDeleteHelper deleteHelper;


    JPrimeTest() throws SQLException {
        GeoproConnection geoproConnection = new GeoproConnection();
        connection = geoproConnection.createConnection();
        connection.setAutoCommit(false);
        pstmt = connection.prepareStatement(String.format("INSERT INTO %s" +
                "(section, uwi, trace_type, top, base, lasname, prime_db, " +
                "curve_data_count, curve_data, version, frac_offset, " +
                "direction, operation_number, step, operator, well_name, " +
                "null_value, log_date, log_time, activity_id, quality, wsfile) VALUES "+
                "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", NIPI42_WELL_PRIME_LOG_CURVE));

        primeDatabaseId = Integer.parseInt(System.getProperty("prime_db", "2998301"));
        deleteHelper = new JDBCDeleteHelper(connection);
    }

    public static void main(String[] args) throws Throwable {
        JPrimeTest test = new JPrimeTest();
        test.test();
        test.close();
    }

    private void close() throws SQLException {
        connection.commit();
        pstmt.close();
        connection.close();
    }


    private void test() throws PrimeReadException, SQLException, IOException {
        long startTime = System.currentTimeMillis();
        primeApi = (PrimeApi) Native.loadLibrary("c:\\prime\\DatServ1", PrimeApi.class);
//        File rootDir = new File("c:\\PrimeDB\\Finder1\\Ай-пимское\\313P\\40001123");
//        scanDir(rootDir);
        String wsFilename = "C:\\PrimeDB\\LogDB1\\Ай-Пимское\\1011\\40037507\\LOG\\2013-07-16\\40037507_16.07.2013_LOG.ws";
        testWsFile(primeApi, wsFilename);
        long elapsed = System.currentTimeMillis() - startTime;
        System.err.println("elapsed " + elapsed + " " + elapsed / 1000);
    }

    private void scanDir(File file) throws PrimeReadException, SQLException, IOException {
        File[] files = file.listFiles(wsFilenameFilter);
        for (File wsFile : files) {
            System.err.println(wsFile.getAbsolutePath());
            String absolutePath = wsFile.getAbsolutePath();
            deleteHelper.delete(absolutePath);
            testWsFile(primeApi, absolutePath);
        }
        File[] dirFiles = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return (pathname.isDirectory());
            }
        });
        for (File dirFile : dirFiles) {
            scanDir(dirFile);
        }
    }

    private void testWsFile(PrimeApi primeApi, String wsFilename) throws PrimeReadException, SQLException, IOException {
        IntByReference errorCode = new IntByReference();
//        Pointer curveListObj = primeApi.DSFindCurvesMatchingConditionsByWSName(wsFilename,
//                "LAS", "МЕТОД_ГИС=*", 128, errorCode, 0, Pointer.NULL, Pointer.NULL);
////        System.err.println("curveListObj " + curveListObj + " " + errorCode.getValue());
//
//        int count = primeApi.DSListCount(curveListObj);
//
//        System.err.println("count " + count);
//
//        if (count > 0) {
//            for (int i = 0; i < count; i++) {
//                processCurve(primeApi, curveListObj, i, wsFilename);
//            }
////            pstmt.executeBatch();
////            if ((index++ % 5) == 0) connection.commit();
//        }
//        primeApi.DSDoneObject(curveListObj);
    }

    private void processCurve(PrimeApi primeApi, Pointer plist, int curveIndex, String wsFilename) throws PrimeReadException, SQLException, IOException {
        PointerByReference primeObjectRef = new PointerByReference();
        PointerByReference arrayRef = new PointerByReference();
        IntByReference columnNumberRef = new IntByReference();
        FloatByReference startRef = new FloatByReference();
        FloatByReference stopRef = new FloatByReference();
        FloatByReference stepRef = new FloatByReference();
        IntByReference dataTypeRef = new IntByReference();
        IntByReference versionRef = new IntByReference();
        IntByReference planNamesLen = new IntByReference();
        IntByReference tableNameLen = new IntByReference();
        IntByReference curveNameLen = new IntByReference();
        IntByReference keyStrLen = new IntByReference();
        PointerByReference planNamesRef = new PointerByReference();
        PointerByReference tableNameRef= new PointerByReference();
        PointerByReference curveNameRef= new PointerByReference();
        PointerByReference keyStrRef = new PointerByReference();

        primeApi.DSGetCurveDataFromList(plist, curveIndex, primeObjectRef, arrayRef, columnNumberRef, startRef, stopRef, stepRef, dataTypeRef, versionRef,
                planNamesLen, tableNameLen, curveNameLen, keyStrLen, planNamesRef, tableNameRef, curveNameRef, keyStrRef);

        String planName = getString(planNamesRef);
        String tableName = getString(tableNameRef);
        String curveName = getString(curveNameRef);
        String keyStr = getString(keyStrRef);

        int arrayLen = primeApi.ArrayGetLen(arrayRef.getValue());
        float[] floatValues = new float[arrayLen];
        for (int j = 0; j < arrayLen; j++) {
            floatValues[j] = primeApi.ArrayGetSingleColumnData(arrayRef.getValue(), (short) columnNumberRef.getValue(), j);
        }

        test(primeApi, primeObjectRef.getValue());

        System.err.println(String.format("%s\t%s\t%s\t%s\t%s\tcol=\t%d\t%s\t%s", planName, tableName, curveName, keyStr,
                arrayLen, columnNumberRef.getValue(), primeObjectRef.getValue(), stopRef.getValue()));

//        insert(primeApi, primeObjectRef.getValue(), curveName, floatValues, wsFilename, versionRef, startRef.getValue(), stopRef.getValue(), stepRef.getValue(), dataTypeRef.getValue());

        primeApi.DoneHandle(planNamesRef);
        primeApi.DoneHandle(tableNameRef);
        primeApi.DoneHandle(curveNameRef);
        primeApi.DoneHandle(keyStrRef);
        primeApi.DoneHandle(primeObjectRef);
        primeApi.DoneHandle(arrayRef);
    }

    private void insert(PrimeApi primeApi, Pointer primeObject, String curveName, float[] floatValues, String wsFilename, IntByReference versionRef, float startValue, float stopValue, float stepValue, int dataTypeValue) throws SQLException, PrimeReadException, IOException {
        String section = PrimeFieldExtractor.getString(primeApi, primeObject, "РАЗДЕЛ");
        setString(1, section == null ? "LOG" : section);
        pstmt.setString(2, PrimeFieldExtractor.getString(primeApi, primeObject, "UWI"));
        pstmt.setString(3, curveName);
        pstmt.setFloat(4, startValue);
        pstmt.setFloat(5, stopValue);
        pstmt.setString(6, PrimeFieldExtractor.getString(primeApi, primeObject, "ИМЯ_ФАЙЛА"));
        pstmt.setInt(7, primeDatabaseId); // Finder1
        pstmt.setInt(8, floatValues.length);
        byte[] bytes = createBytesArray(floatValues);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        pstmt.setBinaryStream(9, inputStream);

        pstmt.setInt(10, versionRef.getValue());
        setFloat(11, PrimeFieldExtractor.getFloat(primeApi, primeObject, "ГРП_СМЕЩЕНИЕ"));
        setString(12, PrimeFieldExtractor.getString(primeApi, primeObject, "НАПРАВЛЕНИЕ_ЗАМЕРА"));
        setLong(13, PrimeFieldExtractor.getLong(primeApi, primeObject, "НОМЕР_С/П"));
        setFloat(14, stepValue); // ШАГ_ДИСКРЕТ_КРИВОЙ
        setString(15, PrimeFieldExtractor.getString(primeApi, primeObject, "ПОДРЯДЧИК"));
        setString(16, PrimeFieldExtractor.getString(primeApi, primeObject, "СКВАЖИНА"));
        String nullValueString = PrimeFieldExtractor.getString(primeApi, primeObject, "NULL");
        pstmt.setFloat(17, Float.parseFloat(nullValueString));
        pstmt.setDate(18, toDate(PrimeFieldExtractor.getDate(primeApi, primeObject, "ДАТА_ИССЛЕДОВАНИЯ")));
        pstmt.setTimestamp(19, toTimestamp(PrimeFieldExtractor.getTime(primeApi, primeObject, "ВРЕМЯ_НАЧАЛА")));
        setLong(20, PrimeFieldExtractor.getLong(primeApi, primeObject, "ACTIVITY_ID"));
        setString(21, PrimeFieldExtractor.getString(primeApi, primeObject, "КАЧЕСТВО_ДАННЫХ"));
        setString(22, wsFilename);

        pstmt.addBatch();

        inputStream.close();
    }

    private Timestamp toTimestamp(java.util.Date utilTime) {
        if (utilTime == null) return null;
        else return new Timestamp(utilTime.getTime());
    }

    private void setString(int i, String s) throws SQLException {
        if (s != null) pstmt.setString(i, s);
        else pstmt.setNull(i, Types.NULL);
    }

    private void setFloat(int i, Float v) throws SQLException {
        if (v != null) pstmt.setFloat(i, v);
        else pstmt.setNull(i, Types.NULL);
    }

    private void setLong(int i, Long v) throws SQLException {
        if (v != null) pstmt.setLong(i, v);
        else pstmt.setNull(i, Types.NULL);
    }

    private Time toTime(java.util.Date utilTime) {
        if (utilTime == null) return null;
        else return new Time(utilTime.getTime());
    }

    private Date toDate(java.util.Date utilDate) {
        return new Date(utilDate.getTime());
    }

    private byte[] createBytesArray(float[] values) throws IOException {
        ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(byteArrayStream);
        for (int i = 0; i < values.length; i++) out.writeFloat(values[i]);
        out.close();
        byte[] bytes = byteArrayStream.toByteArray();
        byteArrayStream.close();
        return bytes;
    }

    static void test(PrimeApi primeApi, Pointer primeObject) throws PrimeReadException {
        outStringField(primeApi, primeObject, "ПУТЬ_ФАЙЛА");
        outStringField(primeApi, primeObject, "ИМЯ_ФАЙЛА");
        outLongField(primeApi, primeObject, "НОМЕР_ВЕРСИИ");
        outFloatField(primeApi, primeObject, "ГРП_СМЕЩЕНИЕ");
        outStringField(primeApi, primeObject, "НАПРАВЛЕНИЕ_ЗАМЕРА");
        outLongField(primeApi, primeObject, "НОМЕР_С/П");
        outFloatField(primeApi, primeObject, "КРОВЛЯ_КРИВОЙ_ГИС");
        outFloatField(primeApi, primeObject, "ПОДОШВА_КРИВОЙ_ГИС");
        outFloatField(primeApi, primeObject, "ШАГ_ДИСКРЕТ_КРИВОЙ");
        outStringField(primeApi, primeObject, "ПОДРЯДЧИК");
        outStringField(primeApi, primeObject, "МЕСТОРОЖДЕНИЕ");
        outLongField(primeApi, primeObject, "КУСТ");
        outStringField(primeApi, primeObject, "СКВАЖИНА");
        outStringField(primeApi, primeObject, "NULL");
        outDateField(primeApi, primeObject, "ДАТА_ИССЛЕДОВАНИЯ");
        outTimeField(primeApi, primeObject, "ВРЕМЯ_НАЧАЛА");
        outStringField(primeApi, primeObject, "LOCATION");

        outStringField(primeApi, primeObject, "ЛИЦЕНЗ_УЧАСТОК");
        outStringField(primeApi, primeObject, "SRV");
        outStringField(primeApi, primeObject, "UWI");
        outStringField(primeApi, primeObject, "НАЧАЛЬНИК_ПАРТИИ");
        outStringField(primeApi, primeObject, "PRTY");
        outStringField(primeApi, primeObject, "РАЗДЕЛ");
        outLongField(primeApi, primeObject, "ACTIVITY_ID");
        outStringField(primeApi, primeObject, "КАЧЕСТВО_ДАННЫХ");
    }

    private static void outStringField(PrimeApi primeApi, Pointer primeObject, String fieldName) throws PrimeReadException {
        System.err.println(fieldName + "=" + new PrimeFieldExtractor(primeApi, primeObject, fieldName).getString().trim());
    }

    private static void outLongField(PrimeApi primeApi, Pointer primeObject, String fieldName) throws PrimeReadException {
        System.err.println(fieldName + "=" + new PrimeFieldExtractor(primeApi, primeObject, fieldName).getLong());
    }

    private static void outFloatField(PrimeApi primeApi, Pointer primeObject, String fieldName) throws PrimeReadException {
        System.err.println(fieldName + "=" + new PrimeFieldExtractor(primeApi, primeObject, fieldName).getFloat());
    }

    private static void outDateField(PrimeApi primeApi, Pointer primeObject, String fieldName) throws PrimeReadException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        System.err.println(fieldName + "=" + dateFormat.format(new PrimeFieldExtractor(primeApi, primeObject, fieldName).getDate()));
    }

    private static void outTimeField(PrimeApi primeApi, Pointer primeObject, String fieldName) throws PrimeReadException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
        java.util.Date time = new PrimeFieldExtractor(primeApi, primeObject, fieldName).getTime();

        System.err.println(fieldName + "=" + (time != null ? dateFormat.format(time) : "null"));
    }


    static String getString(PointerByReference pointerByReference) {
        Pointer pvalue = pointerByReference.getValue();
        if (pvalue == null) return null;
        return pvalue.getString(0);
    }
}
