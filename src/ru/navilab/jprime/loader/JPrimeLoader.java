package ru.navilab.jprime.loader;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.ptr.ShortByReference;
import ru.navilab.jprime.data.LogProperties;
import ru.navilab.jprime.data.WellLogTables;
import ru.navilab.jprime.loader.v2.PrimeConstants;
import ru.navilab.jprime.service.PFDData;
import ru.navilab.jprime.service.PrimeApi;
import ru.navilab.jprime.service.PrimeFieldMetaInfo;
import ru.navilab.jprime.service.PrimeReadException;
import ru.navilab.matrix.engine.*;
import ru.navilab.matrix.entity.MatrixEntity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import static ru.navilab.jprime.loader.PrimeFieldExtractor.*;

/**
 * Created by Mikhailov_KG on 15.01.2016.
 */
public class JPrimeLoader implements IPrimeLoader {
    public static final int MAX_ARRAY_SIZE = 0xF000;
    private final PrimeApi primeApi;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    public JPrimeLoader() {
        primeApi = (PrimeApi) Native.loadLibrary("DatServ1", PrimeApi.class);
    }

    @Override
    public synchronized WellLogTables load(String wsFilename) throws PrimeReadException, SQLException, IOException {
        try {
            return preLoad(wsFilename);
        } catch (java.lang.Error e) {
            logger.severe("GOT EXCEPTION " + e.getMessage() + " TRY CONTINUE");
            return preLoad(wsFilename);
        }
    }

    private synchronized WellLogTables preLoad(String wsFilename) throws PrimeReadException {
        PointerByReference lfHandle = new PointerByReference();
        short status = primeApi.SysLFInitOpen(wsFilename, lfHandle);
        if (status != 0) throw new PrimeReadException("can't open " + wsFilename + " " + status);
        Matrix m = createEmptyMatrix();
        WellLogTables wellLogTables = new WellLogTables();
        try {
            List<String> tableList = getTableList(lfHandle);
            if (tableList.contains(PrimeConstants.LAS_TABLE)) {
                LogProperties properties = new LogProperties();
                Matrix lasTable = beginLoadTable(lfHandle, PrimeConstants.LAS_TABLE, properties);
                wellLogTables.addLogTable(wsFilename, PrimeConstants.LAS_TABLE, lasTable, properties);
            } else {
                wellLogTables.addLogTable(wsFilename, PrimeConstants.LAS_TABLE, m, new LogProperties());
            }
            if (tableList.contains(PrimeConstants.ARMG_TABLE)) {
                LogProperties properties = new LogProperties();
                Matrix table = beginLoadTable(lfHandle, PrimeConstants.ARMG_TABLE, properties);
                wellLogTables.addLogTable(wsFilename, PrimeConstants.ARMG_TABLE, table, properties);
            }
            if (tableList.contains(PrimeConstants.DESCRIPTION_TABLE)) {
                LogProperties properties = new LogProperties();
                Matrix desriptionTable = beginLoadTable(lfHandle, PrimeConstants.DESCRIPTION_TABLE, properties);
                wellLogTables.addLogTable(wsFilename, PrimeConstants.DESCRIPTION_TABLE, desriptionTable, properties);
            }
        } finally {
            primeApi.DoneHandle(lfHandle);
        }
        return wellLogTables;
    }

    private List<String> getTableList(PointerByReference lfHandle) {
        PointerByReference tablesNamesRef = new PointerByReference();
        int sz = primeApi.DSGetTableNames(lfHandle.getValue(), (short) 0x1, tablesNamesRef);
        List<String> list = new ArrayList<String>();
        try {
            byte[] byteArray = tablesNamesRef.getValue().getByteArray(0, sz);
            byte[] stringByteArray = PrimeNativeHelper.extractZeroTerminatedArray(byteArray);
            String tableNamesString = new String(stringByteArray, PrimeNativeHelper.CP866);
            logger.finest("DSGetTableNames=" + tableNamesString);
            String[] tableNamesArr = tableNamesString.split("\r");
            for (String t : tableNamesArr) {
                logger.finer("table " + t);
                list.add(t);
            }
            return list;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } finally {
//            primeApi.DoneHandle(tablesNamesRef);
        }
    }

    private Matrix beginLoadTable(PointerByReference lfHandle, String tableName, LogProperties properties) throws PrimeReadException {
        PointerByReference lrHandle = new PointerByReference();

        Pointer tableNamePtr = (tableName == PrimeConstants.ARMG_TABLE) ? getDosString(tableName) : PrimeNativeHelper.createPointer(tableName);
        short status = primeApi.Get_Table(tableNamePtr, lfHandle.getValue(), lrHandle);

        if (status != 0) {
            logger.info("could not open " + tableName + " table " + status);
            return createEmptyMatrix();
        }
        try {
            return loadTable(lrHandle, tableName, properties);
        } finally {
            primeApi.DoneHandle(lrHandle);
        }
    }

    private Matrix createEmptyMatrix() {
        return new Matrix(MatrixEntity.NULL, 0);
    }

    private Matrix loadTable(PointerByReference lrHandle, String tableName, LogProperties properties) throws PrimeReadException {
        List<PrimeFieldMetaInfo> fieldMetaInfoList = getFieldList(primeApi, lrHandle);
        MatrixEntity entity = createEntity(tableName, fieldMetaInfoList);
        Matrix matrix = entity.createMatrix();
        short status = primeApi.Goto_BeginObj(lrHandle.getValue());
        // на некоторых скважинах сам Прайм не открывает LAS таблицу.
        // Сервис здесь давал 204 ошибку на этом методе:
        if (status == 204) return matrix;
        if (status != 0) throw new PrimeReadException("err Goto_FastBeginObj " + status);
        short err = 0;
        do {
            PointerByReference objectHandle = new PointerByReference();
            status = primeApi.GetCurObj(lrHandle.getValue(), objectHandle);
            if (status != 0) throw new PrimeReadException("err GetFastCurObj=" + status);
            try {
                loadMatrix(objectHandle, fieldMetaInfoList, matrix, properties);
            } finally {
                primeApi.DoneHandle(objectHandle);
            }
        } while((err = primeApi.Goto_NextObj(lrHandle.getValue())) == 0);
        return matrix;
    }

    private void loadMatrix(PointerByReference objectHandle, List<PrimeFieldMetaInfo> fieldMetaInfoList, Matrix matrix, LogProperties properties) throws PrimeReadException {
        MatrixRow row = matrix.newRow();
        MatrixEntity entity = matrix.getEntity();
        for (short i = 0; i < fieldMetaInfoList.size(); i++) {
            PrimeFieldMetaInfo fieldMetaInfo = fieldMetaInfoList.get(i);
            String fieldName = fieldMetaInfo.getFieldName();
            properties.addProperties(fieldName, fieldMetaInfo.getFieldUnit(), fieldMetaInfo.getFieldComment());
            if (entity.getField(fieldName) != null) {
                loadField(objectHandle, i, fieldMetaInfo, row, properties);
            }
        }
    }

    private void loadField(PointerByReference objectHandle, short index, PrimeFieldMetaInfo fieldMetaInfo,
                           MatrixRow row, LogProperties properties) throws PrimeReadException {
        byte type = fieldMetaInfo.getType();
        if (isSimpleValueType(type)) {
            loadSimple(objectHandle, index, fieldMetaInfo, row);
        } else {
            if (type == TYPE_ARRAY) {
                LogProperties arrayProperties = new LogProperties();
                properties.addNestedProperties(row.getRowIndex(), fieldMetaInfo.getFieldName(), arrayProperties);
                loadArray(objectHandle, index, fieldMetaInfo, row, arrayProperties);
            }
        }
    }

    private void loadArray(PointerByReference objectHandle, short index, PrimeFieldMetaInfo fieldMetaInfo, MatrixRow row,
                           LogProperties properties) throws PrimeReadException {
        PointerByReference arrHandle = new PointerByReference();
        short err = primeApi.GetArrayByIndex(arrHandle, index, objectHandle.getValue());
        if (err != 0) throw new PrimeReadException("GetArrayByIndex=" + err);
        try {
            ShortByReference columnCountRef = new ShortByReference();
            err = primeApi.ArrayGetColumns(arrHandle.getValue(), columnCountRef);
            if (err != 0) throw new PrimeReadException("ArrayGetColumns=" + err);

            short arrColumnCount = columnCountRef.getValue();
            int arrLength = primeApi.ArrayGetLen(arrHandle.getValue());
            logger.finer("found array rows=" + arrLength + " cols=" + arrColumnCount);
            List<PrimeFieldMetaInfo> fieldList = createFieldList(arrHandle.getValue(), arrColumnCount);
            List<MatrixBuilderMeta> matrixBuilderMetaList = new ArrayList<MatrixBuilderMeta>();
            if (arrLength > 0) {
                for (PrimeFieldMetaInfo metaInfo : fieldList) {
                    String fieldName = metaInfo.getFieldName();
                    properties.addProperties(fieldName, metaInfo.getFieldUnit(), metaInfo.getFieldComment());
                    int i = metaInfo.getIndex();
                    short columnNumber = (short) (i + 1);
                    switch (metaInfo.getType()) {
                        case PrimeFieldExtractor.TYPE_FLOAT:
                            float[] floats = getBigFloatArray(arrHandle.getValue(), columnNumber, arrLength, this.primeApi);
                            matrixBuilderMetaList.add(createFloatMeta(metaInfo.getFieldName(), floats));
                            break;
                        case PrimeFieldExtractor.TYPE_STRING:
                            String[] strings = getStringArrayValues(arrHandle.getValue(), columnNumber, arrLength);
                            matrixBuilderMetaList.add(createStringMeta(metaInfo.getFieldName(), strings));
                            break;
                    }
                }
            }
            Matrix arrayMatrix = createArrayMatrix(fieldMetaInfo, fieldList, matrixBuilderMetaList);
            row.getCell(fieldMetaInfo.getFieldName()).setMatrixObject(arrayMatrix);
        } finally {
            primeApi.DoneHandle(arrHandle);
        }
    }

    private Matrix createArrayMatrix(PrimeFieldMetaInfo fieldMetaInfo, List<PrimeFieldMetaInfo> fieldList, List<MatrixBuilderMeta> matrixBuilderMetaList) {
        String entityName = "ArrayOf" + fieldMetaInfo.getFieldName();
        if (matrixBuilderMetaList.size() > 0) {
            return MatrixBuilder.buildMatrix(entityName, matrixBuilderMetaList);
        }
        else { // create empty matrix
            MatrixEntity matrixEntity = new MatrixEntity(entityName);
            for (PrimeFieldMetaInfo info : fieldList) {
                MatrixColumnType mtype = getMatrixFieldType(info.getType());
                if (mtype != null) matrixEntity.addField(info.getFieldName(), mtype);
            }
            return matrixEntity.createMatrix();
        }

    }

    private MatrixBuilderMeta createStringMeta(String fieldName, String[] strings) {
        boolean[] notNulls = new boolean[strings.length];
        Arrays.fill(notNulls, true);
        return new MatrixBuilderMeta(fieldName, MatrixColumnType.STRING, strings, notNulls, strings.length);
    }

    private MatrixBuilderMeta createFloatMeta(String fieldName, float[] floats) {
        boolean[] notNulls = new boolean[floats.length];
        Arrays.fill(notNulls, true);
        return new MatrixBuilderMeta(fieldName, MatrixColumnType.FLOAT, floats, notNulls, floats.length);
    }

    private String[] getStringArrayValues(Pointer arrHandleValue, short columnNumber, int arrLength) throws PrimeReadException {
        String[] arr = new String[arrLength];
        Pointer stringPointer = PrimeNativeHelper.createPointer(255);
        for (int i = 0; i < arrLength; i++) {
            //logger.finest("ArrayGetStrColumnData col i " + columnNumber + " " + i);
            short status = primeApi.ArrayGetStrColumnData(arrHandleValue, (byte) columnNumber, i, stringPointer);
            if (status != 0) throw new PrimeReadException("ArrayGetStrColumnData(" + columnNumber + ")");
            arr[i] = stringPointer.getString(0, PrimeNativeHelper.CP866);
        }
        return arr;
    }

    static final float[] getBigFloatArray(Pointer arrHandleValue, short columnNumber, int arrLength, PrimeApi primeApi) {
        int requestLength = Math.min(MAX_ARRAY_SIZE, arrLength);
        float[] arr = new float[arrLength];
        for (int offset=0; offset < arrLength; ) {
            //System.err.print(String.format(" req %d %d %d", offset, requestLength, arrLength));
            int size = requestLength * PrimeNativeHelper.FLOAT_SIZE;
            Pointer buffer = PrimeNativeHelper.createPointer(size);
            primeApi.ArrayGetSingleColData(arrHandleValue, columnNumber, offset, requestLength, buffer);
            float[] floatArray = buffer.getFloatArray(0, requestLength);
            System.arraycopy(floatArray, 0, arr, offset, requestLength);
            offset += requestLength;
            if ((offset + requestLength) > arrLength) requestLength = arrLength - offset;
        }
        return arr;
    }

    private float[] getSmallFloatArray(Pointer arrHandleValue, short columnNumber, int arrLength) {
        int size = arrLength * PrimeNativeHelper.FLOAT_SIZE * 2;
        Pointer buffer = PrimeNativeHelper.createPointer(size);
        primeApi.ArrayGetSingleColData2(arrHandleValue, columnNumber, 0, (short) arrLength, buffer);
        return buffer.getFloatArray(0, arrLength);
    }

    private List<PrimeFieldMetaInfo> createFieldList(Pointer arrHandleValue, short arrColumnCount) throws PrimeReadException {
        List<PrimeFieldMetaInfo> r = new ArrayList<PrimeFieldMetaInfo>();
        for (short i = 0; i < arrColumnCount; i++) {
            Pointer pfdData = PrimeNativeHelper.createPointer(1024);
            short err = primeApi.GetTDescFldsI(arrHandleValue, i, pfdData);
            if (err != 0) throw new PrimeReadException("GetTDescFldsI=" + err);
            PFDData pfd = new PFDData(pfdData);
            String fieldName = pfd.getFieldName();
            byte type = pfd.getType();
            MatrixColumnType matrixColumnType = getMatrixArrayColumnType(type);
            if (matrixColumnType != null) {
                logger.finer("create array matrix column " + fieldName + " " + type + " " + pfd.getFieldUnit() +
                        " " + pfd.getFieldComment());
                r.add(new PrimeFieldMetaInfo(i, fieldName, type, pfd.getFieldUnit(), pfd.getFieldComment()));
            }
        }
        return r;
    }

    private MatrixColumnType getMatrixArrayColumnType(byte type) {
        switch (type) {
            case TYPE_STRING:
                return MatrixColumnType.STRING;
            case TYPE_FLOAT:
                return MatrixColumnType.FLOAT;
        }
        return null;
    }


    private void loadSimple(PointerByReference objectHandle, short index, PrimeFieldMetaInfo metaInfo, MatrixRow row) throws PrimeReadException {
        PrimeFieldExtractor extractor = null;
        try {
            byte type = metaInfo.getType();
            String fieldName = metaInfo.getFieldName();
            logger.finest("set value for matrix column '" + fieldName + "'");
            extractor = new PrimeFieldExtractor(primeApi, objectHandle.getValue(), index, fieldName, type);
            switch (type) {
                case TYPE_STRING:
                    row.setString(fieldName, extractor.getString()); break;
                case TYPE_DATE:
                    row.setDate(fieldName, extractor.getDate()); break;
                case TYPE_FLOAT:
                    row.setFloat(fieldName, extractor.getFloat()); break;
                case TYPE_LONGINT:
                    row.setLong(fieldName, extractor.getLong()); break;
                case TYPE_TIME:
                    row.setDate(fieldName, extractor.getTime()); break;
            }
        } finally {
            if (extractor != null) extractor.close();
        }
    }

    private boolean isSimpleValueType(byte type) {
        return type == TYPE_STRING || type == TYPE_DATE ||
                type == TYPE_FLOAT || type == TYPE_LONGINT || type == TYPE_TIME;
    }

    private MatrixEntity createEntity(String tableName, List<PrimeFieldMetaInfo> fieldMetaInfoList) {
        MatrixEntity e = new MatrixEntity(tableName);
        for (PrimeFieldMetaInfo field : fieldMetaInfoList) {
            String name = field.getFieldName();
            logger.finer("found prime field name '" + name + "'");
            MatrixColumnType mType = getMatrixFieldType(field.getType());
            if (mType != null) e.addField(name, mType);
        }
        return e;
    }

    private MatrixColumnType getMatrixFieldType(byte primeFieldType) {
        switch (primeFieldType) {
            case TYPE_STRING:
                return MatrixColumnType.STRING;
            case TYPE_DATE:
            case TYPE_TIME:
                return MatrixColumnType.DATE;
            case TYPE_FLOAT:
                return MatrixColumnType.FLOAT;
            case TYPE_LONGINT:
                return MatrixColumnType.LONG;
            case TYPE_ARRAY:
                return MatrixColumnType.OBJECT;
        }
        return null;
    }

    static Pointer getDosString(String dosString) {
        return PrimeNativeHelper.createPointer(dosString, PrimeNativeHelper.CP866);
    }

    static List<PrimeFieldMetaInfo> getFieldList(PrimeApi primeApi, PointerByReference lrHandle) {
        List<PrimeFieldMetaInfo> r = new ArrayList<PrimeFieldMetaInfo>();
        ShortByReference counterRef = new ShortByReference();
        short err0 = primeApi.GetTDescCounter(lrHandle.getValue(), counterRef);
        short counter = counterRef.getValue();
//        System.err.println("**GetTDescCounter=" + err0 + " " + counter);

        for (short i = 0; i < counter; i++) {
            Pointer pfdData = PrimeNativeHelper.createPointer(1024);
            short err = primeApi.GetTDescFldsI(lrHandle.getValue(), i, pfdData);
//            System.err.print(i + " **GetTDescFldsI=" + err);
            PFDData pfd = new PFDData(pfdData);
            String fieldName = pfd.getFieldName();
//            System.err.print(" **fieldName=" + fieldName);
            byte type = pfd.getType();
//            System.err.println(" **type=" + type);
            r.add(new PrimeFieldMetaInfo(i, fieldName, type, pfd.getFieldUnit(), pfd.getFieldComment()));
        }
//        System.err.println();
        return r;
    }
}
