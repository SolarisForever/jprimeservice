package ru.navilab.jprime.loader;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.ByteByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import ru.navilab.jprime.service.PrimeApi;
import ru.navilab.jprime.service.PrimeReadException;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Mikhailov_KG on 17.06.2015.
 */
public class PrimeFieldExtractor {
    public static final byte TYPE_LONGINT = 4;
    public static final byte TYPE_FLOAT = 5;
    public static final byte TYPE_DOUBLE = 6;
    public static final byte TYPE_STRING = 7;
    public static final byte TYPE_DATE = 8;
    public static final byte TYPE_TIME = 9;
    public static final byte TYPE_ARRAY = 10;
    public static final byte TYPE_MEMO = 19;
    private final short status;
    private final byte type;
    private final PointerByReference valueRef;
    private int size;
    private PrimeApi primeApi;
    private String fieldName;

    public PrimeFieldExtractor(PrimeApi primeApi, Pointer primeObject, String fieldName) throws PrimeReadException {
        this.primeApi = primeApi;
        this.fieldName = fieldName;
        valueRef = new PointerByReference();
        ByteByReference typeRef = new ByteByReference();
        Pointer fieldNamePtr = PrimeNativeHelper.createPointer(fieldName, PrimeNativeHelper.CP866);
        status = primeApi.GetFieldVal(fieldNamePtr, primeObject, valueRef, typeRef);
        type = typeRef.getValue();
        checkException();
        if (status != -3) size = getSize();
    }

    public PrimeFieldExtractor(PrimeApi primeApi, Pointer primeObject, short index, String fieldName, byte type) throws PrimeReadException {
        this.primeApi = primeApi;
        this.fieldName = fieldName;
        valueRef = new PointerByReference();
        status = primeApi.GetObValByIndex(index, primeObject, valueRef);
        this.type = type;
        checkException();
        if (status != -3) size = getSize();
    }

    public static String getString(PrimeApi primeApi, Pointer primeObject, String fieldName) throws PrimeReadException {
        PrimeFieldExtractor e = null;
        try {
            e = new PrimeFieldExtractor(primeApi, primeObject, fieldName);
            return e.getString();
        } finally {
            if (e != null) e.close();
        }
    }

    public static Long getLong(PrimeApi primeApi, Pointer primeObject, String fieldName) throws PrimeReadException {
        PrimeFieldExtractor e = null;
        try {
            e = new PrimeFieldExtractor(primeApi, primeObject, fieldName);
            return e.getLong();
        } finally {
            if (e != null) e.close();
        }
    }

    public static Float getFloat(PrimeApi primeApi, Pointer primeObject, String fieldName) throws PrimeReadException {
        PrimeFieldExtractor e = null;
        try {
            e = new PrimeFieldExtractor(primeApi, primeObject, fieldName);
            return e.getFloat();
        } finally {
            if (e != null) e.close();
        }
    }

    public static Date getDate(PrimeApi primeApi, Pointer primeObject, String fieldName) throws PrimeReadException {
        PrimeFieldExtractor e = null;
        try {
            e = new PrimeFieldExtractor(primeApi, primeObject, fieldName);
            return e.getDate();
        } finally {
            if (e != null) e.close();
        }
    }

    public static Date getTime(PrimeApi primeApi, Pointer primeObject, String fieldName) throws PrimeReadException {
        PrimeFieldExtractor e = null;
        try {
            e = new PrimeFieldExtractor(primeApi, primeObject, fieldName);
            return e.getTime();
        } finally {
            if (e != null) e.close();
        }
    }

    public String getString() throws PrimeReadException {
        if (status == -3) return null;
        checkType(TYPE_STRING);
        Pointer strPtr = PrimeNativeHelper.createPointer(size);
        primeApi.ObValGetStr(valueRef.getValue(), strPtr);
        String resultString = strPtr.getString(0, PrimeNativeHelper.CP866).trim();
//        primeApi.DSDoneObject(strPtr);
        return resultString;
    }

    public Long getLong() throws  PrimeReadException {
        if (status == -3) return null;
        checkType(TYPE_LONGINT);
        return primeApi.ObValGetLongInt(valueRef.getValue());
    }


    public Float getFloat() throws PrimeReadException {
        if (status == -3) return null;
        checkType(TYPE_FLOAT);
        return primeApi.ObValGetSingle(valueRef.getValue());
    }

    public Date getDate() throws PrimeReadException {
        if (status == -3) return null;
        checkType(TYPE_DATE);
        PointerByReference dateBufPtr = new PointerByReference();
        short statusPtr = primeApi.ObValGetPointer(valueRef.getValue(), dateBufPtr);
        if (statusPtr != 0 || size != 4) {
            throw new PrimeReadException("date status or size fail " + statusPtr + " size=" + size);
        }
        Date date = convertToDate(dateBufPtr.getValue().getByteArray(0, 4));
        primeApi.DoneHandle(dateBufPtr);
        return date;
    }

    public Date getTime() throws PrimeReadException {
        if (status == -3) return null;
        checkType(TYPE_TIME);
        PointerByReference bufPtr = new PointerByReference();
        short statusPtr = primeApi.ObValGetPointer(valueRef.getValue(), bufPtr);
        if (statusPtr != 0 || size != 4) {
            throw new PrimeReadException("time status or size fail " + statusPtr + " size=" + size);
        }
        Date date = convertTime(bufPtr.getValue().getByteArray(0, 4));
        primeApi.DoneHandle(bufPtr);
        return date;
    }


    public void close() {
        primeApi.DoneHandle(valueRef);
    }

    private Date convertTime(byte[] bytes) {
        int b1 = bytes[0];
        int b2 = bytes[1];
        int b3 = bytes[2];
        int b4 = bytes[3];
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MILLISECOND, b1 * 10);
        cal.set(Calendar.SECOND, b2);
        cal.set(Calendar.MINUTE, b3);
        cal.set(Calendar.HOUR_OF_DAY, b4);
        return cal.getTime();
    }


    private final Date convertToDate(byte[] bytes) {
        int b1 = bytes[0];
        int b2 = bytes[1];
        int b3 = bytes[2];
        int b4 = bytes[3];
        int year = (b4 << 8) + (b3 & 0xFF);
        int month = b2;
        int day = b1;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }

    private int getSize() throws PrimeReadException {
        IntByReference sizeRef = new IntByReference();
        short sizeStatus = primeApi.ObValGetSize(valueRef.getValue(), true, sizeRef);
        if (sizeStatus != 0) throw new PrimeReadException("error call ObValGetSize " + sizeStatus + " status=" + status);
        return sizeRef.getValue();
    }

    private void checkType(byte typeToCheck) throws PrimeReadException {
        if (type != typeToCheck) throw new PrimeReadException("Type " + type + " do not match to " + typeToCheck);
    }

    private void checkException() throws PrimeReadException {
        if (status != 0 && status != -3) {
            throw new PrimeReadException("couldn't read " + fieldName + " status=" + status + " type=" + type);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        close();
    }


}
