package ru.navilab.jprime.loader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Mikhailov_KG on 25.06.2015.
 */
public class GeoproConnection {
    public static final String JDBC_GEOPRO = "jdbc:oracle:thin:@10.124.13.55:1521:geopro";

    public GeoproConnection() {
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Connection createConnection() throws SQLException {
        String geoproUser = System.getProperty("geoproUser");
        String geoproPass = System.getProperty("geoproPass");
        String jdbc = System.getProperty("jdbcGeopro", JDBC_GEOPRO);
        return DriverManager.getConnection(jdbc, geoproUser, geoproPass);
    }
}
