package ru.navilab.jprime.loader;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.*;
import ru.navilab.jprime.service.PFDData;
import ru.navilab.jprime.service.PrimeApi;
import ru.navilab.jprime.service.PrimeFieldMetaInfo;
import ru.navilab.jprime.service.PrimeReadException;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static ru.navilab.jprime.loader.PrimeFieldExtractor.*;

/**
 * Created by Mikhailov_KG on 01.12.2015.
 */
public class JPrimeScanTest {
    public static final int MAX_ARRAY_SIZE = 0xF000;
    private PrimeApi primeApi;

    public JPrimeScanTest() {
        primeApi = (PrimeApi) Native.loadLibrary("DatServ1", PrimeApi.class);
    }

    public static void main(String[] args) {
        new JPrimeScanTest().test(args[0]);
    }

    private void test(String file) {
        File rootDir = new File(file);
        try {
            scanDir(rootDir);
        } catch (PrimeReadException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void scanDir(File file) throws PrimeReadException, SQLException, IOException {
        File[] files = file.listFiles(JPrimeTest.wsFilenameFilter);
        for (File wsFile : files) {
            System.err.println(wsFile.getAbsolutePath());
            String absolutePath = wsFile.getAbsolutePath();
            testWsFile(absolutePath);
        }
        File[] dirFiles = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return (pathname.isDirectory());
            }
        });
        for (File dirFile : dirFiles) {
            scanDir(dirFile);
        }
    }

    public synchronized void testWsFile(String wsFilename) throws PrimeReadException, SQLException, IOException {

        PointerByReference lfHandle = new PointerByReference();
        System.err.println("init = " + primeApi.SysLFInitOpen(wsFilename, lfHandle));

        PointerByReference tablesNamesRef = new PointerByReference();
        int sz = primeApi.DSGetTableNames(lfHandle.getValue(), (short) 0x1, tablesNamesRef);
        byte[] byteArray = tablesNamesRef.getValue().getByteArray(0, sz);
        byte[] stringByteArray = PrimeNativeHelper.extractZeroTerminatedArray(byteArray);
        String tableNamesString = new String(stringByteArray, PrimeNativeHelper.CP866);
        String[] tableNamesArr = tableNamesString.split("\r");
        for (String table : tableNamesArr) {
            System.err.println("table " + table);
//            Pointer stableNamePtr = PrimeNativeHelper.createPointer(table, PrimeNativeHelper.CP866);
            dumpTable(table, lfHandle);
        }
//        dumpTable(PrimeNativeHelper.createPointer("LAS", PrimeNativeHelper.CP866), lfHandle);

//        lrHandle = new PointerByReference();
//
//        PointerByReference tablesNamesRef = new PointerByReference();
//        primeApi.DSGetTableNames(lfHandle.getValue(), (short) 0xFFFF, tablesNamesRef);
//        String tableNamesString = tablesNamesRef.getValue().getString(0, "CP866");
//        String[] tableNamesArr = tableNamesString.split("\r");
//        for (String s : tableNamesArr) {
//            System.err.println("table " + s);
//        }
//
//
//        primeApi.DoneHandle(tablesNamesRef);
//
//        primeApi.DoneHandle(lrHandle);
//
        primeApi.DoneHandle(lfHandle);


//        IntByReference errorCode = new IntByReference();
//        Pointer tableNamePtr = PrimeNativeHelper.createPointer("DESCRIPTION", PrimeNativeHelper.CP866);
//        Pointer curveListObj = primeApi.DSFindCurvesMatchingConditionsByWSName(wsFilename,
//                tableNamePtr, "МЕТОД_ГИС=*", 128, errorCode, 0, Pointer.NULL, Pointer.NULL);
////        System.err.println("curveListObj " + curveListObj + " " + errorCode.getValue());
//
//        int count = primeApi.DSListCount(curveListObj);
//
//        System.err.println("count " + count);
//
//        if (count > 0) {
//            for (int i = 0; i < count; i++) {
//                processCurve(primeApi, curveListObj, i, wsFilename);
//            }
//        }
//        primeApi.DSDoneObject(curveListObj);
    }

    private void dumpTable(String table, PointerByReference lfHandle) throws PrimeReadException {
        PointerByReference lrHandle = new PointerByReference();
        short status = primeApi.Get_Table(JPrimeLoader.getDosString(table), lfHandle.getValue(), lrHandle);
        System.err.println("Get_Table=" + status);

        iterateObjectsOld(lrHandle);

        primeApi.DoneHandle(lrHandle);
    }

    private void getResourceDescription() {
        Pointer stableNamePtr = PrimeNativeHelper.createPointer("LAS", PrimeNativeHelper.CP866);
        PointerByReference tableDescHandle = new PointerByReference();
        System.err.println("tdesc = " + primeApi.GetTDescF("C:\\PRIME\\SHAPKA.RES", stableNamePtr, tableDescHandle));
        primeApi.DoneHandle(tableDescHandle);
    }

    private void iterateObjectsNew(PointerByReference lrHandle) throws PrimeReadException {
        List<PrimeFieldMetaInfo> fieldMetaInfoList = JPrimeLoader.getFieldList(primeApi, lrHandle);

        int objectCount = getObjectCount(lrHandle);
        System.err.println("objectCount = " + objectCount);
        for (short i = 1; i < objectCount + 1; i++) {
            PointerByReference objectHandle = new PointerByReference();
            short status = primeApi.GetObjByIdN(lrHandle.getValue(), i, objectHandle);
            System.err.println(i + " GetObjByIdN=" + status);
            if (status == 0) {
                testObject(objectHandle, fieldMetaInfoList);
            }
            primeApi.DoneHandle(objectHandle);
        }
    }

    private int getObjectCount(PointerByReference lrHandle) {
        IntByReference lrObjectCount = new IntByReference();
        short err = primeApi.LRGetObjCount(lrHandle.getValue(), lrObjectCount);
        System.err.println("LRGetObjCount=" + err);
        if (err == 0) {
            int objectCount = lrObjectCount.getValue();
            System.err.println("lrObjectCount=" + objectCount);
            return objectCount;
        } else return 0;
    }

    private void iterateObjectsOld(PointerByReference lrHandle) throws PrimeReadException {
        List<PrimeFieldMetaInfo> fieldMetaInfoList = JPrimeLoader.getFieldList(primeApi, lrHandle);
        System.err.println("goto " + primeApi.Goto_BeginObj(lrHandle.getValue()));
        short err = 0;
        do {
            PointerByReference objectHandle = new PointerByReference();
            short status = primeApi.GetCurObj(lrHandle.getValue(), objectHandle);
            System.err.println("GetCurObj=" + status);
            if (status == 0) {
                testObject(objectHandle, fieldMetaInfoList);
            }
            primeApi.DoneHandle(objectHandle);
        } while((err = primeApi.Goto_NextObj(lrHandle.getValue())) == 0);
        System.err.println("err = "  + err);
    }

    private void testObject(PointerByReference objectHandle, List<PrimeFieldMetaInfo> fieldMetaInfoList) throws PrimeReadException {
        testMetaKeyStr(objectHandle.getValue());
        testKeyStr(objectHandle.getValue());
        testAllFields(objectHandle.getValue(), fieldMetaInfoList);
        //testOldFieldIteration(objectHandle);
    }

    private void testOldFieldIteration(PointerByReference objectHandle) {
        short obvalStatus = 0;
        for(int i=0; obvalStatus == 0; i++) {
            PointerByReference valueHandler = new PointerByReference();
            obvalStatus = primeApi.GetObValByIndex(i, objectHandle.getValue(), valueHandler);
            if (obvalStatus == 0) {
                IntByReference sizeRef = new IntByReference();
                primeApi.ObValGetSize(valueHandler.getValue(), true, sizeRef);
                ByteByReference typeRef = new ByteByReference();
                primeApi.ObValGetType(valueHandler.getValue(), typeRef);
                System.err.println("obval " + i + " " + obvalStatus + " " + typeRef.getValue());
                if (typeRef.getValue() == TYPE_STRING) {
                    Pointer stringPointer = PrimeNativeHelper.createPointer(sizeRef.getValue());
                    primeApi.ObValGetStr(valueHandler.getValue(), stringPointer);
                    System.err.println("s = " + stringPointer.getString(0, "CP866"));
                }
            } else {
                System.err.println("end");
            }
            primeApi.DoneHandle(valueHandler);
        }

        Pointer keyString = PrimeNativeHelper.createPointer(255);
        System.err.println("getpackkeystr " + primeApi.GetMetaKeyStr(objectHandle.getValue(), (byte) 0xFF, keyString));

        System.err.println("keyString '" + keyString.getString(0).trim() + "'");

        primeApi.DSDoneObject(keyString);
    }

    private void testAllFields(Pointer objectHandleValue, List<PrimeFieldMetaInfo> fieldMetaInfoList) throws PrimeReadException {
        // берем из fieldMetaInfoList
//        ShortByReference counterRef = new ShortByReference();
//        short err = primeApi.GetTDescCounter(objectHandleValue, counterRef);
//        short counter = counterRef.getValue();
//        System.err.println("GetTDescCounter=" + err + " " + counter);
        for (short i = 0; i < fieldMetaInfoList.size(); i++) {
            testFieldName(objectHandleValue, i, fieldMetaInfoList.get(i));
        }
    }

    private void testFieldName(Pointer objectHandleValue, short index, PrimeFieldMetaInfo fieldMetaInfo) throws PrimeReadException {
        // берем из MetaInfo
//        Pointer pfdData = PrimeNativeHelper.createPointer(1024);
//        short err = primeApi.GetTDescFldsI(objectHandleValue, index, pfdData);
//        System.err.print(index + " GetTDescFldsI=" + err);
//        PFDData pfd = new PFDData(pfdData);
        String fieldName = fieldMetaInfo.getFieldName();
        System.err.print(" fieldName=" + fieldName);
        byte type = fieldMetaInfo.getType();
        System.err.print(" type=" + type);
        if (isSimpleValueType(type)) {
            dumpSimpleValue(objectHandleValue, index, fieldName, type);
        } else if (type == TYPE_ARRAY) {
            dumpArrayValue(objectHandleValue, index, fieldName, type);
        }
        System.err.println();
        //primeApi.DSDoneObject(pfdData);
    }

    private void dumpArrayValue(Pointer objectHandleValue, short index, String fieldName, byte type) {
        PointerByReference arrHandle = new PointerByReference();
        short err = primeApi.GetArrayByIndex(arrHandle, index, objectHandleValue);
        System.err.print(" GetArrayByIndex=" + err);

        ShortByReference columnCountRef = new ShortByReference();
        short err2 = primeApi.ArrayGetColumns(arrHandle.getValue(), columnCountRef);
        System.err.print(" ArrayGetColumns=" + err2);
        short arrColumnCount = columnCountRef.getValue();
        System.err.print(" columns=" + arrColumnCount);

        int arrLength = primeApi.ArrayGetLen(arrHandle.getValue());
        System.err.print(" rows=" + arrLength);

        PointerByReference tableDescHandle = new PointerByReference();
        short err3 = primeApi.ArrayGetDesc(arrHandle.getValue(), tableDescHandle);
        System.err.print(" ArrayGetDesc=" + err3);

        System.err.println();
        System.err.println("{");
        for (short i = 0; i < arrColumnCount; i++) {
            dumpArrayColumn(arrHandle.getValue(), i, arrLength);
        }
        System.err.println("}");

        primeApi.DoneHandle(tableDescHandle);
        primeApi.DoneHandle(arrHandle);
    }

    private void dumpArrayColumn(Pointer arrHandleValue, short columnIndex, int arrLength) {
        Pointer pfdData = PrimeNativeHelper.createPointer(1024);
        short err4 = primeApi.GetTDescFldsI(arrHandleValue, columnIndex, pfdData);
        System.err.print("\tGetTDescFldsI=" + err4);
        PFDData pfd = new PFDData(pfdData);
        String fieldName = pfd.getFieldName();
        System.err.print(" fieldName=" + fieldName);
        byte type = pfd.getType();
        System.err.print(" type=" + type);

        if (arrLength > 0) {
            short columnNumber = (short) (columnIndex + 1);
            switch (type) {
                case PrimeFieldExtractor.TYPE_FLOAT:
                    dumpFloatValues(arrHandleValue, columnNumber, arrLength);
                    break;
                case PrimeFieldExtractor.TYPE_STRING:
                    dumpStringArrayValues(arrHandleValue, columnNumber, arrLength);
                    break;
            }
        }
        System.err.println();
//        primeApi.DSDoneObject(pfdData);
    }

    private void dumpStringArrayValues(Pointer arrHandleValue, short columnNumber, int arrLength) {
        String[] arr = new String[arrLength];
        Pointer stringPointer = PrimeNativeHelper.createPointer(255);
        for (int i = 0; i < arrLength; i++) {
            short status = primeApi.ArrayGetStrColumnData(arrHandleValue, (byte) columnNumber, i, stringPointer);
            arr[i] = stringPointer.getString(0, PrimeNativeHelper.CP866);
        }
        System.err.print(" v[len/2]=" + arr[arrLength/2]);
//        primeApi.DSDoneObject(stringPointer);
    }

    private void dumpFloatValues(Pointer arrHandleValue, short columnNumber, int arrLength) {
        float[] arr = getFloatArray(arrHandleValue, columnNumber, arrLength);
        System.err.print(" v[len/2]=" + arr[arrLength/2]);
//        primeApi.DSDoneObject(buffer);
    }

    private float[] getFloatArray(Pointer arrHandleValue, short columnNumber, int arrLength) {
        if (arrLength > MAX_ARRAY_SIZE) return getBigFloatArray(arrHandleValue, columnNumber, arrLength);
        else return getSmallFloatArray(arrHandleValue, columnNumber, arrLength);
    }

    private float[] getSmallFloatArray(Pointer arrHandleValue, short columnNumber, int arrLength) {
        int size = arrLength * PrimeNativeHelper.FLOAT_SIZE;
        Pointer buffer = PrimeNativeHelper.createPointer(size);
        primeApi.ArrayGetSingleColData(arrHandleValue, columnNumber, 0, arrLength, buffer);
        return buffer.getFloatArray(0, arrLength);
    }

    private float[] getBigFloatArray(Pointer arrHandleValue, short columnNumber, int arrLength) {
        int requestLength = Math.min(MAX_ARRAY_SIZE, arrLength);
        float[] arr = new float[arrLength];
        for (int offset=0; offset < arrLength; ) {
            System.err.print(String.format(" req %d %d %d", offset, requestLength, arrLength));
            int size = requestLength * PrimeNativeHelper.FLOAT_SIZE;
            Pointer buffer = PrimeNativeHelper.createPointer(size);
            primeApi.ArrayGetSingleColData(arrHandleValue, columnNumber, offset, requestLength, buffer);
            float[] floatArray = buffer.getFloatArray(0, requestLength);
            System.arraycopy(floatArray, 0, arr, offset, requestLength);
            offset += requestLength;
            if ((offset + requestLength) > arrLength) requestLength = arrLength - offset;
        }
        return arr;
    }

    private float[] getBigFloatArrayTrivial(Pointer arrHandleValue, short columnNumber, int arrLength) {
        float[] arr = new float[arrLength];
        for (int i=0; i < arrLength; i++) {
            float v = primeApi.ArrayGetSingleColumnData(arrHandleValue, columnNumber, i);
            arr[i] = v;
        }
        return arr;
    }

    private void dumpSimpleValue(Pointer objectHandleValue, short index, String fieldName, byte type) throws PrimeReadException {
        PrimeFieldExtractor extractor = null;
        try {
            extractor = new PrimeFieldExtractor(primeApi, objectHandleValue, index, fieldName, type);
            switch (type) {
                case TYPE_STRING:
                    System.err.print(" string=" + extractor.getString()); break;
                case TYPE_DATE:
                    System.err.print(" date=" + extractor.getDate()); break;
                case TYPE_FLOAT:
                    System.err.print(" float=" + extractor.getFloat()); break;
                case TYPE_LONGINT:
                    System.err.print(" long=" + extractor.getLong()); break;
                case TYPE_TIME:
                    System.err.print(" time=" + extractor.getTime()); break;
            }
        } finally {
            if (extractor != null) extractor.close();
        }
    }

    private boolean isSimpleValueType(byte type) {
        return type == TYPE_STRING || type == TYPE_DATE ||
                type == TYPE_FLOAT || type == TYPE_LONGINT || type == TYPE_TIME;
    }


    private void testKeyStr(Pointer objectHandleValue) {
        Pointer keyString = PrimeNativeHelper.createPointer(255);
        primeApi.GetKeyStr(objectHandleValue, (short) 3, (byte) 255, keyString);
        System.err.println("GetKeyStr " + keyString.getString(0).trim());
//        primeApi.DSDoneObject(keyString);
    }

    private void testMetaKeyStr(Pointer objectHandleValue) {
        Pointer keyString = PrimeNativeHelper.createPointer(255);
        primeApi.GetMetaKeyStr(objectHandleValue, (byte)255, keyString);
        System.err.println("GetMetaKeyStr " + keyString.getString(0).trim());
//        primeApi.DSDoneObject(keyString);
    }

    private void processCurve(PrimeApi primeApi, Pointer plist, int curveIndex, String wsFilename) throws PrimeReadException, SQLException, IOException {
        PointerByReference primeObjectRef = new PointerByReference();
        PointerByReference arrayRef = new PointerByReference();
        IntByReference columnNumberRef = new IntByReference();
        FloatByReference startRef = new FloatByReference();
        FloatByReference stopRef = new FloatByReference();
        FloatByReference stepRef = new FloatByReference();
        IntByReference dataTypeRef = new IntByReference();
        IntByReference versionRef = new IntByReference();
        IntByReference planNamesLen = new IntByReference();
        IntByReference tableNameLen = new IntByReference();
        IntByReference curveNameLen = new IntByReference();
        IntByReference keyStrLen = new IntByReference();
        PointerByReference planNamesRef = new PointerByReference();
        PointerByReference tableNameRef= new PointerByReference();
        PointerByReference curveNameRef= new PointerByReference();
        PointerByReference keyStrRef = new PointerByReference();

        primeApi.DSGetCurveDataFromList(plist, curveIndex, primeObjectRef, arrayRef, columnNumberRef, startRef, stopRef, stepRef, dataTypeRef, versionRef,
                planNamesLen, tableNameLen, curveNameLen, keyStrLen, planNamesRef, tableNameRef, curveNameRef, keyStrRef);

        String planName = JPrimeTest.getString(planNamesRef);
        String tableName = JPrimeTest.getString(tableNameRef);
        String curveName = JPrimeTest.getString(curveNameRef);
        String keyStr = JPrimeTest.getString(keyStrRef);

        int arrayLen = primeApi.ArrayGetLen(arrayRef.getValue());
        float[] floatValues = new float[arrayLen];
        for (int j = 0; j < arrayLen; j++) {
            floatValues[j] = primeApi.ArrayGetSingleColumnData(arrayRef.getValue(), (short) columnNumberRef.getValue(), j);
        }


        System.err.println(String.format("%s\t%s\t%s\t%s\t%s\tcol=\t%d\t%s\t%s", planName, tableName, curveName, keyStr,
                arrayLen, columnNumberRef.getValue(), primeObjectRef.getValue(), stopRef.getValue()));

        JPrimeTest.test(primeApi, primeObjectRef.getValue());

        primeApi.DoneHandle(planNamesRef);
        primeApi.DoneHandle(tableNameRef);
        primeApi.DoneHandle(curveNameRef);
        primeApi.DoneHandle(keyStrRef);
        primeApi.DoneHandle(primeObjectRef);
        primeApi.DoneHandle(arrayRef);
    }

}
