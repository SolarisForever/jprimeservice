package ru.navilab.jprime.loader.v2;

/**
 * Created by Mikhailov_KG on 28.04.2018.
 */
public class NaviPrimeException extends Exception {
    private int status;

    NaviPrimeException(int status) {
        this.status = status;
    }
}
