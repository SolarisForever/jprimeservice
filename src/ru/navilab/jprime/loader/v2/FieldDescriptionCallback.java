package ru.navilab.jprime.loader.v2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mikhailov_KG on 02.05.2018.
 */
public class FieldDescriptionCallback {
    private List<FieldDescription> list = new ArrayList<FieldDescription>();

    public void add(String fieldName, int fieldType, String fieldUnit, int length, int decimals, String fieldComment) {
        try {
            int index = list.size();
            FieldDescription fd = new FieldDescription(index, fieldName.trim(), fieldType, fieldUnit.trim(),
                    length, decimals, fieldComment.trim());
            list.add(fd);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public List<FieldDescription> getList() {
        return list;
    }
}
