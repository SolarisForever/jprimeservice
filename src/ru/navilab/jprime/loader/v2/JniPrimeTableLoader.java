package ru.navilab.jprime.loader.v2;

import ru.navilab.jprime.data.LogProperties;
import ru.navilab.jprime.data.WellLogTables;
import ru.navilab.jprime.service.PrimeReadException;
import ru.navilab.matrix.engine.*;
import ru.navilab.matrix.entity.MatrixEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Mikhailov_KG on 18.05.2018.
 */
class JniPrimeTableLoader {
    private final String wsFilename;
    private final PrimeHandle fileHandle;
    private final WellLogTables wellLogTables;
    private IJniPrimeApi api;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    public JniPrimeTableLoader(IJniPrimeApi api, String wsFilename, PrimeHandle fileHandle, WellLogTables wellLogTables) {
        this.api = api;
        this.wsFilename = wsFilename;
        this.fileHandle = fileHandle;
        this.wellLogTables = wellLogTables;
    }

    void loadTable(String tableName) throws PrimeReadException {
        int status = api.TableExist(fileHandle, tableName);
        if (status == 0) {
            LogProperties properties = new LogProperties();
            Matrix matrix = beginLoadTable(tableName, properties);
            wellLogTables.addLogTable(wsFilename, tableName, matrix, properties);
        } else {
            logger.finer("table " + tableName + " is not found status = " + status);
            wellLogTables.addLogTable(wsFilename, tableName, createEmptyMatrix(), new LogProperties());
        }
    }

    private Matrix beginLoadTable(String tableName, LogProperties properties) throws PrimeReadException {
        PrimeHandle tableHandle = new PrimeHandle(api);
        try {
            int status = api.GetTableNew(fileHandle, tableName, tableHandle);
            if (status != 0) throw new PrimeReadException("GetTableNew " + tableName, status);
            int rowCount = api.LRGetObjCount(tableHandle);
            if (rowCount < 0) throw new PrimeReadException("LRGetObjCount", status);
            FieldDescriptionCallback fieldDescriptionCallback = new FieldDescriptionCallback();
            status = api.getFieldDescriptions(tableHandle, fieldDescriptionCallback);
            if (status != 0) throw new PrimeReadException("getFieldDescriptions", status);
            List<FieldDescription> fieldList = fieldDescriptionCallback.getList();
            MatrixEntity entity = createEntity(tableName, fieldList);
            Matrix matrix = entity.createMatrix();
            for (int i = 0; i < rowCount; i++) {
                PrimeHandle rowHandle = new PrimeHandle(api);
                try {
                    status = api.openRow(tableHandle, i + 1, rowHandle);
                    if (status != 0) throw new PrimeReadException("openRow", status);
                    MatrixRow row = matrix.newRow();
                    for (short columnIndex = 0; columnIndex < fieldList.size(); columnIndex++) {
                        FieldDescription fieldDescription = fieldList.get(columnIndex);
                        String fieldName = fieldDescription.getFieldName();
                        properties.addProperties(fieldName, fieldDescription.getFieldUnit(), fieldDescription.getFieldComment());
                        if (entity.getField(fieldName) != null) {
                            loadField(rowHandle, fieldDescription, row, properties);
                        }
                    }
                } finally {
                    rowHandle.close();
                }
            }
            return matrix;
        } finally {
            tableHandle.close();
        }
    }

    private void loadField(PrimeHandle rowHandle, FieldDescription fieldDescription, MatrixRow row,
                           LogProperties properties)
            throws PrimeReadException {
        String fieldName = fieldDescription.getFieldName();
        int columnIndex = fieldDescription.getIndex();
        int fieldType = fieldDescription.getFieldType();
        switch (fieldType) {
            case PrimeConstants.STRING:
                row.setString(fieldName, api.getString(rowHandle, columnIndex));
                break;
            case PrimeConstants.FLOAT:
                row.setFloat(fieldName, api.getFloat(rowHandle, columnIndex));
                break;
            case PrimeConstants.LONGINT:
                row.setLong(fieldName, api.getLong(rowHandle, columnIndex));
                break;
            case PrimeConstants.MEMO:
                row.setString(fieldName, toOneString(api.getMemo(rowHandle, columnIndex)));
                break;
            case PrimeConstants.DATE:
                row.setDate(fieldName, api.getDate(rowHandle, columnIndex));
                break;
            case PrimeConstants.TIME:
                row.setDate(fieldName, api.getTime(rowHandle, columnIndex));
                break;
            case PrimeConstants.ARRAY:
                LogProperties arrayProperties = new LogProperties();
                properties.addNestedProperties(row.getRowIndex(), fieldDescription.getFieldName(), arrayProperties);
                loadArray(rowHandle, columnIndex, fieldDescription, row, arrayProperties);
                break;
            case PrimeConstants.STRING_C:
                row.setString(fieldName, api.getString(rowHandle, columnIndex));
                break;
        }
    }

    private void loadArray(PrimeHandle rowHandle, int columnIndex, FieldDescription fieldDescription,
                           MatrixRow row, LogProperties arrayProperties) throws PrimeReadException {
        PrimeHandle arrayHandle = new PrimeHandle(api);
        try {
            int status = api.openArray(rowHandle, columnIndex, arrayHandle);
            if (status != 0) throw new PrimeReadException("loadArray", status);

            FieldDescriptionCallback arrayDescriptionCallback = new FieldDescriptionCallback();
            status = api.getFieldDescriptions(arrayHandle, arrayDescriptionCallback);
            if (status != 0) throw new PrimeReadException("Array getFieldDescriptions", status);

            List<MatrixBuilderMeta> matrixBuilderMetaList = new ArrayList<>();
            List<FieldDescription> arrayDescriptionList = arrayDescriptionCallback.getList();
            for (FieldDescription fieldDesc : arrayDescriptionList) {
                arrayProperties.addProperties(fieldDesc.getFieldName(), fieldDesc.getFieldUnit(), fieldDesc.getFieldComment());
                int columnArrayIndex = fieldDesc.getIndex() + 1;
                if (fieldDesc.getFieldType() == PrimeConstants.FLOAT) {
                    float[] arrayFloat = api.getArrayFloat(arrayHandle, columnArrayIndex);
                    if (arrayFloat != null) {
                        MatrixBuilderMeta builderMeta = new MatrixBuilderMeta(fieldDesc.getFieldName(),
                                MatrixColumnType.FLOAT, arrayFloat, createNotNulls(arrayFloat.length), arrayFloat.length);
                        matrixBuilderMetaList.add(builderMeta);
                    }
                } else if (fieldDesc.getFieldType() == PrimeConstants.STRING) {
                    String[] arrayString = api.getArrayString(arrayHandle, columnArrayIndex);
                    if (arrayString != null) {
                        MatrixBuilderMeta builderMeta = new MatrixBuilderMeta(fieldDesc.getFieldName(),
                                MatrixColumnType.STRING, arrayString,
                                createNotNulls(arrayString.length), arrayString.length);
                        matrixBuilderMetaList.add(builderMeta);
                    }
                }
            }
            Matrix arrayMatrix = createArrayMatrix(fieldDescription, arrayDescriptionList, matrixBuilderMetaList);
            row.getCell(fieldDescription.getFieldName()).setMatrixObject(arrayMatrix);
        } finally {
            arrayHandle.close();
        }
    }

    private Matrix createArrayMatrix(FieldDescription fieldDescription, List<FieldDescription> arrayDescriptionList,
                                     List<MatrixBuilderMeta> matrixBuilderMetaList) {
        String entityName = "ArrayOf" + fieldDescription.getFieldName();
        if (matrixBuilderMetaList.size() > 0) {
            return MatrixBuilder.buildMatrix(entityName, matrixBuilderMetaList);
        } else { // create empty matrix
            MatrixEntity matrixEntity = new MatrixEntity(entityName);
            for (FieldDescription arrayFieldDesc : arrayDescriptionList) {
                MatrixColumnType mtype = getMatrixFieldType(arrayFieldDesc.getFieldType());
                if (mtype != null) matrixEntity.addField(arrayFieldDesc.getFieldName(), mtype);
            }
            return matrixEntity.createMatrix();
        }
    }

    private boolean[] createNotNulls(int length) {
        boolean[] notNulls = new boolean[length];
        Arrays.fill(notNulls, true);
        return notNulls;
    }

    private MatrixBuilderMeta createStringArrayMeta(FieldDescription fieldDesc, String[] arrayString) {
        boolean[] notNulls = new boolean[arrayString.length];
        Arrays.fill(notNulls, true);
        return new MatrixBuilderMeta(fieldDesc.getFieldName(), MatrixColumnType.STRING, arrayString, notNulls, arrayString.length);
    }

    private String toOneString(String[] stringArray) {
        StringBuilder sb = new StringBuilder();
        for (String s : stringArray) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }

    private MatrixEntity createEntity(String tableName, List<FieldDescription> fieldList) {
        MatrixEntity e = new MatrixEntity(tableName);
        for (FieldDescription fieldDesc : fieldList) {
            String name = fieldDesc.getFieldName();
            logger.finer("found prime field name '" + name + "'");
            MatrixColumnType mType = getMatrixFieldType(fieldDesc.getFieldType());
            if (mType != null) e.addField(name, mType);
        }
        return e;
    }

    private MatrixColumnType getMatrixFieldType(int primeFieldType) {
        switch (primeFieldType) {
            case PrimeConstants.STRING:
                return MatrixColumnType.STRING;
            case PrimeConstants.DATE:
            case PrimeConstants.TIME:
                return MatrixColumnType.DATE;
            case PrimeConstants.FLOAT:
                return MatrixColumnType.FLOAT;
            case PrimeConstants.LONGINT:
                return MatrixColumnType.LONG;
            case PrimeConstants.ARRAY:
                return MatrixColumnType.OBJECT;
            case PrimeConstants.STRING_C:
                return MatrixColumnType.STRING;
        }
        return null;
    }

    private Matrix createEmptyMatrix() {
        return new Matrix(MatrixEntity.NULL, 0);
    }
}
