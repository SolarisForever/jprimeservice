package ru.navilab.jprime.loader.v2;

/**
 * Created by Mikhailov_KG on 05.06.2018.
 */
public class SingleStaticApiFactory implements IJniPrimeApiFactory {
    private static volatile JniPrimeApi primeApi;

    @Override
    public synchronized IJniPrimeApi getJniPrimeApi() {
        if (primeApi == null) {
            primeApi = new JniPrimeApi();
            primeApi.loadLibrary(JniPrimeApi.BASE_DLL_NAME + "1");
        }
        return primeApi;
    }
}
