package ru.navilab.jprime.loader.v2;

/**
 * Created by Mikhailov_KG on 02.05.2018.
 */
public class FieldDescription {
    private int index;
    private String fieldName;
    private int fieldType;
    private String fieldUnit;
    private int length;
    private int decimals;
    private String fieldComment;

    public FieldDescription(int index, String fieldName, int fieldType, String fieldUnit, int length, int decimals, String fieldComment) {
        this.index = index;
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.fieldUnit = fieldUnit;
        this.length = length;
        this.decimals = decimals;
        this.fieldComment = fieldComment;
    }

    public int getIndex() {
        return index;
    }

    public String getFieldName() {
        return fieldName;
    }

    public int getFieldType() {
        return fieldType;
    }

    public String getFieldUnit() {
        return fieldUnit;
    }

    public int getLength() {
        return length;
    }

    public int getDecimals() {
        return decimals;
    }

    public String getFieldComment() {
        return fieldComment;
    }

    @Override
    public String toString() {
        return String.format("%s %d '%s' %d %d '%s'", fieldName
                ,fieldType
                ,fieldUnit
                ,length
                ,decimals
                ,fieldComment);
    }
}
