package ru.navilab.jprime.loader.v2;

/**
 * Created by Mikhailov_KG on 10.05.2018.
 */
public class PrimeConstants {
    public static final byte LONGINT = 4;
    public static final byte FLOAT = 5;
    public static final byte DOUBLE = 6;
    public static final byte STRING = 7;
    public static final byte DATE = 8;
    public static final byte TIME = 9;
    public static final byte ARRAY = 10;
    public static final byte STRING_C = 12;
    public static final byte MEMO = 19;
    public static final String LAS_TABLE = "LAS";
    public static final String DESCRIPTION_TABLE = "DESCRIPTION";
    public static final String ARMG_TABLE = "ДАННЫЕ_АРМГ";
}
