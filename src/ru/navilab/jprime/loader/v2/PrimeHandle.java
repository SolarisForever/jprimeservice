package ru.navilab.jprime.loader.v2;

/**
 * Created by Mikhailov_KG on 28.04.2018.
 */
public class PrimeHandle {
    private IJniPrimeApi primeApi;
    private long handle;

    public PrimeHandle(IJniPrimeApi primeApi) {
        this.primeApi = primeApi;
    }

    public void close() {
        if (handle != 0l) {
            try {
                primeApi.doneHandle(this);
            } finally {
                handle = 0l;
                primeApi = null;
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        close();
    }

    @Override
    public String toString() {
        return "" + handle;
    }
}
