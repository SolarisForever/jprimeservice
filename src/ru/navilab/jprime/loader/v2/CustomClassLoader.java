package ru.navilab.jprime.loader.v2;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mikhailov_KG on 17.05.2018.
 */
public class CustomClassLoader extends ClassLoader {
    private Map<String, Class<?>> classMap = new HashMap<String, Class<?>>();


    @Override
    public Class<?> findClass(String name) throws ClassNotFoundException {
        try {
            Class<?> aClass = classMap.get(name);
            if (aClass != null) return aClass;

            byte[] data = load(name);
            Class<?> clz = defineClass(name, data, 0, data.length);
            resolveClass(clz);
            classMap.put(name, clz);
            return clz;
        } catch (IOException e) {
            throw new ClassNotFoundException("Class '" + name + "' is not found", e);
        }
    }

    private byte[] load(String name) throws IOException {
        InputStream in = ClassLoader.getSystemResourceAsStream(name.replace(".", "/") + ".class");
        ByteArrayOutputStream bufOut = new ByteArrayOutputStream();
        try {
            BufferedInputStream bufIn = new BufferedInputStream(in);
            int i;
            while ((i = bufIn.read()) != -1) {
                bufOut.write(i);
            }
            bufIn.close();
            return bufOut.toByteArray();
        } finally {
            bufOut.close();
        }
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
