package ru.navilab.jprime.loader.v2;

/**
 * Created by Mikhailov_KG on 18.05.2018.
 */
public interface PrimeApiStatMBean {
    int getLoadDllCount();
    String getCurrentDllName();
    double getTimeInHoursToUnload();
    double getWorkTimeInHours();
}
