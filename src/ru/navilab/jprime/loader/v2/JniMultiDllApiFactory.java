package ru.navilab.jprime.loader.v2;

import ru.navilab.jprime.crawler.utils.MBeanUtils;

import java.util.logging.Logger;

/**
 * Created by Mikhailov_KG on 18.05.2018.
 */
public class JniMultiDllApiFactory implements IJniPrimeApiFactory, IPrimeApiFactoryStat {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private long lastTime = System.currentTimeMillis();
    private IJniPrimeApi api;
    private CustomClassLoader customClassLoader;
    private int createCount;
    private int nextDllIndex = 1;
    private String currentDllName;

    public JniMultiDllApiFactory() {
        PrimeApiStat primeApiStat = new PrimeApiStat(this);
        MBeanUtils.register(primeApiStat);
    }

    @Override
    public final synchronized IJniPrimeApi getJniPrimeApi() {
        if (api == null || timeLimit()) api = createApi();
        return api;
    }

    private final boolean timeLimit() {
        long nowTime = System.currentTimeMillis();
        double hours = (nowTime - lastTime) / 1000.0 / 60 / 60;
        if (hours > 1) return true;
        else return false;
    }

    private synchronized IJniPrimeApi createApi() {
        try {
            customClassLoader = new CustomClassLoader();
            Class<?> aClass = customClassLoader.findClass(JniPrimeApi.class.getName());
            IJniPrimeApi newApi = (IJniPrimeApi) aClass.newInstance();
            currentDllName = getNextDllName();
            ((INativeLibraryLoader)newApi).loadLibrary(currentDllName);
            createCount++;
            logger.info("create IJniPrimeApi " + currentDllName + " " + createCount);
            lastTime = System.currentTimeMillis();
            return newApi;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private String getNextDllName() {
        String dllName = JniPrimeApi.BASE_DLL_NAME + (nextDllIndex++);
        if (nextDllIndex > JniPrimeApi.DLL_COUNT) nextDllIndex = 1;
        return dllName;
    }

    @Override
    public int getCreateCount() {
        return createCount;
    }

    @Override
    public String getCurrentDllName() {
        return currentDllName;
    }

    @Override
    public long getLastTime() {
        return lastTime;
    }
}
