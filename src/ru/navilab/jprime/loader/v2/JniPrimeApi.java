package ru.navilab.jprime.loader.v2;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Mikhailov_KG on 28.04.2018.
 */
public class JniPrimeApi implements IJniPrimeApi, INativeLibraryLoader {
    public static final String BASE_DLL_NAME = "navi_prime";
    public static final int DLL_COUNT = 3;

    @Override
    public void loadLibrary(String dllName) {
        System.loadLibrary(dllName);
    }

    @Override
    public native int SysLFInitOpen(String wsFileName, PrimeHandle fileHandle);
    @Override
    public native int TableExist(PrimeHandle fileHandle, String tableName);
    @Override
    public native int GetTableNew(PrimeHandle fileHandle, String tableName, PrimeHandle tableHandle);
    @Override
    public native int LRGetObjCount(PrimeHandle tableHandle);
    @Override
    public native int getFieldDescriptions(PrimeHandle tableHandle, FieldDescriptionCallback fieldDescCallback);
    @Override
    public native int openRow(PrimeHandle tableHandle, int rowIndex, PrimeHandle rowHandle);
    @Override
    public native int doneHandle(PrimeHandle handle);
    private native String getStringNative(PrimeHandle rowHandle, int columnIndex);
    private native byte[] getDateTime4b(PrimeHandle rowHandle, int columnIndex);
    @Override
    public native long getLong(PrimeHandle rowHandle, int columnIndex);
    @Override
    public native float getFloat(PrimeHandle rowHandle, int columnIndex);
    @Override
    public native String[] getMemo(PrimeHandle rowHandle, int columnIndex);
    @Override
    public native int openArray(PrimeHandle rowHandle, int columnIndex, PrimeHandle arrayHandle);
    @Override
    public native float[] getArrayFloat(PrimeHandle arrayHandle, int columnArrayIndex);
    @Override
    public native String[] getArrayString(PrimeHandle arrayHandle, int columnArrayIndex);

    @Override
    public String getString(PrimeHandle rowHandle, int columnIndex) {
        String string = getStringNative(rowHandle, columnIndex);
        if (string != null) return string.trim();
        else return null;
    }

    @Override
    public Date getDate(PrimeHandle rowHandle, int columnIndex) {
        byte[] bytes = getDateTime4b(rowHandle, columnIndex);
        if (bytes == null) return null;
        int b1 = bytes[0];
        int b2 = bytes[1];
        int b3 = bytes[2];
        int b4 = bytes[3];
        int year = (b4 << 8) + (b3 & 0xFF);
        int month = b2;
        int day = b1;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }

    @Override
    public Date getTime(PrimeHandle rowHandle, int columnIndex) {
        byte[] bytes = getDateTime4b(rowHandle, columnIndex);
        int b1 = bytes[0];
        int b2 = bytes[1];
        int b3 = bytes[2];
        int b4 = bytes[3];
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MILLISECOND, b1 * 10);
        cal.set(Calendar.SECOND, b2);
        cal.set(Calendar.MINUTE, b3);
        cal.set(Calendar.HOUR_OF_DAY, b4);
        return cal.getTime();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.err.println("finalize " + this.getClass().getName());
    }

}
