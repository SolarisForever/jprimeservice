package ru.navilab.jprime.loader.v2;

import java.util.Date;

/**
 * Created by Mikhailov_KG on 17.05.2018.
 */
public interface IJniPrimeApi {
    int SysLFInitOpen(String wsFileName, PrimeHandle fileHandle);

    int TableExist(PrimeHandle fileHandle, String tableName);

    int GetTableNew(PrimeHandle fileHandle, String tableName, PrimeHandle tableHandle);

    int LRGetObjCount(PrimeHandle tableHandle);

    int getFieldDescriptions(PrimeHandle tableHandle, FieldDescriptionCallback fieldDescCallback);

    int openRow(PrimeHandle tableHandle, int rowIndex, PrimeHandle rowHandle);

    int doneHandle(PrimeHandle handle);

    long getLong(PrimeHandle rowHandle, int columnIndex);

    float getFloat(PrimeHandle rowHandle, int columnIndex);

    String[] getMemo(PrimeHandle rowHandle, int columnIndex);

    int openArray(PrimeHandle rowHandle, int columnIndex, PrimeHandle arrayHandle);

    float[] getArrayFloat(PrimeHandle arrayHandle, int columnArrayIndex);

    String[] getArrayString(PrimeHandle arrayHandle, int columnArrayIndex);

    String getString(PrimeHandle rowHandle, int columnIndex);

    Date getDate(PrimeHandle rowHandle, int columnIndex);

    Date getTime(PrimeHandle rowHandle, int columnIndex);
}
