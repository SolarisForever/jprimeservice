package ru.navilab.jprime.loader.v2;

/**
 * Created by Mikhailov_KG on 18.05.2018.
 */
public class PrimeApiStat implements PrimeApiStatMBean {
    private IPrimeApiFactoryStat stat;
    private long startTime = System.currentTimeMillis();

    public PrimeApiStat(IPrimeApiFactoryStat stat) {
        this.stat = stat;
    }

    @Override
    public int getLoadDllCount() {
        return stat.getCreateCount();
    }

    @Override
    public String getCurrentDllName() {
        return stat.getCurrentDllName();
    }

    @Override
    public double getTimeInHoursToUnload() {
        return hoursSince(stat.getLastTime());
    }

    private double hoursSince(long lastTime) {
        return (System.currentTimeMillis() - lastTime) / 1000.0 / 60 / 60;
    }

    @Override
    public double getWorkTimeInHours() {
        return hoursSince(startTime);
    }
}
