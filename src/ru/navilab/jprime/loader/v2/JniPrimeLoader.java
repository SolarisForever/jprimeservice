package ru.navilab.jprime.loader.v2;

import ru.navilab.jprime.data.WellLogTables;
import ru.navilab.jprime.loader.IPrimeLoader;
import ru.navilab.jprime.service.PrimeReadException;

import java.io.IOException;
import java.sql.SQLException;

public class JniPrimeLoader implements IPrimeLoader {
    private IJniPrimeApiFactory apiFactory = new SingleStaticApiFactory();
    //private Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public synchronized WellLogTables load(String wsFilename) throws PrimeReadException, SQLException, IOException {
        IJniPrimeApi api = apiFactory.getJniPrimeApi();
        PrimeHandle fileHandle = new PrimeHandle(api);
        int status = api.SysLFInitOpen(wsFilename, fileHandle);
        if (status != 0) throw new PrimeReadException("can't open " + wsFilename, status);
        WellLogTables wellLogTables = new WellLogTables();
        try {
            JniPrimeTableLoader tableLoader = new JniPrimeTableLoader(api, wsFilename, fileHandle, wellLogTables);
            tableLoader.loadTable(PrimeConstants.LAS_TABLE);
            tableLoader.loadTable(PrimeConstants.ARMG_TABLE);
            tableLoader.loadTable(PrimeConstants.DESCRIPTION_TABLE);
        } finally {
            fileHandle.close();
        }
        return wellLogTables;
    }

    public void setApiFactory(IJniPrimeApiFactory apiFactory) {
        this.apiFactory = apiFactory;
    }
}
