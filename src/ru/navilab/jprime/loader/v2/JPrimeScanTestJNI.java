package ru.navilab.jprime.loader.v2;

import java.io.File;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by Mikhailov_KG on 28.04.2018.
 */
public class JPrimeScanTestJNI {
    //40013542_05.06.2007_LOG.ws
    public static final String ARMG = "ДАННЫЕ_АРМГ";
    private Map<String,FieldDescription> columnMap = new Hashtable<String, FieldDescription>();

    public static void main(String[] args) {
        new JPrimeScanTestJNI().test(args[0]);
    }

    private void test(String arg) {
        File[] files = new File(".").listFiles();
        for (File file : files) {
            System.err.println("file " + file.getAbsolutePath());
        }
        JniPrimeApi api = new JniPrimeApi();
        PrimeHandle fileHandle = new PrimeHandle(api);
        int status = api.SysLFInitOpen(arg, fileHandle);

        int las = api.TableExist(fileHandle, "LAS");
        System.err.println("exist " + las);

        PrimeHandle tableHandle = new PrimeHandle(api);
        api.GetTableNew(fileHandle, "LAS", tableHandle);
        int rowCount = api.LRGetObjCount(tableHandle);
        System.err.println("count = " + rowCount);

        FieldDescriptionCallback fieldDescriptionCallback = new FieldDescriptionCallback();
        int s = api.getFieldDescriptions(tableHandle, fieldDescriptionCallback);
        if (s != 0) return;

        System.err.println("getFieldDescriptions " + s);
        List<FieldDescription> list = fieldDescriptionCallback.getList();

        for (int i = 0; i < list.size(); i++) {
            FieldDescription fieldDescription = list.get(i);
            columnMap.put(fieldDescription.getFieldName(), fieldDescription);
            System.err.println(i + " " + fieldDescription);
        }

        PrimeHandle row1Handle = new PrimeHandle(api);
        {
            System.err.println("open row 1 " + api.openRow(tableHandle, 1, row1Handle));
            int columnIndex = getIndex("ИМЯ_ФАЙЛА");
            System.err.println("s = '" + api.getString(row1Handle, columnIndex) + "'");
            Date surveyDate = api.getDate(row1Handle, getIndex("ДАТА_ИССЛЕДОВАНИЯ"));
            System.err.println(surveyDate);
        }

        {
            String activity_id = "ACTIVITY_ID";
            long v = api.getLong(row1Handle, getIndex(activity_id));
            System.err.println("long  " + v);

            float f = api.getFloat(row1Handle, getIndex("КРОВЛЯ_КРИВОЙ_ГИС"));
            System.err.println("float top  " + f);

            String[] headerArray = api.getMemo(row1Handle, getIndex("HEADER"));
            if (headerArray != null) {
                for (String header : headerArray) {
                    System.err.println("header = " + header);
                }
            }
        }
        {
            PrimeHandle arrayHandle = new PrimeHandle(api);
            int lasArrayIndex = columnMap.get("LAS_ARRAY").getIndex();
            System.err.println("before open arr " + lasArrayIndex);
            s = api.openArray(row1Handle, lasArrayIndex, arrayHandle);
            System.err.println("s = " + s);
            if (s == 0) {
                FieldDescriptionCallback arrayFieldDescCb = new FieldDescriptionCallback();
                api.getFieldDescriptions(arrayHandle, arrayFieldDescCb);

                List<FieldDescription> list1 = arrayFieldDescCb.getList();
                for (FieldDescription fieldDescription : list1) {
                    System.err.print(fieldDescription);
                    int index = fieldDescription.getIndex() + 1;
                    if (fieldDescription.getFieldType() == PrimeConstants.FLOAT) {
                        System.err.print("index = " + index + " ");
                        float[] arrayFloat = api.getArrayFloat(arrayHandle, index);
                        System.err.print("got array of float " + arrayFloat[44]);
                        printArr10(arrayFloat);
                    } else if (fieldDescription.getFieldType() == PrimeConstants.STRING) {
                        String[] arrayString = api.getArrayString(arrayHandle, index);
                        System.err.print("got array of str " + arrayString[0]);
                        printArr10(arrayString);
                    }
                    System.err.println("");
                }
            }
        }
    }

    private void printArr10(String[] arrayString) {
        for (int i = 0; i < 5 && i < arrayString.length; i++) {
            System.err.print(String.format(" %s", arrayString[i]));
        }
    }

    private void printArr10(float[] arrayFloat) {
        for (int i = 0; i < 5 && i < arrayFloat.length; i++) {
            System.err.print(String.format(" %4.1f", arrayFloat[i]));
        }
        if (arrayFloat.length >= 5) {
            System.err.print(" ...");
            for (int i = arrayFloat.length - 5; i < arrayFloat.length; i++) {
                System.err.print(String.format(" %4.1f", arrayFloat[i]));
            }
        }
    }

    private int getIndex(String activity_id) {
        return columnMap.get(activity_id).getIndex();
    }
}
