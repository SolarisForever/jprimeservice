package ru.navilab.jprime.loader;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;
import ru.navilab.jprime.service.PrimeApi;
import ru.navilab.jprime.service.PrimeFieldMetaInfo;
import ru.navilab.jprime.service.PrimeReadException;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Mikhailov_KG on 05.02.2016.
 */
public class WellCurveHdrTest {
    private static final String DELEMITER = ",";
    private final PrimeApi primeApi;

    public WellCurveHdrTest() {
        primeApi = (PrimeApi) Native.loadLibrary("DatServ1", PrimeApi.class);
    }

    public static void main(String[] args) {
        new WellCurveHdrTest().test(args[0]);
    }

    private void test(String file) {
        File rootDir = new File(file);
        try {
            scanDir(rootDir);
        } catch (PrimeReadException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void scanDir(File file) throws PrimeReadException, SQLException, IOException {
        File[] files = file.listFiles(JPrimeTest.wsFilenameFilter);
        for (File wsFile : files) {
            String absolutePath = wsFile.getAbsolutePath();
            testWsFile(absolutePath);
        }
        File[] dirFiles = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return (pathname.isDirectory());
            }
        });
        for (File dirFile : dirFiles) {
            scanDir(dirFile);
        }
    }

    private void testWsFile(String wsFilename) throws PrimeReadException {
        PointerByReference lfHandle = new PointerByReference();
        primeApi.SysLFInitOpen(wsFilename, lfHandle);

        beginDumpTable(wsFilename, lfHandle, "LAS");

        primeApi.DoneHandle(lfHandle);
    }

    private void beginDumpTable(String wsFilename, PointerByReference lfHandle, String tableName) throws PrimeReadException {
        PointerByReference lrHandle = new PointerByReference();
        short status = primeApi.Get_Table(JPrimeLoader.getDosString(tableName), lfHandle.getValue(), lrHandle);
        if (status != 0) throw new PrimeReadException("err Get_Table=" + status);
        try {
            dumpTable(wsFilename, lrHandle, tableName);
        } finally {
            primeApi.DoneHandle(lrHandle);
        }
    }

    private void dumpTable(String wsFilename, PointerByReference lrHandle, String tableName) throws PrimeReadException {
        List<PrimeFieldMetaInfo> fieldMetaInfoList = JPrimeLoader.getFieldList(primeApi, lrHandle);
        short status = primeApi.Goto_BeginObj(lrHandle.getValue());
        if (status != 0) throw new PrimeReadException("err Goto_FastBeginObj " + status);
        short err = 0;
        do {
            PointerByReference objectHandle = new PointerByReference();
            status = primeApi.GetCurObj(lrHandle.getValue(), objectHandle);
            if (status != 0) throw new PrimeReadException("err GetFastCurObj=" + status);
            try {
                dumpObject(wsFilename, objectHandle.getValue(), fieldMetaInfoList);
            } finally {
                primeApi.DoneHandle(objectHandle);
            }
        } while((err = primeApi.Goto_NextObj(lrHandle.getValue())) == 0);
    }

    private void dumpObject(String wsFilename, Pointer primeObject, List<PrimeFieldMetaInfo> fieldMetaInfoList) throws PrimeReadException {
        PointerByReference arrHandle = new PointerByReference();
        short err = primeApi.GetArrayByName(arrHandle, "LAS_ARRAY", primeObject);
        if (err != 0) throw new PrimeReadException("GetArrayByIndex=" + err);
        List<PrimeFieldMetaInfo> fieldList = null;
        try {
            fieldList = JPrimeLoader.getFieldList(primeApi, arrHandle);

            float[][] arr = getAllFloats(arrHandle, fieldList);
            for (int i = 0; i < fieldList.size() ; i++) {
                PrimeFieldMetaInfo field = fieldList.get(i);
                if (!field.getFieldName().equals("ГЛУБИНА")) {
                    System.err.print("INSERT INTO logdb_info VALUES (");
                    out(wsFilename);
                    out(primeObject, "UWI");
                    out(primeObject, "РАЗДЕЛ");
                    out(primeObject, "ПУТЬ_ФАЙЛА");
                    out(primeObject, "ИМЯ_ФАЙЛА");
//                    outI(primeObject, "НОМЕР_ВЕРСИИ");
                    outF(primeObject, "КРОВЛЯ_КРИВОЙ_ГИС");
                    outF(primeObject, "ПОДОШВА_КРИВОЙ_ГИС");
                    outF(primeObject, "ШАГ_ДИСКРЕТ_КРИВОЙ");
                    out(field.getFieldName());
                    Float nv  = Float.parseFloat(PrimeFieldExtractor.getString(primeApi, primeObject, "NULL"));
                    Float[] topBase = findTopBase(arr[i], nv);
                    Float[] startStop = findStartStop(arr, nv);
                    out(topBase[0]);
                    out(topBase[1]);
                    out(startStop[0]);
                    out(startStop[1]);
                    System.err.println(");");
                }
            }
        } finally {
            primeApi.DoneHandle(arrHandle);
        }
    }

    private Float[] findStartStop(float[][] arr, Float nv) {
        float nullValue = nv != null ? nv.floatValue() : -999f;
        Float minStart = null;
        Float maxStop = null;
        for (int i = 1; i < arr.length; i++) {
            Float start = null;
            float end = 0;
            for (int j = 0; j < arr[i].length; j++) {
                float value = arr[i][j];
                boolean notNullValue = !isNullValue(value, nullValue);
                if (notNullValue && start == null) {
                    start = arr[0][j];
                }
                if (notNullValue) end = arr[0][j];
            }
            if (minStart == null || start < minStart) minStart = start;
            if (maxStop == null || end > maxStop) maxStop = end;
        }
        return new Float[] {minStart, maxStop};
    }


    private float[][] getAllFloats(PointerByReference arrHandle, List<PrimeFieldMetaInfo> fieldList) {
        int arrLength = primeApi.ArrayGetLen(arrHandle.getValue());
        float[][] result = new float[fieldList.size()][];
        for (int i = 0; i < fieldList.size(); i++) {
            PrimeFieldMetaInfo metaInfo = fieldList.get(i);
            int index = metaInfo.getIndex();
            short columnNumber = (short) (index + 1);
            if (metaInfo.getType() == PrimeFieldExtractor.TYPE_FLOAT) {
                float[] floats = JPrimeLoader.getBigFloatArray(arrHandle.getValue(), columnNumber, arrLength, primeApi);
                result[i] = floats;
            }
        }
        return result;
    }


    private Float[] findTopBase(float[] floats, Float nv) {
        float nullValue = nv != null ? nv.floatValue() : -999f;
        Float start = null;
        float end = 0;
        for (int j = 0; j < floats.length; j++) {
            float value = floats[j];
            boolean notNullValue = !isNullValue(value, nullValue);
            if (notNullValue && start == null) {
                start = value;
            }
            if (notNullValue) end = value;
        }
        return new Float[]{start, new Float(end)};
    }

    private boolean isNullValue(float value, float nullValue) {
        return value == nullValue || value == -999.25f;
    }

    private void out(Pointer primeObject, String uwi) throws PrimeReadException {
        out(PrimeFieldExtractor.getString(primeApi, primeObject, uwi));
    }

    private void outI(Pointer primeObject, String uwi) throws PrimeReadException {
        out(PrimeFieldExtractor.getLong(primeApi, primeObject, uwi));
    }

    private void outF(Pointer primeObject, String uwi) throws PrimeReadException {
        out(PrimeFieldExtractor.getFloat(primeApi, primeObject, uwi));
    }

    private void out(Object object) {
        String msg = object != null ? object.toString() : "null";
        if (object instanceof String) {
            System.err.print("'" + msg + "'" + DELEMITER);
        } else {
            System.err.print(msg + DELEMITER);
        }
    }
}
