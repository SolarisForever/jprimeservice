package ru.navilab.jprime.service;

import java.io.File;
import java.io.Serializable;

import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.je.Cursor;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;
import ru.navilab.common.Debug;

/**
 * @author Mikhailov_KG
 *         <p/>
 *         Singleton шаблон не используется намеренно
 *         <p/>
 *         Created: 31 марта 2014 г.
 */
public class LocalDb {
    private static final String CLASS_DB = "Class";
    private static final String DATABASE_DIRECTORY = "db";
    private DatabaseConfig dbConfig;
    private EnvironmentConfig envConfig;
    private Database database;
    private Database classDb;
    private Environment environment;
    private DatabaseEntry theKey = new DatabaseEntry();
    private DatabaseEntry theData = new DatabaseEntry();
    private StoredClassCatalog storedClassCatalog;
    private String dbName;

    public LocalDb(String dbName) {
        this.dbName = dbName;
        dbConfig = new DatabaseConfig();
        dbConfig.setAllowCreate(true);
        dbConfig.setDeferredWrite(true);

        envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        File dbDir = new File(DATABASE_DIRECTORY);
        dbDir.mkdirs();
        environment = new Environment(dbDir, envConfig);
        database = environment.openDatabase(null, dbName, dbConfig);
        classDb = environment.openDatabase(null, dbName + "_" + CLASS_DB, dbConfig);
        storedClassCatalog = new StoredClassCatalog(classDb);
    }

    public void close() {
        classDb.close();
        database.close();
        environment.close();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.close();
    }

    public void put(Serializable key, Serializable data) {
        SerialBinding keyBinding = new SerialBinding(storedClassCatalog, key.getClass());
        keyBinding.objectToEntry(key, theKey);
        SerialBinding dataBinding = new SerialBinding(storedClassCatalog, data.getClass());
        dataBinding.objectToEntry(data, theData);
        database.put(null, theKey, theData);
    }

    public Object get(Serializable key, Class clazz) {
        SerialBinding keyBinding = new SerialBinding(storedClassCatalog, key.getClass());
        keyBinding.objectToEntry(key, theKey);
        theData.setSize(0);
        database.get(null, theKey, theData, LockMode.DEFAULT);
        SerialBinding dataBinding = new SerialBinding(storedClassCatalog, clazz);
        return dataBinding.entryToObject(theData);
    }

    public void clean() {
        Debug.getLogger(this).fine("clean database " + dbName);
        final Cursor cursor = database.openCursor(null, null);
        final DatabaseEntry valueEntry = new DatabaseEntry();
        final DatabaseEntry keyEntry = new DatabaseEntry();
        keyEntry.setPartial(0, 0, true);
        OperationStatus status = cursor.getNext(keyEntry, valueEntry,
                LockMode.DEFAULT);
        try {
            while (status == OperationStatus.SUCCESS) {
                status = cursor.getNext(keyEntry, valueEntry, LockMode.DEFAULT);
                if (status == OperationStatus.SUCCESS) database.delete(null, keyEntry);
            }
        } catch (final DatabaseException e) {
            e.printStackTrace();
        }

        cursor.close();
    }
}