package ru.navilab.jprime.service;

import ru.navilab.jprime.loader.JPrimeScanTest;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 * Created by Mikhailov_KG on 17.12.2015.
 */
@Provider
public class JPrimeScanTestProvider implements ContextResolver<JPrimeScanTest> {
    private JPrimeScanTest jPrimeScanTest;

    public JPrimeScanTestProvider() {
        jPrimeScanTest = new JPrimeScanTest();
    }

    @Override
    public JPrimeScanTest getContext(Class<?> aClass) {
        return jPrimeScanTest;
    }
}
