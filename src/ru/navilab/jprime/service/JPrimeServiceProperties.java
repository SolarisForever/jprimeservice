package ru.navilab.jprime.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Mikhailov_KG on 20.01.2016.
 */
public class JPrimeServiceProperties {
    private static JPrimeServiceProperties instance;
    private final Properties properties;

    private JPrimeServiceProperties() throws IOException {
        properties = new Properties();
        properties.load(new FileInputStream("jprimeservice.ini"));
    }

    public static JPrimeServiceProperties getInstance() throws IOException {
        if (instance == null) instance = new JPrimeServiceProperties();
        return instance;
    }

    public synchronized Properties getProperties() {
        return properties;
    }
}
