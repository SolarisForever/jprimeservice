package ru.navilab.jprime.service;

import ru.navilab.jprime.loader.IPrimeLoader;
import ru.navilab.jprime.loader.v2.JniPrimeLoader;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 * Created by Mikhailov_KG on 15.01.2016.
 */
@Provider
public class JPrimeLoaderProvider implements ContextResolver<IPrimeLoader> {
    private final static IPrimeLoader primeLoader = new JniPrimeLoader();

    public JPrimeLoaderProvider() {
    }

    @Override
    public IPrimeLoader getContext(Class<?> aClass) {
        return primeLoader;
    }
}