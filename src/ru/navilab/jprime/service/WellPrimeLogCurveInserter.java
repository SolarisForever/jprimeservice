package ru.navilab.jprime.service;

import ru.navilab.jprime.loader.GeoproConnection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.*;

/**
 * Created by Mikhailov_KG on 29.06.2015.
 */
public class WellPrimeLogCurveInserter {
    public static final String NIPI42_WELL_PRIME_LOG_CURVE = "NIPI42.WELL_PRIME_LOG_CURVE";
    private final Connection connection;
    private final PreparedStatement pstmt;
    private final JDBCDeleteHelper deleteHelper;
    private  int primeDatabaseId;
    private int index;

    public WellPrimeLogCurveInserter() throws SQLException {
        GeoproConnection geoproConnection = new GeoproConnection();
        connection = geoproConnection.createConnection();
        connection.setAutoCommit(false);
        pstmt = connection.prepareStatement(String.format("INSERT INTO %s" +
                "(section, uwi, trace_type, top, base, lasname, prime_db, " +
                "curve_data_count, curve_data, version, frac_offset, " +
                "direction, operation_number, step, operator, well_name, " +
                "null_value, log_date, log_time, activity_id, quality, wsfile) VALUES "+
                "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", NIPI42_WELL_PRIME_LOG_CURVE));

        deleteHelper = new JDBCDeleteHelper(connection);
    }

    public void setPrimeDatabaseId(int primeDatabaseId) {
        this.primeDatabaseId = primeDatabaseId;
    }

    public void close() throws SQLException {
        connection.commit();
        pstmt.close();
        connection.close();
    }

    public void delete(String wsFileAbsolutePath) throws SQLException {
        deleteHelper.delete(wsFileAbsolutePath);
    }

    public void executeBatch() throws SQLException {
        pstmt.executeBatch();
    }

    public void mayCommit() throws SQLException {
        if ((index++ % 5) == 0) connection.commit();
    }

    public void insert(LogInsertObj logInsertObj) throws SQLException, IOException {
        String section = logInsertObj.getSection();
        setString(1, section == null ? "LOG" : section);
        pstmt.setString(2, logInsertObj.getUwi());
        pstmt.setString(3, logInsertObj.getCurveName());
        pstmt.setFloat(4, logInsertObj.getStart());
        pstmt.setFloat(5, logInsertObj.getStop());
        pstmt.setString(6, logInsertObj.getLasFilename());
        pstmt.setInt(7, primeDatabaseId); // Finder1
        float[] floatValues = logInsertObj.getFloatValues();
        pstmt.setInt(8, floatValues.length);
        byte[] bytes = createBytesArray(floatValues);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        pstmt.setBinaryStream(9, inputStream);

        pstmt.setInt(10, logInsertObj.getVersion());
        setFloat(11, logInsertObj.getFracOffset());
        setString(12, logInsertObj.getDirection());
        setLong(13, logInsertObj.getOperationNumber());
        setFloat(14, logInsertObj.getStep()); // ШАГ_ДИСКРЕТ_КРИВОЙ
        setString(15, logInsertObj.getOperator());
        setString(16, logInsertObj.getWellName());
        float nullValue = Float.parseFloat(logInsertObj.getNullValueString());
        pstmt.setFloat(17, nullValue);
        pstmt.setDate(18, logInsertObj.getLogDate());
        pstmt.setTimestamp(19, logInsertObj.getLogTime());
        setLong(20, logInsertObj.getActivityId());
        setString(21, logInsertObj.getQuality());
        setString(22, logInsertObj.getWsFilename());

        pstmt.addBatch();

        inputStream.close();
    }

    private void setString(int i, String s) throws SQLException {
        if (s != null) pstmt.setString(i, s);
        else pstmt.setNull(i, Types.NULL);
    }

    private void setFloat(int i, Float v) throws SQLException {
        if (v != null) pstmt.setFloat(i, v);
        else pstmt.setNull(i, Types.NULL);
    }

    private void setLong(int i, Long v) throws SQLException {
        if (v != null) pstmt.setLong(i, v);
        else pstmt.setNull(i, Types.NULL);
    }

    private byte[] createBytesArray(float[] values) throws IOException {
        ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(byteArrayStream);
        for (int i = 0; i < values.length; i++) out.writeFloat(values[i]);
        out.close();
        byte[] bytes = byteArrayStream.toByteArray();
        byteArrayStream.close();
        return bytes;
    }
}
