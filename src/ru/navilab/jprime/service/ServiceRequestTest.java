package ru.navilab.jprime.service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import ru.navilab.common.utils.StopWatch;
import ru.navilab.jprime.data.IWellLogTable;
import ru.navilab.jprime.data.IWellLogTables;
import ru.navilab.matrix.engine.Matrix;
import ru.navilab.matrix.engine.MatrixCell;
import ru.navilab.matrix.engine.MatrixColumnType;
import ru.navilab.matrix.entity.MatrixEntity;
import ru.navilab.matrix.entity.MatrixEntityField;

import javax.ws.rs.core.MediaType;
import java.io.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Mikhailov_KG on 17.12.2015.
 */
public class ServiceRequestTest {
    public static final String HTTP_LOCALHOST_9696_JAXRS_PRIME = "http://localhost:9696/jaxrs/prime/";
    private StopWatch stopWatch;
    static FilenameFilter wsFilenameFilter = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.toUpperCase().endsWith(".WS");
        }
    };

    public ServiceRequestTest() {

    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        if (args.length == 1) new ServiceRequestTest().test(args[0]);
        else new ServiceRequestTest().getWsFile(args[0], args[1], args[2], args[3]);
    }


    private void test(String filedir) {
        stopWatch = new StopWatch("all");
        File rootDir = new File(filedir);
        try {
            scanDir(rootDir);
        } catch (PrimeReadException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            stopWatch.stop();
        }
    }

    private void scanDir(File file) throws PrimeReadException, SQLException, IOException, ClassNotFoundException {
        File[] files = file.listFiles(wsFilenameFilter);
        for (File wsFile : files) {
            System.err.println(wsFile.getAbsolutePath());
            String absolutePath = wsFile.getAbsolutePath();
            testWsFile(absolutePath);
        }
        File[] dirFiles = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return (pathname.isDirectory());
            }
        });
        for (File dirFile : dirFiles) {
            scanDir(dirFile);
        }
        stopWatch.stop(file.getName());
    }

    private void testWsFile(String absolutePath) throws IOException, ClassNotFoundException {
        ClientConfig cc = new DefaultClientConfig();
        cc.getProperties().put(
                ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true);
        Client c = Client.create(cc);
        WebResource r = c.resource(HTTP_LOCALHOST_9696_JAXRS_PRIME).path("getwsfile");
        r = r.queryParam("wsfile", absolutePath);
        ClientResponse clientResponse = r.accept(MediaType.APPLICATION_OCTET_STREAM).post(ClientResponse.class);
        InputStream inputStream = clientResponse.getEntityInputStream();
        ObjectInputStream dataInputStream = new ObjectInputStream(inputStream);
        Matrix matrix = (Matrix) dataInputStream.readObject();
//        dataInputStream.close();
//        clientResponse.close();
        System.err.println("matrix " + matrix.getRowCount() + " " + matrix.getColumnCount());
    }

    private void getWsFile(String field, String fieldPool, String wellNameRu, String uwi) throws IOException, ClassNotFoundException {
        ClientConfig cc = new DefaultClientConfig();
        cc.getProperties().put(
                ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true);
        Client c = Client.create(cc);
        WebResource r = c.resource(HTTP_LOCALHOST_9696_JAXRS_PRIME).path("getall");
        r = r.queryParam("field", field);
        r = r.queryParam("fieldPool", fieldPool);
        r = r.queryParam("wellNameRu", wellNameRu);
        r = r.queryParam("uwi", uwi);
        ClientResponse clientResponse = r.accept(MediaType.APPLICATION_OCTET_STREAM).post(ClientResponse.class);
        InputStream inputStream = clientResponse.getEntityInputStream();
        ObjectInputStream dataInputStream = new ObjectInputStream(inputStream);
        IWellLogTables wellLogTables = (IWellLogTables) dataInputStream.readObject();
        List<IWellLogTable> wellLogTableList = wellLogTables.getWellLogTableList();
        System.err.println("table count " + wellLogTableList.size());
        for (IWellLogTable wellLogTable : wellLogTableList) {
            String file = wellLogTable.getWsfile();
            Matrix matrix = wellLogTable.getMatrix();
            System.err.println(String.format("%s\t%d\t%d", file, matrix.getRowCount(),matrix.getColumnCount()));
            write(matrix);
        }
    }

    public void write(Matrix matrix) {
        System.err.println("Matrix rows=" + matrix.getRowCount());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            int ex = matrix.getRowCount();
            int columnCount = matrix.getColumnCount();
            MatrixEntity entity = matrix.getEntity();
            List<MatrixEntityField> fieldList = entity.getFieldList();
            for(int c = 0; c < fieldList.size(); ++c) {
                System.err.print(fieldList.get(c).getName() + "\t");
            }
            System.err.println();
            for(int r = 0; r < ex; ++r) {
                for(int c = 0; c < columnCount; ++c) {
                    MatrixCell cell = matrix.getCell(r, c);
                    MatrixColumnType columnType = cell.getColumnType();
                    boolean isDate = columnType == MatrixColumnType.DATE;
                    if (columnType == MatrixColumnType.OBJECT) {
                        System.err.println();
                        write((Matrix) cell.getMatrixObjectValue());
                    }
                    else {
                        String s = !cell.isNull() && isDate ? dateFormat.format(cell.getDateValue()) : cell.convertToStringValue();
                        System.err.print(s + (c + 1 < columnCount ? "\t" : ""));
                    }
                }
                System.err.println();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
