package ru.navilab.jprime.service;

import ru.navilab.matrix.BindKey;
import ru.navilab.matrix.IBindParameter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mikhailov_KG on 20.01.2016.
 */
public class KeyValueBinding {
    private Pattern pattern = Pattern.compile("\\{(.*?)\\}");

    public String bind(String patternString, Map<String,String> keyValueMap) {
        List<String> keysNames = new ArrayList<String>(keyValueMap.keySet());
        Matcher m = pattern.matcher(patternString);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            boolean tagFind = false;
            String tagName = m.group(1).toUpperCase();
            for (String keyName : keysNames) {
                if (tagName.equalsIgnoreCase(keyName)) {
                    String value = keyValueMap.get(keyName);
                    m.appendReplacement(sb, value);
                    tagFind = true;
                    break;
                }
            }
            if (!tagFind) throw new RuntimeException("tag " + tagName + " is unknown");
        }
        m.appendTail(sb);
        return sb.toString();
    }
}
