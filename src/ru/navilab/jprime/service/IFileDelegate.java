package ru.navilab.jprime.service;

/**
 * Created by Mikhailov_KG on 20.01.2016.
 */
public interface IFileDelegate {
    void fileFound(String file);
}
