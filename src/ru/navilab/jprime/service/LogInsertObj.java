package ru.navilab.jprime.service;

import com.sun.jna.Pointer;
import ru.navilab.jprime.loader.PrimeFieldExtractor;

import java.sql.Date;
import java.sql.Timestamp;

public class LogInsertObj {
    private final String curveName;
    private final float[] floatValues;
    private final String wsFilename;
    private final int version;
    private final float start;
    private final float stop;
    private final float step;
    private final int dataType;
    private final String section;
    private final String uwi;
    private final String lasFilename;
    private final Long operationNumber;
    private final String operator;
    private final String wellName;
    private final String nullValueString;
    private final Date logDate;
    private final Timestamp logTime;
    private final Long activityId;
    private final String quality;
    private Float fracOffset;
    private String direction;

    public LogInsertObj(PrimeApi primeApi, Pointer primeObject, String curveName, float[] floatValues, String wsFilename, int version, float start, float stop, float step, int dataType) throws PrimeReadException {
        this.curveName = curveName;
        this.floatValues = floatValues;
        this.wsFilename = wsFilename;
        this.version = version;
        this.start = start;
        this.stop = stop;
        this.step = step;
        this.dataType = dataType;
        section = PrimeFieldExtractor.getString(primeApi, primeObject, "РАЗДЕЛ");
        uwi = PrimeFieldExtractor.getString(primeApi, primeObject, "UWI");
        lasFilename = PrimeFieldExtractor.getString(primeApi, primeObject, "ИМЯ_ФАЙЛА");
        fracOffset = PrimeFieldExtractor.getFloat(primeApi, primeObject, "ГРП_СМЕЩЕНИЕ");
        direction = PrimeFieldExtractor.getString(primeApi, primeObject, "НАПРАВЛЕНИЕ_ЗАМЕРА");
        operationNumber = PrimeFieldExtractor.getLong(primeApi, primeObject, "НОМЕР_С/П");
        operator = PrimeFieldExtractor.getString(primeApi, primeObject, "ПОДРЯДЧИК");
        wellName = PrimeFieldExtractor.getString(primeApi, primeObject, "СКВАЖИНА");
        nullValueString = PrimeFieldExtractor.getString(primeApi, primeObject, "NULL");
        logDate = toDate(PrimeFieldExtractor.getDate(primeApi, primeObject, "ДАТА_ИССЛЕДОВАНИЯ"));
        logTime = toTimestamp(PrimeFieldExtractor.getTime(primeApi, primeObject, "ВРЕМЯ_НАЧАЛА"));
        activityId = PrimeFieldExtractor.getLong(primeApi, primeObject, "ACTIVITY_ID");
        quality = PrimeFieldExtractor.getString(primeApi, primeObject, "КАЧЕСТВО_ДАННЫХ");
    }

    private Timestamp toTimestamp(java.util.Date utilTime) {
        if (utilTime == null) return null;
        else return new Timestamp(utilTime.getTime());
    }

    private Date toDate(java.util.Date utilDate) {
        return new Date(utilDate.getTime());
    }


    public String getCurveName() {
        return curveName;
    }

    public float[] getFloatValues() {
        return floatValues;
    }

    public String getWsFilename() {
        return wsFilename;
    }

    public int getDataType() {
        return dataType;
    }

    public float getStart() {
        return start;
    }

    public float getStep() {
        return step;
    }

    public float getStop() {
        return stop;
    }

    public int getVersion() {
        return version;
    }

    public String getSection() {
        return section;
    }

    public String getUwi() {
        return uwi;
    }

    public String getLasFilename() {
        return lasFilename;
    }

    public Float getFracOffset() {
        return fracOffset;
    }

    public String getDirection() {
        return direction;
    }

    public Long getOperationNumber() {
        return operationNumber;
    }

    public Long getActivityId() {
        return activityId;
    }

    public Date getLogDate() {
        return logDate;
    }

    public Timestamp getLogTime() {
        return logTime;
    }

    public String getNullValueString() {
        return nullValueString;
    }

    public String getOperator() {
        return operator;
    }

    public String getQuality() {
        return quality;
    }

    public String getWellName() {
        return wellName;
    }
}
