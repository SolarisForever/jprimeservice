package ru.navilab.jprime.service;

import com.sun.jna.Pointer;

import java.nio.charset.Charset;

/**
 * Created by Mikhailov_KG on 12.12.2015.
 *
 * <pre>
 *
 * typedef struct {
 * NameStr afldName;
 * unsigned char aRC;
 * NameStr afldUnitName;
 * short aLen;
 * short aDec;
 * MnemoStr aMin;
 * MnemoStr aMax;
 * unsigned short aMask;
 * PathStr fldComment;
 * } TCFDData, *PCFDData; // Dsapi.h:194
 *
 *
 *         Константы версии формата данных !!! // Dsapi.h:176
 *         #define MaxNameLen 32
 *         #define MnemoLen   8+1
 *         #define FNameLen   12
 *         #define ShortLen   20+1
 *         #define LongLen    60+1
 *         #define MaxPathLen 81
 *
 *         typedef char NameStr[MaxNameLen+1];
 *         typedef char FNameStr[FNameLen+1];
 *         typedef char ShortStr[ShortLen+1];
 *         typedef char PathStr[MaxPathLen+1];
 *         typedef char MnemoStr[MnemoLen+1];
 *
 * </pre>
 */
public class PFDData {
    private static final int PATH_LEN = 82;
    private static final int NAME_LEN = 33;
    private static final int MNEMO_LEN = 9;
    public static final String CP_866 = "CP866";
    private final String fieldName;
    private final byte type;
    private final String fieldUnit;
    private final String fieldComment;

    public PFDData(Pointer pfdData) {
        int offset = 0;
        fieldName = new String(pfdData.getByteArray(offset, NAME_LEN), Charset.forName(CP_866)).trim();
        offset += NAME_LEN;
        type = pfdData.getByte(offset);
        offset++;
        fieldUnit = new String(pfdData.getByteArray(offset, NAME_LEN), Charset.forName(CP_866)).trim();
        offset += NAME_LEN;
        offset += 2 * 2;
        offset += MNEMO_LEN * 2;
        offset += 2;
        fieldComment = new String(pfdData.getByteArray(offset, PATH_LEN), Charset.forName(CP_866)).trim();
    }

    public String getFieldName() {
        return fieldName;
    }

    public byte getType() {
        return type;
    }

    public String getFieldUnit() {
        return fieldUnit;
    }

    public String getFieldComment() {
        return fieldComment;
    }
}
