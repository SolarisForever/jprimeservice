package ru.navilab.jprime.service;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.*;

/**
 * Created by Mikhailov_KG on 17.06.2015.
 */
public interface PrimeApi extends Library {
    short SysLFInitOpen(String filename, PointerByReference lfHandle);
    int DSGetTableNames(Pointer lfHandle, short lrOption, PointerByReference tablesNames);
// unicode UTF-16LE    int DSGetTableNamesW(Pointer lfHandle, short lrOption, PointerByReference tablesNames);
    short GetCurTable(Pointer lfHandle, PointerByReference lrHandle);
    short GetKeyStr(Pointer objectHandle, short id, byte length, Pointer string);
    short GetMetaKeyStr(Pointer objectHandle, byte length, Pointer string);
    short GetObValByIndex(int index, Pointer objectHandle, PointerByReference valueHandler);
    short ObValGetType(Pointer valueHandler, ByteByReference typeRef);
    //short GetWSName(Pointer lrHandle, PointerByReference wsName);

    short Get_Table(Pointer tableName, Pointer lfHandle, PointerByReference lrHandle);
    //short Goto_FastBeginObj(Pointer lrHandle);
    short Goto_BeginObj(Pointer lrHandle);
    //short Goto_FastNextObj(Pointer lrHandle);
    short Goto_NextObj(Pointer lrHandle);
    //short GetFastCurObj(Pointer lrHandle, PointerByReference objectHandle);
    short GetCurObj(Pointer lrHandle, PointerByReference objectHandle);
    short GetPackKeyStr(Pointer objectHandle, PointerByReference keyString);

    short GetTDescF(String fileName, Pointer tablName, PointerByReference tableDescHandle);

    Pointer DSFindCurvesMatchingConditionsByWSName(String wsFilename,
                                                   Pointer tableString, String condition,
                                                   int options, IntByReference p1, long l1, Pointer p2, Pointer p3);

    int DSListCount(Pointer list);

    int DSGetCurveDataFromList(Pointer list, int index, PointerByReference objectRef, PointerByReference arrayRef,
                               IntByReference ncol, FloatByReference start, FloatByReference stop,
                               FloatByReference step, IntByReference dataType, IntByReference verNumber,
                               IntByReference planNamesLen, IntByReference tableNameLen,
                               IntByReference curveNameLen, IntByReference keyStrLen,
                               PointerByReference planName, PointerByReference tableName, PointerByReference curveName, PointerByReference keyStr);

    int ArrayGetLen(Pointer array);

    float ArrayGetSingleColumnData(Pointer object, short column, int index);

    void DSFreeMemLib(Pointer pointer, int size);

    void DSDoneObject(Pointer object);

    short DoneHandle(PointerByReference handleRef);

    short GetFieldVal(Pointer name, Pointer object, PointerByReference handlerRef, ByteByReference typeRef);

    short ObValGetStr(Pointer handler, Pointer str);

    short ObValGetSize(Pointer handler, boolean isStANSI, IntByReference sizeRef);

    long ObValGetLongInt(Pointer handler);

    float ObValGetSingle(Pointer handler);

    short ObValGetPointer(Pointer value, PointerByReference pointerRef);

    short GetTDescFldsI(Pointer objectHandle, short index, Pointer pfdData);

    short GetTDescCounter(Pointer objectHandle, ShortByReference counterRef);

    short GetArrayByIndex(PointerByReference arrHandle, short index, Pointer objectHandle);

    short GetArrayByName(PointerByReference arrHandle, String name, Pointer objectHandle);

    short ArrayGetColumns(Pointer value, ShortByReference columnCountRef);

    short ArrayGetDesc(Pointer value, PointerByReference tableDescHandle);

    short LRGetObjCount(Pointer value, IntByReference lrObjectCount);

    short GetObjByIdN(Pointer value, short index, PointerByReference objectHandle);

    /**
     *
     * @param columnNumber from 1 to N
     */
    short ArrayGetSingleColData(Pointer arrayHandle, short columnNumber, int fromIndex, int arrLength, Pointer buffer);
    short ArrayGetSingleColData2(Pointer arrayHandle, short columnNumber, int fromIndex, int arrLength, Pointer buffer);

    short GetArrayString(Pointer arrHandle, int index, Pointer columnName, Pointer stringPointer);

    short ArrayGetStrColumnData(Pointer arrHandle, byte columnNumber, int index, Pointer stringPointer);

//   short ArrayGetData(Pointer arrHandle, short index, long arrLength, Pointer buffer);

//    short GetWSHandle(Pointer lrHandle, PointerByReference wsHandle);
}
