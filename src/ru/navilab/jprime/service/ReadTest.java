package ru.navilab.jprime.service;

import ru.navilab.jprime.loader.GeoproConnection;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Mikhailov_KG on 25.06.2015.
 */
public class ReadTest {
    private final Connection connection;
    private final static String TAB = "\t";

    ReadTest() throws SQLException {
        connection = new GeoproConnection().createConnection();
    }

    public static void main(String[] args) throws Exception {
        new ReadTest().test();
    }

    private void test() throws SQLException, IOException {

        PreparedStatement pstmt = connection.prepareStatement("select * from NIPI42.WELL_PRIME_LOG_CURVE");
        ResultSet rs = pstmt.executeQuery();

        while(rs.next()) {
            long d1 = System.currentTimeMillis();
            String uwi = rs.getString("UWI");
            String traceType = rs.getString("TRACE_TYPE");
            int curveDataCount = rs.getInt("CURVE_DATA_COUNT");
            InputStream binaryStream = rs.getBinaryStream("CURVE_DATA");
            DataInputStream inputStream = new DataInputStream(binaryStream);
            float[] values = new float[curveDataCount];
            for (int i = 0; i < curveDataCount; i++) {
                values[i] = inputStream.readFloat();
            }

            float sec = (System.currentTimeMillis() - d1) / 1000f;
            System.err.println("uwi\t" + uwi + TAB + traceType + TAB + values.length + TAB + values[0] + TAB + sec);
        }
        rs.close();
        pstmt.close();
        connection.close();
    }
}
