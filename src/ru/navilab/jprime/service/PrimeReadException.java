package ru.navilab.jprime.service;

/**
 * Created by Mikhailov_KG on 17.06.2015.
 */
public class PrimeReadException extends Throwable {
    private int status;

    public PrimeReadException(String reason) {
        super(reason);
    }
    public PrimeReadException(String reason, int status) {
        super(reason + " " + status);
        this.status = status;
    }
}
