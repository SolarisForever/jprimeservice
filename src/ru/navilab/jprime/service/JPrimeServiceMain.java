package ru.navilab.jprime.service;

import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;
import org.boris.winrun4j.AbstractService;
import org.boris.winrun4j.ServiceException;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by Mikhailov_KG on 17.12.2015.
 */
public class JPrimeServiceMain extends AbstractService {
    public static final String URL = "http://localhost:9696/jaxrs";
    private Logger logger = Logger.getLogger(this.getClass().getName());

    public static void main(String[] args) {
        createStartThread().start();
    }

    public int serviceMain(String[] args) throws ServiceException {
        try {
            logger.info("start service using url " + URL);
            final HttpServer httpServer = HttpServerFactory.create(URL);
            if (!shutdown) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        logger.fine("before httpServer start");
                        httpServer.start();
                        logger.fine("after httpServer start");
                    }
                }).start();
            }
            while (!shutdown) {
                try {
                    Thread.sleep(6000);
                } catch (InterruptedException e) {
                }
            }
            logger.info("stop service");
            httpServer.stop(0);
        } catch (IOException e) {
            logger.severe("error in service " + e.getMessage());
            throw new ServiceException(e);
        }
        return 0;
    }

    private static Thread createStartThread() {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpServer httpServer = HttpServerFactory.create(URL);
                    httpServer.start();
                } catch (Exception e) {
                    System.err.println("JAXRS CRASH " + e.getMessage());
                    e.printStackTrace();
                    System.err.println("JAXRS RESTART");
                }
            }
        });
    }
}
