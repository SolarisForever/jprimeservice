package ru.navilab.jprime.service;

import java.io.Serializable;

/**
 * Created by Mikhailov_KG on 29.06.2015.
 */
public class WsFileStat implements Serializable {
    private long timestamp;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
