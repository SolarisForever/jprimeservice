package ru.navilab.jprime.service;

/**
 * Created by Mikhailov_KG on 17.12.2015.
 */
public class PrimeFieldMetaInfo {
    private short index;
    private String fieldName;
    private byte type;
    private String fieldUnit;
    private String fieldComment;

    public PrimeFieldMetaInfo(short index, String fieldName, byte type, String fieldUnit, String fieldComment) {
        this.index = index;
        this.fieldName = fieldName;
        this.type = type;
        this.fieldUnit = fieldUnit;
        this.fieldComment = fieldComment;
    }

    public short getIndex() {
        return index;
    }

    public String getFieldName() {
        return fieldName;
    }

    public byte getType() {
        return type;
    }

    public String getFieldUnit() {
        return fieldUnit;
    }

    public String getFieldComment() {
        return fieldComment;
    }
}
