package ru.navilab.jprime.service;

import com.sun.jersey.api.Responses;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Mikhailov_KG on 15.01.2016.
 */
public class JaxrsDataException extends WebApplicationException {
    public JaxrsDataException(String message) {
        super(Response.status(Responses.CLIENT_ERROR).
                entity(message).
                type(MediaType.TEXT_PLAIN_TYPE).build());
    }
}
