/*
 * Copyright (c) 2018. Navilab.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY RESPONSIBILITY OF THE AUTHOR.
 */

package ru.navilab.jprime.service;

/**
 * Created by Mikhailov_KG on 17.12.2015.
 */

import ru.navilab.jprime.data.IWellLogTable;
import ru.navilab.jprime.data.IWsFile;
import ru.navilab.jprime.data.WellLogTables;
import ru.navilab.jprime.data.WsFile;
import ru.navilab.jprime.loader.IPrimeLoader;
import ru.navilab.jprime.loader.v2.JniPrimeLoader;
import ru.navilab.matrix.engine.Matrix;
import ru.navilab.matrix.engine.MatrixRow;
import ru.navilab.matrix.engine.MatrixStructNotEqualException;
import ru.navilab.matrix.entity.MatrixEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.ext.ContextResolver;
import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Path("/prime")
public class JPrimeService {

    public static final String PRIME_LOGDB_PATTERN = "prime.logdb.pattern";
    public static final String FIELD = "field";
    public static final String FIELDPOOL = "fieldPool";
    public static final String WELL_NAME_RU = "wellNameRu";
    public static final String UWI = "uwi";
    public static final String PRIME_FIELDPOOL_PREFIX = "prime.field_pool_prefix";
    public static final String PRIME_LOGDB_PARSE = "prime.logdb.parse";
    private Logger logger = Logger.getLogger(this.getClass().getName());
    static FilenameFilter wsFilenameFilter = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.toUpperCase().endsWith(".WS");
        }
    };

    @POST
    @Path("/getwsfile")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getall(@QueryParam("wsfile") String wsfile,
                           @Context ContextResolver<IPrimeLoader> primeLoaderContextResolver
    ) {
        try {
            logger.info("wsfile = " + wsfile);
            final IPrimeLoader loader = primeLoaderContextResolver.getContext(JniPrimeLoader.class);
            WellLogTables logTables = loader.load(wsfile);
            return Response.ok().entity(send(logTables)).type(MediaType.APPLICATION_OCTET_STREAM).build();
        } catch (Throwable ex) {
            logger.log(Level.SEVERE, "error ", ex);
            throw new JaxrsDataException(ex.getMessage());
        }
    }

    @POST
    @Path("/getwsfiles")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getWsFiles(@QueryParam(FIELD) String field,
                           @QueryParam(FIELDPOOL) String fieldPool,
                           @QueryParam(WELL_NAME_RU) String wellNameRu,
                           @QueryParam(UWI) String uwi,
                           @Context ContextResolver<IPrimeLoader> primeLoaderContextResolver
    ) {
        try {
            final Properties properties = JPrimeServiceProperties.getInstance().getProperties();
            String patternString = properties.getProperty(PRIME_LOGDB_PATTERN);
            String fieldPoolPrefix = properties.getProperty(PRIME_FIELDPOOL_PREFIX);
            KeyValueBinding keyValueBinding = new KeyValueBinding();
            Map<String, String> map = new Hashtable<String, String>();
            String fieldDir = field.equals("Нет значения") ? fieldPoolPrefix + fieldPool : field;
            map.put(FIELD, fieldDir);
            map.put(WELL_NAME_RU, wellNameRu);
            map.put(UWI, uwi);
            String path = keyValueBinding.bind(patternString, map);
            logger.fine("path = " + path);
            final ArrayList<IWsFile> fileList = new ArrayList<IWsFile>();
            scanDir(new File(path), new IFileDelegate() {
                @Override
                public void fileFound(String file) {
                    WsFile wsfile = new WsFile();
                    wsfile.setFile(file);
                    parseFile(file, wsfile, properties.getProperty(PRIME_LOGDB_PARSE));
                    fileList.add(wsfile);
                }
            });
            return Response.ok().entity(send(fileList)).type(MediaType.APPLICATION_OCTET_STREAM).build();
        } catch (Throwable ex) {
            ex.printStackTrace();
            throw new JaxrsDataException(ex.getMessage());
        }
    }

    private void parseFile(String wsFilename, WsFile wsFile, String parsePattern) {
        Pattern pattern = Pattern.compile(parsePattern);
        Matcher matcher = pattern.matcher(wsFilename);
        if (matcher.find()) {
            String section = matcher.group(1);
            String year = matcher.group(2);
            String month = matcher.group(3);
            String day = matcher.group(4);
            wsFile.setSection(section);
            wsFile.setDate(day + "." + month + "." + year);
        }
    }

    @POST
    @Path("/getall")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getall(@QueryParam(FIELD) String field,
                           @QueryParam(FIELDPOOL) String fieldPool,
                           @QueryParam(WELL_NAME_RU) String wellNameRu,
                           @QueryParam(UWI) String uwi,
                          @Context ContextResolver<IPrimeLoader> primeLoaderContextResolver
    ) {
        try {
            Properties properties = JPrimeServiceProperties.getInstance().getProperties();
            String patternString = properties.getProperty(PRIME_LOGDB_PATTERN);
            String fieldPoolPrefix = properties.getProperty(PRIME_FIELDPOOL_PREFIX);
            KeyValueBinding keyValueBinding = new KeyValueBinding();
            Map<String, String> map = new Hashtable<String, String>();
            String fieldDir = field.equals("Нет значения") ? fieldPoolPrefix + fieldPool : field;
            logger.fine("field=" + field);
            logger.fine("fieldPool=" + fieldPool);
            logger.fine("fieldDir=" + fieldDir);
            logger.fine("wellNameRu=" + wellNameRu);
            logger.fine("uwi=" + uwi);
            map.put(FIELD, fieldDir);
            map.put(WELL_NAME_RU, wellNameRu);
            map.put(UWI, uwi);
            String path = keyValueBinding.bind(patternString, map);
            logger.fine("path = " + path);
            final IPrimeLoader loader = primeLoaderContextResolver.getContext(JniPrimeLoader.class);
            final WellLogTables wellLogTables = new WellLogTables();

            scanDir(new File(path), new IFileDelegate() {
                @Override
                public void fileFound(String file) {
                    try {
                        logger.fine("load " + file);
                        WellLogTables tables = loader.load(file);
                        List<IWellLogTable> tableList = tables.getWellLogTableList();
                        for (IWellLogTable table : tableList) {
                            wellLogTables.addLogTable(table);
                        }
                    } catch (Throwable e) {
                        throw new RuntimeException(e);
                    }
                }
            });
            return Response.ok().entity(send(wellLogTables)).type(MediaType.APPLICATION_OCTET_STREAM).build();
        } catch (Throwable ex) {
            ex.printStackTrace();
            throw new JaxrsDataException(ex.getMessage());
        }
    }

    private Matrix matrixUnion(List<Matrix> matrixList) throws MatrixStructNotEqualException {
        if (matrixList.size() == 0) return new Matrix(MatrixEntity.NULL, 0);
        Matrix first = matrixList.get(0);
        Matrix result = first.createEmptyMatrix();
        for (int i = 0; i < matrixList.size(); i++) {
            Matrix m = matrixList.get(i);
            int rowCount = m.getRowCount();
            for (int r = 0; r < rowCount; r++) {
                MatrixRow resultRow = result.newRow();
                resultRow.setRowValues(m.getRow(r));
            }
        }
        return result;
    }

    private void scanDir(File file, IFileDelegate fileDelegate) throws PrimeReadException, SQLException, IOException {
        File[] files = file.listFiles(wsFilenameFilter);
        if (files != null) {
            for (File wsFile : files) {
                String absolutePath = wsFile.getAbsolutePath();
                fileDelegate.fileFound(absolutePath);
            }
        }
        File[] dirFiles = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return (pathname.isDirectory());
            }
        });
        logger.finer("dirFiles " + dirFiles + " " + file);
        if (dirFiles != null) {
            for (File dirFile : dirFiles) {
                logger.fine("dirFile " + dirFile);
                scanDir(dirFile, fileDelegate);
            }
        }
    }

    private StreamingOutput send(final Serializable object) {
        return new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                ObjectOutputStream out = new ObjectOutputStream(outputStream);
                out.writeObject(object);
                outputStream.flush();
            }
        };
    }
}
