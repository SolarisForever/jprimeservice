package ru.navilab.jprime.service;

import ru.navilab.jprime.loader.JPrimeTest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Mikhailov_KG on 26.06.2015.
 */
public class JDBCDeleteHelper {
    private final PreparedStatement pstmt;

    public JDBCDeleteHelper(Connection connection) throws SQLException {
        pstmt = connection.prepareStatement(String.format("DELETE FROM %s WHERE wsfile=?",
                JPrimeTest.NIPI42_WELL_PRIME_LOG_CURVE));
    }

    public boolean delete(String wsfile) throws SQLException {
        pstmt.setString(1, wsfile);
        return pstmt.execute();
    }
}
