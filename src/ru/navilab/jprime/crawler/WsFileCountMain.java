package ru.navilab.jprime.crawler;

import ru.navilab.common.utils.StopWatch;

import java.io.File;

/**
 * Created by Mikhailov_KG on 23.04.2018.
 */
public class WsFileCountMain {
    public static void main(String[] args) {
        new WsFileCountMain().startCount(args[0]);
    }

    public void startCount(String startDir) {
        StopWatch stopWatch = new StopWatch("count");
        File startDirFile = new File(startDir);
        listDir(startDirFile);
        stopWatch.stop();
    }

    private void listDir(File startDirFile) {
        File[] files = startDirFile.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                int count = count(file);
                System.err.println(file.getPath() + "\t" + count);
                System.out.println(file.getPath() + "\t" + count);
            }
        }
    }

    private int count(File dir) {
        int count = 0;
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) count += count(file);
                else {
                    String fileNameLowerCase = file.getName().toLowerCase();
                    if (fileNameLowerCase.endsWith(".ws")) {
                        count++;
                    }
                }
            }
        }
        return count;
    }
}
