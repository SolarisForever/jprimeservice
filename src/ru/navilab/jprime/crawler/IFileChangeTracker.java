package ru.navilab.jprime.crawler;

import java.io.File;

/**
 * Created by Solaris on 24.10.2017.
 */
public interface IFileChangeTracker {
    void updateTimestamp(FileChangeInfo fileInfo) throws ChangeTrackerException;

    FileChangeInfo getFileChangeInfo(File file) throws ChangeTrackerException;

    void updateLastModified(FileChangeInfo fileChangeInfo) throws ChangeTrackerException;
}
