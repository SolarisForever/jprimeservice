package ru.navilab.jprime.crawler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Mikhailov_KG on 21.05.2018.
 */
public class SqliteDb {
    private final Connection connection;

    public SqliteDb(String databaseFilename) throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        String url = "jdbc:sqlite:" + databaseFilename + ".s3db";
        System.err.println("url = " + url);
        connection = DriverManager.getConnection(url);
        connection.setAutoCommit(false);
    }

    public Connection getConnection() {
        return connection;
    }
}
