package ru.navilab.jprime.crawler.table;

import navi.client.geo.prime.common.*;
import ru.navilab.common.utils.Crypto;
import ru.navilab.jprime.adapter.*;
import ru.navilab.jprime.crawler.*;
import ru.navilab.jprime.crawler.utils.NullProxy;
import ru.navilab.jprime.data.ILogProperties;
import ru.navilab.jprime.data.IWellLogTable;
import ru.navilab.jprime.data.WellLogTables;
import ru.navilab.jprime.loader.v2.JniMultiDllApiFactory;
import ru.navilab.jprime.loader.v2.JniPrimeLoader;
import ru.navilab.jprime.service.PrimeReadException;
import ru.navilab.matrix.MatrixConfiguration;
import ru.navilab.matrix.engine.*;
import ru.navilab.matrix.entity.MatrixEntity;
import ru.navilab.matrix.export.MatrixPrinter;
import ru.navilab.matrix.load.MatrixConnection;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mikhailov_KG on 19.04.2018.
 */
public class PrimeCrawler {
    public static final String PRIME_CRAWLER_INI = "prime-crawler.ini";
    public static final String WS_FILE_EXT = ".ws";
    public static final String SQLITE_DB = "sqlite.db";
    public static final String MATRIX_DIR = "matrix.dir";
    public static final String JDBC_URL = "jdbc.url";
    public static final String JDBC_USER = "jdbc.user";
    public static final String JDBC_DRIVER_CLASSNAME = "jdbc.driver.classname";
    private final Properties properties;
    private final String wsfileParsePattern;
    private final long timestamp;
    private final MatrixConfiguration matrixConfiguration;
    private final String source;
    private final String serverName;
    private final Connection sqliteConnection;
    private boolean saveResult = true;
    private IFileChangeTracker fileChangeTracker;
    private JniPrimeLoader primeLoader = new JniPrimeLoader();
    private MatrixConnection matrixConnection;
    private OutputShrinker outputShrinker = new OutputShrinker();
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private String startDir;


    PrimeCrawler() {
        try {
            primeLoader.setApiFactory(new JniMultiDllApiFactory());
            timestamp = System.currentTimeMillis();
            properties = new Properties();
            properties.load(new FileInputStream(PRIME_CRAWLER_INI));
            String primeCrawlerDbName = properties.getProperty(SQLITE_DB, "prime-crawler");
            SqliteDb sqliteDb = new SqliteDb(primeCrawlerDbName);
            sqliteConnection = sqliteDb.getConnection();
            fileChangeTracker = new FileChangeTracker(sqliteDb.getConnection(), timestamp);
            wsfileParsePattern = properties.getProperty("wsfile.parse.pattern");
            source = properties.getProperty("well_log_curve_hdr.source", "LogDB");
            serverName = properties.getProperty("well_log_curve_hdr.server", "tonipi-prime");
            String matrixDir = properties.getProperty(MATRIX_DIR, "matrix");
            matrixConfiguration = new MatrixConfiguration(matrixDir);
            String jdbcUrl = properties.getProperty(JDBC_URL, "jdbc:oracle:thin:@10.124.13.55:1521:geopro");
            String jdbcUser = properties.getProperty(JDBC_USER, "mikhailov_kg");
            String jdbcPassword = Crypto.decrypt(properties.getProperty("jdbc.password", ""));
            String driverClassname = properties.getProperty(JDBC_DRIVER_CLASSNAME,
                    "oracle.jdbc.driver.OracleDriver");
            matrixConnection = new MatrixConnection(driverClassname, "PrimeGeoproConnection", jdbcUrl,
                    jdbcUser, jdbcPassword);
            String doNotSaveOption = System.getProperty("DO_NOT_SAVE");
            if (doNotSaveOption != null) saveResult = false;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void startPrimeCrawl(String startDir) {
        this.startDir = startDir;
        try {
            Integer fileCount = MatrixJdbcUtils.queryInt(sqliteConnection, "SELECT count(file) from files WHERE " +
                    fileLikeStartDirAndTsNE());
            logger.info("fileCount (in local db) = " + fileCount);
            File startDirFile = new File(startDir);
            listDir(startDirFile);
            if (saveResult) makeDeleteList();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private String fileLikeStartDirAndTsNE() {
        return " file like '" + startDir + "%' AND ts <> " + timestamp;
    }

    private void makeDeleteList() {
        try {
            String selectSql = "SELECT DISTINCT file from files WHERE " + fileLikeStartDirAndTsNE();
            logger.fine(selectSql);
            List<String> wsfileList = MatrixJdbcUtils.queryStringList(sqliteConnection, selectSql);
            logger.info("got " + wsfileList.size() + " .ws files to delete");
            MatrixEntity entity = matrixConfiguration.getEntity(Entities.NAVI_CURVE_FILE_LIST.getEntityName());
            Matrix matrix = entity.createMatrix();
            for (String wsfile : wsfileList) {
                INaviCurveFileList curveFileList = NaviCurveFileListAdapter.create(matrix.newRow());
                logger.fine("add to deletion " + wsfile);
                curveFileList.setWsfile(wsfile);
            }
            MatrixJdbcInserter inserter = new MatrixJdbcInserter(matrix);
            inserter.insert(matrixConnection);
            String deleteSql = "DELETE from files WHERE " + fileLikeStartDirAndTsNE();
            logger.fine(deleteSql);
            MatrixJdbcUtils.executeStatement(sqliteConnection, deleteSql);

            Connection oraConnection = matrixConnection.getConnection();
            Statement stmt = oraConnection.createStatement();
            stmt.execute("{CALL KM4.PRIME_INDEX()}");
            stmt.close();

            sqliteConnection.commit();
        } catch (SQLException e) {
            try {
                sqliteConnection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
                throw new RuntimeException(e);
            }
            throw new RuntimeException(e);
        }
    }

    private void listDir(File startDirFile) {
        File[] files = startDirFile.listFiles();
        assert files != null;
        for (File file : files) {
            if (file.isDirectory()) listDir(file);
            else {
                String fileNameLowerCase = file.getName().toLowerCase();
                if (fileNameLowerCase.endsWith(WS_FILE_EXT)) {
                    processWsFile(file);
                }
            }
        }
    }

    private void processWsFile(File file) {
        try {
            FileChangeInfo fileChangeInfo = fileChangeTracker.getFileChangeInfo(file);
            try {
                if (fileChangeInfo.isFileChanged()) {
                    processChangedFile(file, fileChangeInfo);
                } else {
                    if (outputShrinker.isAllowed()) {
                        System.err.println(String.format("skip\t%s\t%d", file.getPath(), file.lastModified()));
                    }
                }
            } finally {
                if (saveResult) fileChangeTracker.updateTimestamp(fileChangeInfo);
            }
        } catch (SQLException | ChangeTrackerException | IOException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            System.err.println("skip file - can't parse date " + file.getPath());
        }
    }

    private void processChangedFile(File file, FileChangeInfo fileChangeInfo)
            throws IOException, ParseException, ChangeTrackerException, SQLException {
        try {
            indexWsFile(file);
            if (saveResult) fileChangeTracker.updateLastModified(fileChangeInfo);
        } catch (SQLException e) {
            if (e.getErrorCode() == 1) { // java.sql.SQLIntegrityConstraintViolationException:
                String msg = stripNewLines(e.getMessage());
                System.out.println(String.format("%s\t%s", file.getPath(), msg));
                System.err.println("failed to index " + file.getPath() + " " + msg);
            } else throw e;
        } catch (PrimeReadException e) {
            String msg = e.getMessage();
            System.out.println(String.format("%s\t%s", file.getPath(), msg));
            System.err.println("failed to index " + file.getPath() + " " + msg);
        }
    }

    private String stripNewLines(String message) {
        int index = message.indexOf('\n');
        if (index != -1) {
            return message.substring(0, index);
        }
        return message;
    }

    private void indexWsFile(File wsfile) throws PrimeReadException, SQLException, IOException, ParseException {
        MatrixEntity entity = matrixConfiguration.getEntity(Entities.NAVI_CURVE.getEntityName());
        Matrix resultMatrix = entity.createMatrix();
        logger.info("start index " + wsfile);
        System.err.println("start index " + wsfile);
        String wsfilePath = wsfile.getPath();
        logger.finer("before  primeLoader.load");
        WellLogTables logTables = primeLoader.load(wsfilePath);
        logger.finer("after  primeLoader.load");
        Pattern pattern = Pattern.compile(wsfileParsePattern);
        Matcher matcher = pattern.matcher(wsfilePath);
        if (matcher.find()) {
            String section = matcher.group(1);
            String year = matcher.group(2);
            String month = matcher.group(3);
            String day = matcher.group(4);
            String uwi = matcher.group(5);
            Date primeLogDate = new SimpleDateFormat("dd.MM.yyyy").parse(day + "." + month + "." + year);
            IWellLogTable lasTable = PrimeUtils.getJoinedLasArmgTable(logTables, primeLogDate);
            Matrix lasMatrix = lasTable.getMatrix();
            int rowCount = lasMatrix.getRowCount();

            for (int i = 0; i < rowCount; i++) {
                MatrixRow lasRow = lasMatrix.getRow(i);
                LasTableExtractor extractor = new LasTableExtractor(lasTable, lasRow, lasTable.getProperties());
                Matrix lasArrayMatrix = extractor.getLasArrayMatrix();
                String nameFile = getStringNvl(lasRow.getCell(LasTableFields.NAME_FILE));
                String logComment = getLogComment(lasRow);
                String logDataQuality = getStringNvl(lasRow.getCell(LasTableFields.QUALITY));
                ILogProperties columnProps = getNestedProperties(i, lasTable.getProperties(), LasTableFields.LAS_ARRAY);

                String[] columnsNames = lasArrayMatrix.getColumnsNames();
                for (int col = 0; col < columnsNames.length; col++) {
                    MatrixColumn column = lasArrayMatrix.getColumn(col);
                    String traceName = column.getColumnName();

                    if (!LasTableExtractor.isDepthColumn(traceName)) {
                        try {
                            Date logDate = extractor.getDateValue();
                            if (logDate == null) logDate = primeLogDate;
                            CurveValues v = extractor.getCurveValues(column);
                            String fieldUnit = columnProps.getFieldUnit(traceName);
                            String fieldComment = columnProps.getFieldComment(traceName);

                            save(resultMatrix, uwi, source, traceName, v.getTop(), v.getBase(), v.getStep(),
                                    v.getTraceMin(), v.getTraceMax(), fieldUnit, logComment, fieldComment,
                                    logDataQuality, nameFile, logDate, v.getNullValue(), serverName, section,
                                    wsfilePath, i);
                        } catch (CurveExtractException e) {
                            System.out.println(wsfilePath + "\t" + e.getMessage());
                        }
                    }
                }
            }
            if (saveResult) {
                insertMatrix(resultMatrix);
            } else {
                String matrixPrint = "resultMatrix: \n" + MatrixPrinter.toString(resultMatrix);
                logger.info(matrixPrint);
            }
        } else {
            System.err.println("skip file " + wsfile);
            throw new ParseException("", 0);
        }
    }

    private ILogProperties getNestedProperties(int i, ILogProperties properties, String traceName) {
        ILogProperties nestedProperties = properties.getNestedProperties(i, traceName);
        if (nestedProperties != null) return nestedProperties;
        else return NullProxy.create(ILogProperties.class);
    }

    private String getStringNvl(MatrixCell cell) {
        if (cell == null || cell.isNull()) return "";
        else return cell.getStringValue();
    }

    // "КОММЕНТАРИЙ" - встретился fieldType = 4 (LONGINT) в одной ws-ке
    private String getLogComment(MatrixRow lasRow) {
        MatrixCell cell = lasRow.getCell(LasTableFields.LOG_COMMENT);
        if (cell == null || cell.isNull()) return "";
        else return cell.convertToStringValue();
    }

    private void insertMatrix(Matrix resultMatrix) throws SQLException {
        logger.fine("insertMatrix " + resultMatrix.getRowCount());
        MatrixJdbcInserter matrixJdbcInserter = new MatrixJdbcInserter(resultMatrix);
        List<Long> longList = matrixJdbcInserter.insert(matrixConnection);
        logger.finer("insertMatrix done " + longList.size());
    }

    private void save(Matrix resultMatrix, String uwi, String source, String traceName, float top, float base,
                      float step, float traceMin, float traceMax, String fieldUnit,
                      String logComment, String fieldComment, String qualityString, String nameFile, Date logDate,
                      float nullValue, String serverName, String section, String wsfilePath, int index) {
        INaviCurve curve = NaviCurveAdapter.create(resultMatrix.newRow());
        curve.setUwi(uwi);
        curve.setSource(source);
        curve.setTraceType(traceName);
        curve.setTop(top);
        curve.setBase(base);
        curve.setDepthInc(step);
        curve.setTraceMin(traceMin);
        curve.setTraceMax(traceMax);
        curve.setTraceUnit(fieldUnit);
        curve.setLogComment(logComment);
        curve.setRemarks(fieldComment);
        curve.setQualityCode(qualityString);
        curve.setDigitalRefVolume(nameFile);
        curve.setDigitalRefVolumeWs(wsfilePath);
        curve.setDigitizedDate(logDate);
        curve.setNullValue(nullValue);
        curve.setServer(serverName);
        curve.setSection(section);
        curve.setLasRowIndex(index);
        logger.finest(String.format("save row\t%s\t%.2f\t%.2f\t%.2f\t%s\t%s\t%d",
                traceName, top, base, step, nameFile, wsfilePath, index));
    }
}
