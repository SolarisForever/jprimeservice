package ru.navilab.jprime.crawler.table;

import ru.navilab.common.utils.Crypto;
import ru.navilab.jprime.adapter.*;
import ru.navilab.jprime.crawler.SqliteDb;
import ru.navilab.matrix.MatrixConfiguration;
import ru.navilab.matrix.engine.Matrix;
import ru.navilab.matrix.engine.MatrixJdbcInserter;
import ru.navilab.matrix.engine.MatrixJdbcUtils;
import ru.navilab.matrix.entity.MatrixEntity;
import ru.navilab.matrix.load.MatrixConnection;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by Mikhailov_KG on 22.05.2018.
 */
public class PrimeIndexFixMain {
    private final SqliteDb sqliteDb;
    private final MatrixConfiguration matrixConfiguration;
    private final MatrixConnection matrixConnection;
    private Properties properties;
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private String fileListWorkTable;

    PrimeIndexFixMain() throws Exception {
        properties = new Properties();
        properties.load(new FileInputStream(PrimeCrawler.PRIME_CRAWLER_INI));
        String primeCrawlerDbName = properties.getProperty("sqlite.db", "prime-crawler");
        fileListWorkTable = properties.getProperty("file.list.work.table", "navi_curve_file_list_work");
        sqliteDb = new SqliteDb(primeCrawlerDbName);
        String matrixDir = properties.getProperty(PrimeCrawler.MATRIX_DIR, "matrix");
        matrixConfiguration = new MatrixConfiguration(matrixDir);
        String jdbcUrl = properties.getProperty(PrimeCrawler.JDBC_URL, "jdbc:oracle:thin:@10.124.13.55:1521:geopro");
        String jdbcUser = properties.getProperty(PrimeCrawler.JDBC_USER, "mikhailov_kg");
        String jdbcPassword = Crypto.decrypt(properties.getProperty("jdbc.password", ""));
        String driverClassname = properties.getProperty(PrimeCrawler.JDBC_DRIVER_CLASSNAME, "oracle.jdbc.driver.OracleDriver");
        matrixConnection = new MatrixConnection(driverClassname, "PrimeGeoproConnection", jdbcUrl, jdbcUser, jdbcPassword);
    }

    public static void main(String[] args) throws Throwable {
        new PrimeIndexFixMain().run();
    }

    private void run() throws SQLException {
        Connection sqliteConnection = sqliteDb.getConnection();
        List<String> allFileList = MatrixJdbcUtils.queryStringList(sqliteConnection, "SELECT file FROM files");
        logger.fine("got all .ws count = " + allFileList.size());

        Matrix matrix = createAllFilesMatrix(allFileList);
        logger.fine("insert into " + fileListWorkTable);
        MatrixJdbcInserter inserter = new MatrixJdbcInserter(matrix, fileListWorkTable);
        inserter.setProgressHandler(new DotProgressHandler());
        inserter.setPeriodicCommit(true);
        inserter.setLogHandler(new MatrixJdbcInserter.Log() {
            @Override
            public void log(String s) {
                logger.finest(s);
            }
        });
        inserter.insert(matrixConnection);

        Connection oraConnection = matrixConnection.getConnection();
        String reindexListSql = String.format("SELECT %1$s FROM %2$s WHERE %1$s NOT IN (SELECT DISTINCT %3$s as %1$s FROM navi_curve)",
                NaviCurveFileListFields.WSFILE, fileListWorkTable,
                NaviCurveFields.DIGITAL_REF_VOLUME_WS);
        logger.fine(reindexListSql);
        List<String> reindexList = MatrixJdbcUtils.queryStringList(oraConnection, reindexListSql);
        logger.fine("got reindexList size = " + reindexList.size());


        try {
            String updateSql = "UPDATE files SET last_modified=? WHERE file=?";
            logger.fine(updateSql);
            PreparedStatement updateStmt = sqliteConnection.prepareStatement(updateSql);
            for (String reindexWsFile : reindexList) {
                logger.fine("reindex " + reindexWsFile);
                updateStmt.setLong(1, 0l);
                updateStmt.setString(2, reindexWsFile);
                int execCount = updateStmt.executeUpdate();
                logger.finer("update count = " + execCount);
            }
            updateStmt.close();
            sqliteConnection.commit();
            oraConnection.close();
        } catch (SQLException e) {
            sqliteConnection.rollback();
            throw e;
        }
    }

    private Matrix createAllFilesMatrix(List<String> allFileList) {
        MatrixEntity entity = matrixConfiguration.getEntity(Entities.NAVI_CURVE_FILE_LIST.getEntityName());
        Matrix matrix = entity.createMatrix();
        for (String wsfile : allFileList) {
            INaviCurveFileList curveFileList = NaviCurveFileListAdapter.create(matrix.newRow());
            logger.finest("all fileList = " + wsfile);
            curveFileList.setWsfile(wsfile);
        }
        return matrix;
    }
}
