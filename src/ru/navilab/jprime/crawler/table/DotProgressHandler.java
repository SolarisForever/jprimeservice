package ru.navilab.jprime.crawler.table;

import ru.navilab.common.ui.progress.ProgressCancelException;
import ru.navilab.common.ui.progress.ProgressHandler;

/**
 * Created by Mikhailov_KG on 22.05.2018.
 */
public class DotProgressHandler implements ProgressHandler {
    private int min;
    private int max;
    private int value;
    private long lastTime;

    @Override
    public void setMinimum(int min) {
        this.min = min;
    }

    @Override
    public void setMaximum(int max) {
        this.max = max;
    }

    @Override
    public void setIndeterminate(boolean b) {

    }

    @Override
    public void setThreadToInterrupt(Thread thread) {

    }

    @Override
    public void start() {
        this.value = min;
        System.err.println("start (" + min + "-" + max + ")");
    }

    @Override
    public void stop() {
        System.err.println("stop (" + min + "-" + max + ")");
    }

    @Override
    public boolean isCanceled() {
        return false;
    }

    @Override
    public void setProgress(int value) throws ProgressCancelException {
        this.value = value;
        if ((System.currentTimeMillis() - lastTime)/1000 > 3) {
            System.err.println(value + " (" + min + "-" + max + ")");
            lastTime = System.currentTimeMillis();
        }
    }

    @Override
    public void setProgressSilently(int i) {
        try {
            setProgress(i);
        } catch (ProgressCancelException e) {
        }
    }

    @Override
    public void incrementProgress() throws ProgressCancelException {
        setProgress(this.value + 1);
    }

    @Override
    public void incrementProgress(int add) throws ProgressCancelException {
        setProgress(this.value + add);
    }

    @Override
    public void setNote(String s) {

    }

    @Override
    public void setNote(String s, Object... objects) {

    }
}
