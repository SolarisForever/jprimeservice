package ru.navilab.jprime.crawler.table;

import ru.navilab.common.utils.StopWatch;

import java.util.Date;

/**
 * Created by Mikhailov_KG on 15.11.2017.
 */
public class PrimeCrawlerMain {
    public static void main(String[] args) {
        StopWatch sw = new StopWatch(PrimeCrawlerMain.class.getName());
        try {
            new PrimeCrawler().startPrimeCrawl(args[0]);
        } finally {
            sw.stop();
            System.err.println(new Date());
        }
    }
}
