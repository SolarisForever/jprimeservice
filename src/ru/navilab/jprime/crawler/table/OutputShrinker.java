package ru.navilab.jprime.crawler.table;

/**
 * Created by Mikhailov_KG on 23.04.2018.
 */
public class OutputShrinker {
    private long ts;

    OutputShrinker() {
        ts = System.currentTimeMillis();
    }

    boolean isAllowed() {
        long cts = System.currentTimeMillis();
        long delta = (cts - ts);

        boolean result = delta > 1500;
        if (result) ts = cts;
        return result;
    }
}
