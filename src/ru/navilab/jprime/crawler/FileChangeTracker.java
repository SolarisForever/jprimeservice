package ru.navilab.jprime.crawler;

import java.io.File;
import java.sql.*;

/**
 * Created by Solaris on 24.10.2017.
 */
public class FileChangeTracker implements IFileChangeTracker {
    private Connection connection;
    private long timestamp;

    public FileChangeTracker(String databaseFilename, long timestamp) throws ClassNotFoundException, SQLException, ChangeTrackerException {
        this(new SqliteDb(databaseFilename).getConnection(), timestamp);
    }

    public FileChangeTracker(Connection connection, long timestamp) throws SQLException {
        this.connection = connection;
        this.timestamp = timestamp;
        Statement statmt = connection.createStatement();
        statmt.execute("CREATE TABLE if not exists 'files' ('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'file' TEXT, " +
                "'last_modified' INTEGER, ts LONG);");
        statmt.execute("create index if not exists files_lm_ts on files (last_modified, ts);");
        statmt.execute("create index if not exists files_file on files (file);");
        statmt.close();
    }



    @Override
    public FileChangeInfo getFileChangeInfo(File file) throws ChangeTrackerException {
        try {
            long lastModified = file.lastModified();
            PreparedStatement pstmt = connection.prepareStatement("SELECT last_modified FROM files WHERE file=?");
            pstmt.setString(1, file.getPath());
            ResultSet resultSet = pstmt.executeQuery();
            try {
                if (resultSet.next()) {
                    long dbLastModified = resultSet.getLong(1);
                    return new FileChangeInfo(file, lastModified, dbLastModified);
                } else {
                    return new FileChangeInfo(file, lastModified);
                }
            } finally {
                resultSet.close();
                pstmt.close();
            }
        } catch (SQLException e) {
            throw new ChangeTrackerException(e);
        }
    }

    @Override
    public void updateLastModified(FileChangeInfo fileInfo) throws ChangeTrackerException {
        File file = fileInfo.getFile();
        try {
            if (fileInfo.isFileExistInDatabase()) {
                PreparedStatement update = connection.prepareStatement(
                        "UPDATE files SET last_modified=?,ts=? WHERE file=?");
                update.setLong(1, file.lastModified());
                update.setLong(2, timestamp);
                update.setString(3, file.getPath());
                update.execute();
                update.close();
            } else {
                PreparedStatement insertStmt = connection.prepareStatement(
                        "INSERT INTO files (file,last_modified,ts) VALUES (?,?,?)");
                insertStmt.setString(1, file.getPath());
                insertStmt.setLong(2, file.lastModified());
                insertStmt.setLong(3, timestamp);
                insertStmt.execute();
                insertStmt.close();
            }
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            throw new ChangeTrackerException(e);
        }
    }

    @Override
    public void updateTimestamp(FileChangeInfo fileInfo) throws ChangeTrackerException {
        File file = fileInfo.getFile();
        try {
            PreparedStatement update = connection.prepareStatement(
                    "UPDATE files SET ts=? WHERE file=?");
            update.setLong(1, timestamp);
            update.setString(2, file.getPath());
            update.execute();
            update.close();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            throw new ChangeTrackerException(e);
        }
    }
}
