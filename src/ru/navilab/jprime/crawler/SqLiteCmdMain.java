package ru.navilab.jprime.crawler;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.sql.*;

/**
 * Created by Mikhailov_KG on 23.04.2018.
 */
public class SqLiteCmdMain {
    private Connection connection;


    SqLiteCmdMain(String file) throws Throwable {
        Class.forName("org.sqlite.JDBC");
        String url = "jdbc:sqlite:" + file + ".s3db";
        System.err.println("url = " + url);
        connection = DriverManager.getConnection(url);
        connection.setAutoCommit(false);
    }

    public static void main(String[] args) throws Throwable {
        new SqLiteCmdMain(args[0]).run();
    }

    private void run() throws Throwable {
        LineNumberReader lr = new LineNumberReader(new InputStreamReader(System.in));
        StringBuilder sb = new StringBuilder();
        String s = null;
        while ((s = lr.readLine()) != null) {
            if (!s.contains(";")) sb.append(s);
            else {
                sb.append(s);
                Statement stmt = connection.createStatement();
                String sql = sb.toString();
                System.out.println("sql='" + sql + "'");
                sb = new StringBuilder();
                if (sql.toLowerCase().contains("select")) {
                    select(stmt, sql);
                } else {
                    execute(stmt, sql);
                }
                stmt.close();
            }
        }
    }

    private void execute(Statement stmt, String sql) throws SQLException {
        int count = stmt.executeUpdate(sql);
        System.out.println("count = " + count);
    }

    private void select(Statement stmt, String sql) throws SQLException {
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            int columnCount = rs.getMetaData().getColumnCount();
            for (int i = 1; i < columnCount + 1; i++) {
                System.out.print(rs.getString(i) + "\t");
            }
            System.out.println();
        }
        System.out.println();
        rs.close();
    }
}
