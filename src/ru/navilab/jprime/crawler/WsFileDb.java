package ru.navilab.jprime.crawler;

import java.sql.*;

/**
 * Created by Mikhailov_KG on 11.05.2018.
 */
public class WsFileDb {
    private final long timestamp;
    private final Connection connection;
    private final PreparedStatement insertStmt;

    WsFileDb(String databaseFilename, long timestamp) throws SQLException {
        try {
            this.timestamp = timestamp;
            Class.forName("org.sqlite.JDBC");
            String url = "jdbc:sqlite:" + databaseFilename + ".s3db";
            System.err.println("url = " + url);
            connection = DriverManager.getConnection(url);
            connection.setAutoCommit(false);
            Statement statmt = connection.createStatement();
            statmt.execute("CREATE TABLE if not exists 'wsfiles' (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "uwi TEXT, wsfile TEXT, trace_type TEXT, section TEXT, log_date DATE, ts LONG);");
            statmt.execute("create index if not exists wsfiles_uwi_section on wsfiles (uwi, section);");
            statmt.close();

            insertStmt = connection.prepareStatement(
                    "INSERT INTO wsfiles (uwi,wsfile,trace_type,section,log_date,ts) VALUES (?,?,?,?,?,?)");

            insertStmt.close();
        } catch (ClassNotFoundException e) {
            throw new SQLException(e.getMessage());
        }
    }

    public void insert(String uwi, String wsfile, String traceType, Date logDate) throws SQLException {
        insertStmt.setString(1, uwi);
        insertStmt.setString(2, wsfile);
        insertStmt.setString(3, traceType);
        insertStmt.setDate(4, logDate);
        insertStmt.setLong(5, timestamp);
        insertStmt.execute();
    }
}
