package ru.navilab.jprime.crawler.utils;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.logging.Logger;

/**
 * Created by Mikhailov_KG on 18.05.2018.
 */
public class MBeanUtils {
    public static String getObjectName(Class<?> clz) {
        return clz.getPackage().getName() + ":type=" + clz.getSimpleName();
    }

    public static void register(Object mbean) {
        try {
            Logger logger = Logger.getLogger(MBeanUtils.class.getName());
            logger.fine("register mbean " + mbean.getClass().getName());
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            String objectName = MBeanUtils.getObjectName(mbean.getClass());
            ObjectName name = new ObjectName(objectName);
            mbs.registerMBean(mbean, name);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
