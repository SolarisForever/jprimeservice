package ru.navilab.jprime.crawler.blob;

/**
 * Created by Mikhailov_KG on 15.11.2017.
 */
public class PrimeCrawlerMain {
    public static void main(String[] args) {
        new PrimeCrawler().startPrimeCrawl(args[0]);
    }
}
