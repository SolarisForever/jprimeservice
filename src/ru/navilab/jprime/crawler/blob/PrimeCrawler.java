package ru.navilab.jprime.crawler.blob;

import ru.navilab.jprime.crawler.ChangeTrackerException;
import ru.navilab.jprime.crawler.FileChangeTracker;
import ru.navilab.jprime.crawler.IFileChangeTracker;
import ru.navilab.jprime.data.WellLogTables;
import ru.navilab.jprime.loader.JPrimeLoader;
import ru.navilab.jprime.service.PrimeReadException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mikhailov_KG on 15.11.2017.
 */
public class PrimeCrawler {
    private static final String PRIME_CRAWLER_INI = "prime-crawler.ini";
    public static final String WS_FILE_EXT = ".ws";
    private final Properties properties;
    private final LogTablesInserter logTablesInserter;
    private final String wsfileParsePattern;
    private IFileChangeTracker fileChangeTracker;
    private JPrimeLoader primeLoader = new JPrimeLoader();


    PrimeCrawler() {
        try {
            fileChangeTracker = new FileChangeTracker("storage-crawler", System.currentTimeMillis());
            properties = new Properties();
            properties.load(new FileInputStream(PRIME_CRAWLER_INI));
            wsfileParsePattern = properties.getProperty("wsfile.parse.pattern");

            logTablesInserter = new LogTablesInserter(properties);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ChangeTrackerException e) {
            throw new RuntimeException(e);
        }
    }

    public void startPrimeCrawl(String startDir) {
        File startDirFile = new File(startDir);
        listDir(startDirFile);
    }

    private void listDir(File startDirFile) {
        File[] files = startDirFile.listFiles();
        for (File file : files) {
            if (file.isDirectory()) listDir(file);
            else {
                String fileNameLowerCase = file.getName().toLowerCase();
                if (fileNameLowerCase.endsWith(WS_FILE_EXT)) {
//                    if (fileChangeTracker.isChangedOrNew(file)) {
//                        try {
//                            indexWsFile(file);
//                            fileChangeTracker.updateTimestamp(file);
//                        } catch (PrimeReadException e) {
//                            throw new RuntimeException(e);
//                        } catch (SQLException e) {
//                            throw new RuntimeException(e);
//                        } catch (IOException e) {
//                            throw new RuntimeException(e);
//                        } catch (ParseException e) {
//                            System.err.println("skip file - can't parse date " + file.getPath());
//                        }
//                    }
                }
            }
        }
    }

    private void indexWsFile(File wsfile) throws PrimeReadException, SQLException, IOException, ParseException {
        System.err.println("start index " + wsfile);
        WellLogTables logTables = primeLoader.load(wsfile.getPath());
        Pattern pattern = Pattern.compile(wsfileParsePattern);
        Matcher matcher = pattern.matcher(wsfile.getPath());
        if (matcher.find()) {
            String section = matcher.group(1);
            String year = matcher.group(2);
            String month = matcher.group(3);
            String day = matcher.group(4);
            String uwi = matcher.group(5);
            Date logDate = new SimpleDateFormat("dd.MM.yyyy").parse(day + "." + month + "." + year);
            logTablesInserter.writeLogTables(section, uwi, wsfile.getPath(), logDate, logTables);
        } else {
            System.err.println("skip file " + wsfile);
        }
    }
}
