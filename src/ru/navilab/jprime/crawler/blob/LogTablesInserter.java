package ru.navilab.jprime.crawler.blob;

import ru.navilab.jprime.data.IWellLogTables;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

/**
 * Created by Mikhailov_KG on 15.11.2017.
 */
public class LogTablesInserter {
    private String jdbcUrl;
    private String jdbcUser;
    private String jdbcPassword;
    private Connection connection;
    private String tableName;


    public LogTablesInserter(Properties properties) throws SQLException {
        tableName = properties.getProperty("table.name", "well_log_tables");
        jdbcUrl = properties.getProperty("jdbc.url", "jdbc:oracle:thin:@10.124.13.55:1521:geopro");
        jdbcUser = properties.getProperty("jdbc.user", "mikhailov_kg");
        jdbcPassword = properties.getProperty("jdbc.password", "");
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        connection = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);
        connection.setAutoCommit(false);
    }

    public void writeLogTables(String section, String uwi, String wsfile, Date logDate, IWellLogTables logTables)
            throws SQLException, IOException {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("INSERT INTO ");
        sqlBuilder.append(tableName);
        sqlBuilder.append(" (uwi, wsfile, log_date, log_tables, section)");
        sqlBuilder.append(" VALUES (?, ?, ?, ?, ?)");
        byte[] data = dataToBlob(logTables);
        PreparedStatement pstmt = connection.prepareStatement(sqlBuilder.toString());
        pstmt.setString(1, uwi);
        pstmt.setString(2, wsfile);
        pstmt.setDate(3, new java.sql.Date(logDate.getTime()));
        pstmt.setBytes(4, data);
        pstmt.setString(5, section);
        pstmt.execute();
        pstmt.close();
        connection.commit();
    }

    private byte[] dataToBlob(Serializable serializable) throws IOException {
        ObjectOutputStream out = null;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            out = new ObjectOutputStream(byteArrayOutputStream);
            out.writeObject(serializable);
        } finally {
            if (out != null) try {
                out.flush();
                out.close();
            } catch (IOException e) {
                throw new IOException(e);
            }
        }
        return byteArrayOutputStream.toByteArray();
    }
}
