package ru.navilab.jprime.crawler;

/**
 * Created by Mikhailov_KG on 19.04.2018.
 */
public class ChangeTrackerException extends Exception {
    public ChangeTrackerException(Throwable e) {
        super(e);
    }
}
