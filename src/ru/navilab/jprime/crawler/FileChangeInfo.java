package ru.navilab.jprime.crawler;

import java.io.File;

/**
 * Created by Mikhailov_KG on 19.04.2018.
 */
public class FileChangeInfo {
    private final long lastModified;
    private Long dbLastModified;
    private final File file;

    public FileChangeInfo(File file, long lastModified, Long dbLastModified) {
        this.file = file;
        this.lastModified = lastModified;
        this.dbLastModified = dbLastModified;
    }

    public FileChangeInfo(File file, long lastModified) {
        this.file = file;
        this.lastModified = lastModified;
    }

    public boolean isFileChanged() {
        if (dbLastModified != null) return lastModified > dbLastModified;
        else return true;
    }

    public boolean isFileExistInDatabase() {
        return dbLastModified != null;
    }

    public File getFile() {
        return file;
    }
}
