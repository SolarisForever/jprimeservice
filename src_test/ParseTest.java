import org.junit.Assert;
import org.junit.Test;
import ru.navilab.jprime.service.JPrimeService;
import ru.navilab.jprime.service.JPrimeServiceProperties;

import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mikhailov_KG on 13.04.2016.
 */
public class ParseTest {

    @Test
    public void parseFile() throws IOException {
        Properties properties = JPrimeServiceProperties.getInstance().getProperties();
        String patternStr = properties.getProperty(JPrimeService.PRIME_LOGDB_PARSE);
        String patternStrAlt = "\\S+\\\\(\\S+)\\\\(\\d{4})\\-(\\d{2})\\-(\\d{2})";
        System.err.println(patternStr);
        System.err.println(patternStrAlt);
        Pattern pattern = Pattern.compile(patternStrAlt);
        Matcher matcher = pattern.matcher("\\\\TONIPI-PRIME\\data2\\PrimeDB\\LogDB-1\\Ватлорское\\10В\\40039141\\LOG\\2014-08-21\\40039141_21.08.2014_LOG.ws");
        if (matcher.find()) {
            String section = matcher.group(1);
            String year = matcher.group(2);
            String month = matcher.group(3);
            String day = matcher.group(4);
            System.err.println("sec " + section);
            System.err.println("year " + year);
            System.err.println("month " + month);
            System.err.println("day " + day);
            Assert.assertEquals(section, "LOG");
            Assert.assertEquals(year, "2014");
            Assert.assertEquals(month, "08");
            Assert.assertEquals(day, "21");
        }
    }
}
