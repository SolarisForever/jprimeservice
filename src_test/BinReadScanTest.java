import ru.navilab.common.utils.StopWatch;
import ru.navilab.jprime.service.IFileDelegate;
import ru.navilab.jprime.service.PrimeReadException;

import java.io.*;
import java.sql.SQLException;

public class BinReadScanTest {
    static FilenameFilter wsFilenameFilter = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.toUpperCase().endsWith(".WS") && dir.getPath().contains("\\LOG\\");
        }
    };

    public static void main(String[] args) throws PrimeReadException, SQLException, IOException {
        StopWatch scan = new StopWatch("scan");
        new BinReadScanTest().scanDir(new File("\\\\TONIPI-PRIME\\data1\\PrimeDB\\LogDB\\Ай-Пимское\\"), new IFileDelegate() {
            @Override
            public void fileFound(String file) {
                System.err.println("file " + file);
                test(file);
                return;
            }
        });
        scan.stop();
    }

    private static void test(String file)  {
        try {
            BinaryReader binaryReader = new BinaryReader(file);
            binaryReader.readAllTables();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void scanDir(File file, IFileDelegate fileDelegate) throws PrimeReadException, SQLException, IOException {
        System.err.println(file);
        File[] files = file.listFiles(wsFilenameFilter);
        if (files != null) {
            for (File wsFile : files) {
                System.err.println(wsFile.getAbsolutePath());
                String absolutePath = wsFile.getAbsolutePath();
                fileDelegate.fileFound(absolutePath);
            }
        }
        File[] dirFiles = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return (pathname.isDirectory());
            }
        });
        System.err.println("dirFiles " + dirFiles + " " + file);
        if (dirFiles != null) {
            for (File dirFile : dirFiles) {
                System.err.println("dirFile " + dirFile);
                scanDir(dirFile, fileDelegate);
            }
        }
    }
}
