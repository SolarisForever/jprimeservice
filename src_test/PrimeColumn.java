public class PrimeColumn {
    public static final int FLOAT = 5;
    public static final int STRING_1031 = 1031;
    public static int STRING = 7;
    public static int MEMO = 19;
    private final String name;
    private final int type;
    private final int size;

    public PrimeColumn(String name, int type, int sz) {
        this.name = name;
        this.type = type;
        this.size = sz;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public int getSize() {
        return size;
    }
}
