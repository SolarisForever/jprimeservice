import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TableReader {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private static final Charset CP_866 = Charset.forName("CP866");
    private final MyInputStream tableStream;
    private byte[] stringBuffer = new byte[256];
    private byte[] buf400 = new byte[0x400];
    private byte[] bytes8 = new byte[8];

    public TableReader(RandomAccessFile raf, int tableOffset, int nextTableOffset) throws IOException {
        raf.seek(tableOffset);
        long next = Math.max(raf.length(), nextTableOffset);
        long size = next - tableOffset;
        byte[] bytes = new byte[(int) size];
        raf.readFully(bytes);
        tableStream = new MyInputStream(tableOffset, bytes);
    }

    public void readTable() throws IOException {
        List<PrimeColumn> columnList = new ArrayList<>();
        tableStream.skip(0x76);
        tableStream.mark(0);
        short id = 0;
        int arrayColumnCount = 0;
        do {
            id = (short) (0xFF & readShort(tableStream));
            if (id == 0x40) break;
            tableStream.reset();
            tableStream.skip(0x26);
            int sz = readShort(tableStream);
            int type = readShort(tableStream);
            tableStream.reset();
            tableStream.skip(0x4);
            String name = readString();
            if (logger.isLoggable(Level.FINE)) logger.fine(String.format("name%x=%s (%d - %d)\t%04x", id, name, type, sz, tableStream.getFilePointer()));

            if (arrayColumnCount > 0) {
                arrayColumnCount--;
            } else {
                columnList.add(new PrimeColumn(name, type, sz));
            }
            if (type == 10) arrayColumnCount = sz;
            tableStream.reset();

            if (id == 0x30) {
                tableStream.skip(0x94);
                tableStream.mark(0);
            } else if (id == 0x32) {
                tableStream.skip(0xB5);
                tableStream.mark(0);;
            }else if (id == 0x20 || id == 0x2920) {
                tableStream.skip(0x42);
                tableStream.mark(0);
            }else if (id == 0x26) { // LAS ARRAY
                tableStream.skip(0x8D);
                tableStream.mark(0);
            }else if (id == 0xB2) { // колонки LAS_ARRAY?
                tableStream.skip(0xB5);
                tableStream.mark(0);
            }else if (id == 0x34) {
                tableStream.skip(0xBE);
                tableStream.mark(0);
            }else if (id == 0xA0) {
                tableStream.skip(0x42);
                tableStream.mark(0);
            }else if (id == 0xB4) {
                tableStream.skip(0xBE);
                tableStream.mark(0);
            }else if (id == 0xB0) {
                tableStream.skip(0x94);
                tableStream.mark(0);
            }else if (id == 0x22 || id == 0xA2) {
                tableStream.skip(0x63);
                tableStream.mark(0);
            }else if (id == 0x24 || id == 0x7F24) {
                tableStream.skip(0x6C);
                tableStream.mark(0);
            }else if (id == 0x7f20 || id == 0x7fa0) {
                tableStream.skip(0x42);
                tableStream.mark(0);
            }else if (id == 0x36 ) {
                tableStream.skip(0xDF);
                tableStream.mark(0);
            }else if (id == 0xA4 ) {
                tableStream.skip(0x6C);
                tableStream.mark(0);
            }else if (id == 0x4B0) { // 4B0 - End of Columns block
                tableStream.skip(0x94);
                tableStream.mark(0);
            }
        } while (id == 0x30 || id == 0x32|| id == 0x20 || id == 0x26  || id == 0xB2
                || id == 0x34 || id == 0xA0 || id == 0xB4|| id == 0xB0|| id == 0x22
                || id == 0x24
                || id == 0x36 || id == 0xA2 || id == 0xA4);

        if (logger.isLoggable(Level.FINE)) {
            long pos = tableStream.getFilePointer();
            logger.fine(String.format("pos = %04X, id=%02X", pos, id));
        }
        if (id != 0x40) throw new RuntimeException("wrong format");

        int row = 1;
        while (id == 0x40) {
            tableStream.skip(4);
            byte[] buf = new byte[0x25];
            tableStream.read(buf);
            String tableName = nullTerminatedString(buf, 0);
            if (logger.isLoggable(Level.FINE)) logger.fine("Table " + tableName);
            if (logger.isLoggable(Level.FINE)) logger.fine(String.format("read table pos %04x", tableStream.getFilePointer()));
            readTableData(tableStream, columnList);
            id = (short) tableStream.read();
            if (logger.isLoggable(Level.FINE)) logger.fine("*********** row = " + row);
            row++;

            if (logger.isLoggable(Level.FINE)) logger.fine(String.format("pos=%04x", tableStream.getFilePointer()));
        }
    }

    private int readShort(InputStream stream) throws IOException {
        int b1 = stream.read();
        int b2 = stream.read();
        if ((b1 | b2) < 0)
            throw new EOFException();
        return (b1 << 8) + b2;
    }

    private void readTableData(MyInputStream tableStream, List<PrimeColumn> columnList) throws IOException {
        Iterator<PrimeColumn> columnIterator = columnList.iterator();
        while (columnIterator.hasNext()) {
            PrimeColumn primeColumn = columnIterator.next();
            byte nullFlag = (byte) (tableStream.read() & 0xFF);
            if (nullFlag == 9) {
                if (logger.isLoggable(Level.FINEST)) logger.finest(String.format("pos=%04x", tableStream.getFilePointer()));
                readArrayDesc(tableStream, primeColumn, columnIterator);
                continue;
            }
            if (nullFlag != 1 && nullFlag != 0) {
                if (logger.isLoggable(Level.FINEST)) logger.finest(String.format("b=%04x", nullFlag));
                if (logger.isLoggable(Level.FINEST)) logger.finest(String.format("pos=%04x", tableStream.getFilePointer()));
                break;
            }
            if (nullFlag != 0) {
                int sz = primeColumn.getSize();
                int type = primeColumn.getType();
                if (type == 7) readString();
                else if (type == 5) readSingle();
                else if (type == 4) readLongint();
                else if (type == 8) readDate();
                else if (type == 19) readMemoDesc();
                else if (sz > 0) tableStream.read(new byte[sz]);
            } else {
                if (logger.isLoggable(Level.FINER)) logger.finer(primeColumn.getName() + " is null");
            }
        }
    }

    private int readArrayDesc(MyInputStream tableStream, PrimeColumn primeColumn, Iterator<PrimeColumn> columnIterator) throws IOException {
        int tableRows = readPascalInt();
        if (logger.isLoggable(Level.FINER)) logger.finer(primeColumn.getName() + " table rows " + tableRows);
        tableStream.skip(7);
        read01();
        int tableColumns = tableStream.read() & 0x000000FF;
        if (logger.isLoggable(Level.FINER)) logger.finer("table cols " + tableColumns);
        tableStream.skip(6);

        int one = 0;
        tableStream.mark(0);
        List<PrimeColumn> tableColumnList = new ArrayList<>();
        while ((one = tableStream.read()) == 1){
            tableStream.skip(2);
            String s = readString();
            tableStream.reset();
            tableStream.skip(0x25);
            int sz = readShort(tableStream);
            int type = readShort(tableStream);
            tableStream.skip(1);
            int unknownFlag = tableStream.read();
            if (logger.isLoggable(Level.FINER)) logger.finer(String.format("table col %s %d %04x f=%d", s, type, tableStream.getFilePointer(), unknownFlag));
            tableStream.reset();
            tableStream.skip(0xB8);
            tableStream.mark(0);
            PrimeColumn column = new PrimeColumn(s, type, sz);
            tableColumnList.add(column);
        }
        if (logger.isLoggable(Level.FINE)) logger.finer(String.format("exit table pos=%04x, %02x", tableStream.getFilePointer(), one));
        tableStream.skip(0x3C);
        String lasArray = readString();
        if (logger.isLoggable(Level.FINER)) logger.finer("la=" + lasArray);
        tableStream.reset();
        tableStream.skip(0x3C + 0x2B);

        for (int r = 0; r < tableRows; r++) {
            boolean show = ((r == 0) || (r == (tableRows - 1)));
            for (PrimeColumn column : tableColumnList) {
                int columnType = column.getType();
                if (columnType == PrimeColumn.FLOAT) {
                    float f = readPascalFloat();
                    if (show) logger.finest(""+f);
                } else if (columnType == PrimeColumn.STRING) {
                    String s = readString();
                    if (show) logger.finest(s);
                } else if (columnType == PrimeColumn.STRING_1031) {
                    String s = readString();
                    if (show) logger.finest(s);
                } else if (columnType == PrimeColumn.MEMO) {
                    String s = readMemo();
                    if (show) logger.finest(s);
                } else if (columnType == 263) {
                    String s = readString();
                    if (show) logger.finest(s);
                } else if (columnType == 1) {
                    char read = (char) tableStream.read();
                    if (show) logger.finest("'" + read + "'");
                } else if (columnType == 1036) {
                    String s = read1036();
                    if (show) logger.finest(s);
                } else if (columnType == 268) {
                    String s = read268();
                    if (show) logger.finest(s);
                } else if (columnType == 8) {
                    int date = readPascalInt();
                    if (show) logger.finest(Integer.toString(date, 16));
                } else if (columnType == 4) {
                    int v = readPascalInt();
                    if (show) logger.finest(Integer.toString(v, 16));
                } else if (columnType == 6) {
                    double v = readPascalDouble();
                    if (show) logger.finest("" + v);
                } else {
                    if (logger.isLoggable(Level.FINEST)) logger.finest(String.format("pos=%04x", tableStream.getFilePointer()));
                    throw new RuntimeException("unknown type " + columnType);
                }
                if (show) logger.finest("\t");
                //if (logger.isLoggable(Level.FINE)) logger.fine(String.format("pos=%04x", raf.getFilePointer()));
            }
        }

        if (logger.isLoggable(Level.FINER)) logger.finer(String.format("pos=%04x", tableStream.getFilePointer()));
        return tableColumns;
    }

    private double readPascalDouble() throws IOException {
        tableStream.read(bytes8);
        return 0;
    }

    private String read268() throws IOException {
        byte[] buf = new byte[0x100];
        tableStream.read(buf);
        return nullTerminatedString(buf, 0);
    }

    private String read1036() throws IOException {
        tableStream.read(buf400);
        return nullTerminatedString(buf400, 0);
    }

    private String readMemo() throws IOException {
        int size = readPascalInt();
        String s = null;
        if (size > 0) {
            if (logger.isLoggable(Level.FINER)) logger.finer(String.format("memo size = %d %x", size, size));
            byte[] buf = new byte[size];
            tableStream.read(buf);
            s = nullTerminatedString(buf, 0);
        }
        tableStream.skip(1024-size);
        return s;
    }

    private String nullTerminatedString(byte[] buf, int offset) {
        int i = offset;
        for (; i < buf.length; i++) {
            if (buf[i] == 0) break;
        }
        return new String(buf, offset, i, CP_866);
    }


    private void readMemoDesc() throws IOException {
        int size = readPascalInt();
        tableStream.skip(12);
        read01();
        tableStream.skip(5);
        if (size > 0) {
            tableStream.skip(size);
        }
    }

    private void read01() throws IOException {
        int b = tableStream.read();
        if (b != 1) throw new RuntimeException("wrong format");
    }

    private int readDate() throws IOException {
        return readPascalInt();
    }

    private int readLongint() throws IOException {
        int ch1 = tableStream.read();
        int ch2 = tableStream.read();
        int ch3 = tableStream.read();
        int ch4 = tableStream.read();
        if ((ch1 | ch2 | ch3 | ch4) < 0)
            throw new EOFException();
        return ((ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0));
    }

    private float readSingle() throws IOException {
        float v = readPascalFloat();
        return v;
    }



//    private String readString() throws IOException {
//        int sz = tableStream.read() & 0x000000FF;
//        String s = null;
//        if (sz > 0) {
//            byte[] buf = new byte[sz];
//            int read = tableStream.read(buf);
//            if (read != sz) throw new RuntimeException("wrong format");
//            s = new String(buf, 0, read, CP_866).trim();
//        }
//        return s;
//    }

    private String readString() throws IOException {
        int strSize = tableStream.read();
        if (strSize <= 0) return null;
        int read = tableStream.read(stringBuffer, 0, strSize);
        if (read > 0) return new String(stringBuffer, 0, read, CP_866);
        else return null;
    }


//    private String readString(int offset) throws IOException {
//        raf.seek(offset);
//        if (logger.isLoggable(Level.FINE)) logger.fine(String.format("%02X %02X", offset, offset - lastSeek));
//        lastSeek = offset;
//        int strSize = raf.readByte();
//        if (strSize <= 0) return null;
//        int read = raf.read(stringBuffer, 0, strSize);
//        if (read > 0) return new String(stringBuffer, 0, read, Charset.forName("CP866"));
//        else return null;
//    }

    private float readPascalFloat() throws IOException {
        int ch1 = tableStream.read();
        int ch2 = tableStream.read();
        int ch3 = tableStream.read();
        int ch4 = tableStream.read();
        if ((ch1 | ch2 | ch3 | ch4) < 0)
            throw new EOFException();
        return Float.intBitsToFloat((ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0));
    }

    private int readPascalInt() throws IOException {
        int ch1 = tableStream.read();
        int ch2 = tableStream.read();
        int ch3 = tableStream.read();
        int ch4 = tableStream.read();
        if ((ch1 | ch2 | ch3 | ch4) < 0)
            throw new EOFException();
        return (ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0);
    }

}
