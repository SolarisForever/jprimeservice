import ru.navilab.jprime.data.IWellLogTable;
import ru.navilab.jprime.data.WellLogTables;
import ru.navilab.jprime.loader.JPrimeLoader;
import ru.navilab.matrix.engine.Matrix;

import java.util.List;

/**
 * Created by Mikhailov_KG on 18.01.2018.
 */
public class JPrimeLoaderTest {
    public static void main(String[] args) throws Throwable {
        JPrimeLoader primeLoader = new JPrimeLoader();
        WellLogTables wellLogTables = primeLoader.load(args[0]);
        System.err.println("wellLogTables  " + wellLogTables.getWellLogTableList().size());
        List<IWellLogTable> wellLogTableList = wellLogTables.getWellLogTableList();
        for (IWellLogTable table : wellLogTableList) {
            System.err.print("table " + table.getTableName());
            Matrix matrix = table.getMatrix();
            System.err.println("\tmatrix " + matrix.getRowCount());
        }
    }
}
