package mbean;

/**
 * Created by Mikhailov_KG on 17.05.2018.
 */
public interface TestMBean {
    String getTest();
}
