package mbean;

import ru.navilab.jprime.crawler.utils.MBeanUtils;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

/**
 * Created by Mikhailov_KG on 17.05.2018.
 */

public class MBeanTestMain {
    public static void main(String[] args) throws Exception {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

        String objectName = MBeanUtils.getObjectName(Test.class);
        System.err.println("obj " + objectName);
        ObjectName name = new ObjectName(objectName);

        Test mbean = new Test();

        mbs.registerMBean(mbean, name);

        System.out.println("Waiting forever...");
        Thread.sleep(Integer.MAX_VALUE);
    }

}
