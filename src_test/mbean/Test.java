package mbean;

/**
 * Created by Mikhailov_KG on 17.05.2018.
 */
public class Test implements TestMBean {
    private String test;

    @Override
    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
