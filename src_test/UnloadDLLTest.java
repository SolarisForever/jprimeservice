import ru.navilab.jprime.loader.v2.CustomClassLoader;
import ru.navilab.jprime.loader.v2.IJniPrimeApi;
import ru.navilab.jprime.loader.v2.JniPrimeApi;

/**
 * Created by Mikhailov_KG on 17.05.2018.
 */
public class UnloadDLLTest {
    public static void main(String[] args) {
        try {
            CustomClassLoader customClassLoader = new CustomClassLoader();
            Class<?> aClass = customClassLoader.findClass(JniPrimeApi.class.getName());
            IJniPrimeApi api = (IJniPrimeApi) aClass.newInstance();
            api = null;
            aClass = null;
            customClassLoader = null;
            System.gc();
            Thread.sleep(1000);
            System.gc();
            Thread.sleep(1000);
            System.gc();
            customClassLoader = new CustomClassLoader();
            aClass = customClassLoader.findClass(JniPrimeApi.class.getName());
            api = (IJniPrimeApi) aClass.newInstance();
            Thread.sleep(10000);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
