import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.je.*;

import java.io.File;
import java.io.Serializable;

/**
 * Created by Mikhailov_KG on 29.06.2015.
 */
public class TestBDB {
    private static class TestData implements Serializable {
        private String data;
        private int count;

        public TestData(String data, int count) {
            super();
            this.data = data;
            this.count = count;
        }

        @Override
        public String toString() {
            return data + " " + count;
        }
    }

    public static void main(String[] args) {
        new TestBDB().test();
    }

    private Environment myEnv;
    private Database testDb;
    private Database classDb;
    private StoredClassCatalog storedClassCatalog;
    private SerialBinding serialBinding;

    void test() {
        DatabaseConfig myDbConfig = new DatabaseConfig();
        myDbConfig.setAllowCreate(true);
        myDbConfig.setDeferredWrite(true);

        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        myEnv = new Environment(new File("db"), envConfig);

        testDb = myEnv.openDatabase(null, "TestDB", myDbConfig);
        classDb = myEnv.openDatabase(null, "ClassDB", myDbConfig);
        storedClassCatalog = new StoredClassCatalog(classDb);
        serialBinding = new SerialBinding(storedClassCatalog, TestData.class);
        put();
        get();
        testDb.close();
        classDb.close();
        myEnv.close();
    }

    private void get() {
        DatabaseEntry key = new DatabaseEntry("1K".getBytes());
        DatabaseEntry data = new DatabaseEntry();
        testDb.get(null, key, data, LockMode.DEFAULT);
        TestData testData = (TestData) serialBinding.entryToObject(data);
        //String dataString = new String(data.getData());
        System.err.println("data is " + testData);
    }

    private void put() {
        DatabaseEntry key = new DatabaseEntry("1K".getBytes());
//		DatabaseEntry data = new DatabaseEntry("test".getBytes());
        DatabaseEntry data = new DatabaseEntry();
        serialBinding.objectToEntry(new TestData("1", 1), data);
        testDb.put(null, key, data);
    }
}