import ru.navilab.jprime.service.IFileDelegate;
import ru.navilab.jprime.service.PrimeReadException;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Mikhailov_KG on 20.01.2016.
 */
public class ScanTest {
    static FilenameFilter wsFilenameFilter = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.toUpperCase().endsWith(".WS");
        }
    };

    public static void main(String[] args) throws PrimeReadException, SQLException, IOException {
        new ScanTest().scanDir(new File("\\\\TONIPI-PRIME\\data2\\PrimeDB\\LogDB-1\\Ай-Пимское\\1020\\31601329\\"), new IFileDelegate() {
            @Override
            public void fileFound(String file) {
                System.err.println("file " + file);
            }
        });
    }
    private void scanDir(File file, IFileDelegate fileDelegate) throws PrimeReadException, SQLException, IOException {
        System.err.println(file);
        File[] files = file.listFiles(wsFilenameFilter);
        if (files != null) {
            for (File wsFile : files) {
                System.err.println(wsFile.getAbsolutePath());
                String absolutePath = wsFile.getAbsolutePath();
                fileDelegate.fileFound(absolutePath);
            }
        }
        File[] dirFiles = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return (pathname.isDirectory());
            }
        });
        System.err.println("dirFiles " + dirFiles + " " + file);
        if (dirFiles != null) {
            for (File dirFile : dirFiles) {
                System.err.println("dirFile " + dirFile);
                scanDir(dirFile, fileDelegate);
            }
        }
    }
}
