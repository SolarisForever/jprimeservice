import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  @author Kirill Mikhailov
 *  23.03.2020
 *
 * {  afldType   = (  0-Nodef, 1- _Byte, 2- _Short, 3-  _Word, 4-_LongInt, 5-_Single,
 * 6-_Double, 7-_String,8-  _Date, 9-  _Time,10-  _Array,11-_Object,                                              12-_StringC, 13-_Table,14-_User1,15- _User2,16-  _User3,17-  _Blob,
 * 18-_Boolean, 19-_Memo); }
 */
public class BinaryReader {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private static final Charset CP_866 = Charset.forName("CP866");
    private static final int FIRST_TABLE_OFFSET = 0x1a;
    private final RandomAccessFile raf;
    private byte[] stringBuffer = new byte[256];
    private int lastSeek;

    public BinaryReader(String file) throws FileNotFoundException {
        raf = new RandomAccessFile(file, "r");
    }

    public void readAllTables() throws IOException {
        int tableOffset = FIRST_TABLE_OFFSET;
        do {
            raf.seek(tableOffset);
            int nextTableOffset = readPascalInt();
            raf.skipBytes(7);
            String tableName = readString();
//            System.err.println((String.format("offset = %04x %s", tableOffset, tableName)));
            if (tableName.equals("LAS")
                    || tableName.equals("DESCRIPTION")
                    || tableName.equals("ДАННЫЕ_АРМГ")) {
                TableReader tableReader = new TableReader(raf, tableOffset, nextTableOffset);
                tableReader.readTable();
            }
            tableOffset = nextTableOffset;
        } while (tableOffset > 0);
    }

    private String readString() throws IOException {
        int strSize = raf.readByte();
        if (strSize <= 0) return null;
        int read = raf.read(stringBuffer, 0, strSize);
        if (read > 0) return new String(stringBuffer, 0, read, CP_866);
        else return null;
    }



    public static void main(String[] args) throws Exception {
        BinaryReader r1 = new BinaryReader(args[0]);
        Logger logger = Logger.getLogger(r1.getClass().getName());
        if (logger.isLoggable(Level.FINE)) logger.fine(String.format("f %04x", Float.floatToIntBits(123f)));
        r1.scanAllTables();
        r1.readAllTables();
    }

    private void scanAllTables() throws IOException {
        int offset = 0x1a;
        do {
            raf.seek(offset);
            offset = readPascalInt();
            raf.skipBytes(7);
            String table = readString();
            if (logger.isLoggable(Level.FINE)) {
                logger.fine(table);
                logger.fine(String.format("table %s offset = %04x", table, offset));
            }
        } while (offset > 0);
    }

    private int readPascalInt() throws IOException {
        int ch1 = raf.read();
        int ch2 = raf.read();
        int ch3 = raf.read();
        int ch4 = raf.read();
        if ((ch1 | ch2 | ch3 | ch4) < 0)
            throw new EOFException();
        return (ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0);
    }
}
