#include "ru_navilab_jprime_loader_v2_JniPrimeApi.h"
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <iconv.h>
#include <windows.h>

#define ERR -1000
#define ERR_HANDLE -1001
#define HANDLE "handle"

#define MaxNameLen 32
#define MnemoLen   8+1
#define FNameLen   12
#define ShortLen   20+1
#define LongLen    60+1
#define MaxPathLen 81

typedef char NameStr[MaxNameLen+1];
typedef char FNameStr[FNameLen+1];
typedef char ShortStr[ShortLen+1];
typedef char PathStr[MaxPathLen+1];
typedef char MnemoStr[MnemoLen+1];

typedef struct {
		NameStr fieldName;
    	unsigned char fieldType;
    	NameStr fieldUnit;
    	short length;
    	short dec;
    	MnemoStr min;
    	MnemoStr max;
    	unsigned short mask;
    	PathStr fieldComment;
} PCFDData;


/*__declspec (dllimport) short __stdcall SysLFInitOpen(const char *wsFileName, void *fileHandle);
//__declspec (dllimport) short __stdcall Get_Table(const char *tableName, void *fileHandle, void *tableHandle);
__declspec (dllimport) short __stdcall Get_TableNew(const char *tableName, void *fileHandle, void *tableHandle);
//__declspec (dllimport) short __cdecl LRGetObjCount(void *tableHandle, int *count);
//__declspec (dllimport) short __cdecl GetObjByIdN(void *tableHandle, unsigned short rowIndex, void *objHandle);
__declspec (dllimport) short __stdcall GetTDescCounter(void *objHandle, void *count);
//__declspec (dllimport) short __cdecl GetTDescFldsI(void *objHandle, short fieldNumber, void *data);
*/

short __stdcall SysLFInitOpen(const char *wsFileName, void *fileHandle);
short __stdcall Get_TableNew(const char *tableName, void *fileHandle, void *tableHandle);
short __stdcall GetTDescCounter(void *objHandle, void *count);
short __stdcall GetTDescFldsI(void *objHandle, short fieldNumber, void *data);
short __stdcall GetObjByIdN(void *tableHandle, unsigned short rowIndex, void *objHandle);
short __stdcall GetObValByIndex(short index, void *objHandle, void *objValueHandle);
short __stdcall ObValGetStrW(void *objHandle, void *str);
short __stdcall DoneHandle(void *objHandle);
short __stdcall ObValGetPointer(void *objValueHandle, byte **pointer);
long __stdcall MemoGetLineCount(void *objValueHandle);
long __stdcall MemoGetLineW(void *objValueHandle, int lineIndex, void *str, long strMaxLen);

void *fileHandle = NULL;
void *tableHandle = NULL;

void fromUTF16toCP866(const wchar_t *from, char *to, int sz) {
	memset(to, 0, sz);
	int len = wcslen(from);
	iconv_t ic = iconv_open("CP866", "UTF-16LE");
	if (ic == (iconv_t)-1) {
		if (errno != EINVAL) perror("iconv_open");
	} else {
		iconv(ic, (char **)&from, &len, &to, &sz);
		iconv_close(ic);
	}
}

void main(int argc, char **argv) {
	const char *str = "C:\\data\\prime\\40013542_05.06.2007_LOG.ws";
	printf("%s\n", str);
	void *fp = NULL;
	void *tp = NULL;
	short s = 0;
	printf("f1 %p %d\n", fp, (int)s);
	printf("f2 %p %d %s\n", tp, (int)s, str);

	s = SysLFInitOpen(str, &fp);
	printf("f1 %p %d\n", fp, (int)s);
	printf("f2 %p %d\n", tp, (int)s);

	s = Get_TableNew("LAS", fp, &tp);
	printf("t %p %d %s\n", tp, (int)s, str);
	if (s != 0) exit(0);

	unsigned short count = 0;
	s = GetTDescCounter(tp, &count);
	if (s != 0) exit(0);
	printf("c %p %hd %d\n", tp, count, (int)s);

	int i=0;
	for (i=0; i < count; i++) {
		PCFDData data;
        memset(&data, 0, sizeof(data));
		s = GetTDescFldsI(tp, i, &data);
		if (s != 0) exit(0);
		data.fieldName[32] = 0;
		printf("%d %s\n", i, data.fieldName);
	}

	void *rp;
	s = GetObjByIdN(tp, 1, &rp);
	printf("GetObjByIdN %hd %p\n", s, rp);
	if (s != 0) exit(0);
	{
		void *ovp;
		s = GetObValByIndex(3, rp, &ovp);
		printf("GetObValByIndex %hd %p\n", s, ovp);
		if (s != 0) exit(0);

		void *vstr = malloc(1024);
		s = ObValGetStrW(ovp, vstr);
		printf("ObValGetStrW %hd %p\n", s, vstr);
		if (s != 0) exit(0);

		int strSize = wcslen(vstr);
		char buf[1024];
		memset(buf, 0, sizeof(buf));
		fromUTF16toCP866(vstr, buf, sizeof(buf));
		printf("%s %d\n", buf, strSize);

		free(vstr);
		printf("DoneHandle %p\n", ovp);
		DoneHandle(&ovp);
	}
	/* ---- */
	{
		void *ovp = NULL;
		printf("f GetObValByIndex %hd %p\n", s, ovp);
		s = GetObValByIndex(17, rp, &ovp);
		printf("GetObValByIndex %hd %p\n", s, ovp);
		if (s != 0) exit(0);
		byte *p;
    	s = ObValGetPointer(ovp, &p);
    	int year = (p[3] << 8) + (p[2] & 0xFF);
    	printf("%d %d %d", (int)p[0], (int)p[1], year);
    	DoneHandle(&ovp);
    	DoneHandle(p);
    }
    {
		void *ovp = NULL;
		s = GetObValByIndex(32, rp, &ovp);
		printf("GetObValByIndex %hd %p\n", s, ovp);
		if (s != 0) exit(0);
		long lineCount = MemoGetLineCount(ovp);
		printf("line count %ld\n", lineCount);
		void *str = malloc(10240);
		for(i=0; i < lineCount; i++) {
//			long sz = MemoGetLineW(ovp, i, str, 10240);
//			char buf[10240];
//			memset(buf, 0, sizeof(buf));
//			fromUTF16toCP866(str, buf, sizeof(buf));
//			printf("%s %ld\n", buf, sz);
			long sz = MemoGetLineW(ovp, i, NULL, 0);
			printf("%d %ld\n", i, sz);
			if (sz > 0) {
//				void *str = malloc(sz);
//				sz = MemoGetLineW(ovp, i, str, sz);
//				if (sz == 0)
//				printf("ok\n"); //(int)wcslen(str));
//				free(str);
			}
		}
		free(str);
		DoneHandle(&ovp);
	}
}
