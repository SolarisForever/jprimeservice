#include "ru_navilab_jprime_loader_v2_JniPrimeApi.h"
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <iconv.h>
#include <windows.h>

#define ERR -1000
#define HANDLE "handle"

#define MaxNameLen 32
#define MnemoLen   8+1
#define FNameLen   12
#define ShortLen   20+1
#define LongLen    60+1
#define MaxPathLen 81

typedef char NameStr[MaxNameLen+1];
typedef char FNameStr[FNameLen+1];
typedef char ShortStr[ShortLen+1];
typedef char PathStr[MaxPathLen+1];
typedef char MnemoStr[MnemoLen+1];

typedef struct {
		NameStr fieldName;
    	unsigned char fieldType;
    	NameStr fieldUnit;
    	short length;
    	short dec;
    	MnemoStr min;
    	MnemoStr max;
    	unsigned short mask;
    	PathStr fieldComment;
    	char dummy[1024];
} PCFDData;

void nameStrCopy(char *to, char * from) {
	strncpy(to, from, MaxNameLen+1);
}

__declspec (dllimport) short __cdecl SysLFInitOpenW(const jchar *wsFileName, void *fileHandle);
__declspec (dllimport) short __cdecl TableExistW(const jchar *tableName, void *fileHandle);
__declspec (dllimport) short __cdecl Get_TableNewW(const jchar *tableName, void *fileHandle, void *tableHandle);
__declspec (dllimport) short __cdecl LRGetObjCount(void *tableHandle, int *count);
__declspec (dllimport) short __cdecl GetObjByIdN(void *tableHandle, unsigned short rowIndex, void *objHandle);
__declspec (dllimport) short __cdecl GetTDescCounter(void *objHandle, unsigned short *count);
__declspec (dllimport) short __cdecl GetTDescFldsIW(void *objHandle, unsigned short fieldNumber, PCFDData *pcfdData);

void* get_handle(JNIEnv *env, jobject handle);
void set_handle(JNIEnv *env, jobject handle, const void *p);

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_SysLFInitOpen
  (JNIEnv *env, jobject obj, jstring wsFileName, jobject handle) {
    void *p;
	const jchar *filename = (*env)->GetStringChars(env, wsFileName, JNI_FALSE);
	if (filename == NULL) return ERR;
	short status = SysLFInitOpenW(filename, &p);
	printf("native call complete %d, %p\n", (int)status, p);
	jobject fh = (*env)->NewGlobalRef(env, handle);
	set_handle(env, fh, p);
	(*env)->DeleteGlobalRef(env, fh);
	printf("native call complete %d, %p\n", (int)status, p);
	(*env)->ReleaseStringChars(env, wsFileName, filename);
    return status;
}

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_TableExist
  (JNIEnv *env, jobject obj, jobject handle, jstring tableName) {

  	jobject gh = (*env)->NewGlobalRef(env, handle);
 	void *p = get_handle(env, gh);
 	(*env)->DeleteGlobalRef(env, gh);

 	printf("native call TableExist  %p\n",  p);
 	if (p == NULL) return ERR;
 	const jchar *tableNameStr = (*env)->GetStringChars(env, tableName, JNI_FALSE);
 	if (tableNameStr == NULL) return ERR;
	short status = TableExistW(tableNameStr, p);
	(*env)->ReleaseStringChars(env, tableName, tableNameStr);
  	return status;
}

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_GetTableNew
  (JNIEnv *env, jobject obj, jobject fileHandle, jstring tableName, jobject tableHandle)
{
	jobject gh = (*env)->NewGlobalRef(env, fileHandle);
 	void *filePtr = get_handle(env, gh);
 	(*env)->DeleteGlobalRef(env, gh);

	if (filePtr == NULL) return ERR;
	printf("native call GetTableNew  fp %p\n", filePtr);

	const jchar *tableNameStr = (*env)->GetStringChars(env, tableName, JNI_FALSE);
	if (tableNameStr == NULL) return ERR;
	void *tablePtr = NULL;
	short status = 0;
	printf("native call GetTableNew  %p / %d\n", tablePtr, (int)status);
	status = Get_TableNewW(tableNameStr, filePtr, &tablePtr);
	printf("native call GetTableNew  %p / %d\n", tablePtr, (int)status);
	jobject th = (*env)->NewGlobalRef(env, tableHandle);
	set_handle(env, th, tablePtr);
	(*env)->DeleteGlobalRef(env, th);
	printf("native call GetTableNew  %p - %d\n", tablePtr, (int)status);
	(*env)->ReleaseStringChars(env, tableName, tableNameStr);
}

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_LRGetObjCount
  (JNIEnv *env, jobject obj, jobject tableHandle)
{
	jobject gh = (*env)->NewGlobalRef(env, tableHandle);
    void *tablePtr = get_handle(env, gh);
	(*env)->DeleteGlobalRef(env, gh);

	if (tablePtr == NULL) return ERR;
	int count;
	printf("native call LRGetObjCount  %p / %d\n", tablePtr, count);

	short status = LRGetObjCount(tablePtr, &count);
	printf("native call LRGetObjCount  %p %d\n", tablePtr, count);

	if (status != 0) return -abs(status);
	else return count;
}

void fromCP866(const char *from, int sz1, char *to, int sz2) {
	iconv_t ic = iconv_open("UTF-8", "CP866");
	if (ic != (iconv_t)-1) {
		int len = sz1;
		int len_out = sz2;
		int k = iconv(ic, &from, &len, &to, &len_out);
		printf("k=%d\n", k);
		*to = '\0';
		iconv_close(ic);
	}
}

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_getFieldDescriptions
  (JNIEnv *env, jobject obj, jobject tableHandle, jobject callback)
{
	jobject gh = (*env)->NewGlobalRef(env, tableHandle);
	void *tablePtr = get_handle(env, gh);
	(*env)->DeleteGlobalRef(env, gh);
	if (tablePtr == NULL) return ERR;
	void *objPtr;
	short status1 = GetObjByIdN(tablePtr, 1, &objPtr);
	if (status1 != 0) return ERR;
	unsigned short count;
	int status2 = GetTDescCounter(objPtr, &count);
	if (status2 != 0) return status2;
	printf("zcount = %p %d %d %d\n", objPtr, (int)0, (int)status2, (int)count);


//	char str[MaxNameLen+1];
	int i;
	//char *str2 = malloc(1024);
	for (i=1; i < count; i++) {
		PCFDData *data = malloc(4096);
		int status3 = GetTDescFldsIW(objPtr, i, &data);
		if (status3 != 0) {
			printf("status = %d", status3);
			return status3;
		}
		//strncpy(str, data.fieldName, MaxNameLen+1);
		//fromCP866(data.fieldName, MaxNameLen, str2, 255);
		printf("%d\n", i);
		//printf("data.name %d %s\n", i, str2);
		//free(data);
	}
	printf("end\n");
}



/* ---------------------------------------------------------------------------------------------- */

void* get_handle(JNIEnv *env, jobject handle) {
	jclass handleClass = (*env)->GetObjectClass(env, handle);
	if (handleClass == NULL) return NULL;
	jfieldID handleID = (*env)->GetFieldID(env, handleClass, HANDLE, "J");
	if (handleID == NULL) return NULL;
	intptr_t v = (*env)->GetLongField(env, handle, handleID);
	return (void*)v;
}

void set_handle(JNIEnv *env, jobject handle, const void *p) {
	jclass handleClass = (*env)->GetObjectClass(env, handle);
	//if (handleClass == NULL) return;
	jfieldID handleID = (*env)->GetFieldID(env, handleClass, HANDLE, "J");
	//if (handleID == NULL) return;
	jlong longValue = p;
	(*env)->SetLongField(env, handle, handleID, longValue);
}

void main(int argc, char **argv) {

}
