#include "ru_navilab_jprime_loader_v2_JniPrimeApi.h"
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <iconv.h>
#include <windows.h>

#define ERR -1000
#define ERR_HANDLE -1001
#define ERR_FLOAT -9999999
#define ERR_LONG  -9999999
#define HANDLE "handle"

#define UNICODE_BUFFER_SIZE 1024

#define MaxNameLen 32
#define MnemoLen   8+1
#define FNameLen   12
#define ShortLen   20+1
#define LongLen    60+1
#define MaxPathLen 81

typedef char NameStr[MaxNameLen+1];
typedef char FNameStr[FNameLen+1];
typedef char ShortStr[ShortLen+1];
typedef char PathStr[MaxPathLen+1];
typedef char MnemoStr[MnemoLen+1];

typedef struct {
		NameStr fieldName;
    	unsigned char fieldType;
    	NameStr fieldUnit;
    	short length;
    	short decimals;
    	MnemoStr min;
    	MnemoStr max;
    	unsigned char mask;
    	PathStr fieldComment;
} PCFDData;

void nameStrCopy(char *to, char * from) {
	strncpy(to, from, MaxNameLen+1);
}

short __stdcall SysLFInitOpenW(const jchar *wsFileName, void **fileHandle);
short __stdcall TableExistW(const jchar *tableName, void *fileHandle);
short __stdcall Get_TableNewW(const jchar *tableName, void *fileHandle, void **tableHandle);
short __stdcall LRGetObjCount(void *tableHandle, int *count);
short __stdcall GetObjByIdN(void *tableHandle, unsigned short rowIndex, void **objHandle);
short __stdcall GetTDescCounter(void *objHandle, unsigned short *count);
short __stdcall GetTDescFldsI(void *objHandle, short fieldNumber, PCFDData *data);
short __stdcall GetObValByIndex(short index, void *objHandle, void **objValueHandle);
short __stdcall ObValGetStrW(void *objHandle, void *str);
short __stdcall DoneHandle(void *objHandle);
short __stdcall ObValGetPointer(void *objValueHandle, byte **pointer);
long __stdcall ObValGetLongInt(void *objValueHandle);
float __stdcall ObValGetSingle(void *objValueHandle);
long __stdcall MemoGetLineCount(void *objValueHandle);
long __stdcall MemoGetLineW(void *objValueHandle, int lineIndex, void *str, long strMaxLen);
short __stdcall GetArrayByIndex(void *rowHandle, unsigned short columnIndex, void **arrayHandle);
short __stdcall GetArrayByNumber(void *rowHandle, unsigned short columnIndex, void **arrayHandle);
unsigned long __stdcall ArrayGetLen(void *arrayHandle);
short __stdcall ArrayGetSingleColData2(void *arrayHandle, unsigned short columnIndex, unsigned long rowIndex,
	unsigned short count, float *floatArray);
short __stdcall ArrayGetStrColumnData2W(void *arrayHandle, unsigned short columnIndex,
	unsigned long rowIndex, void *str);


/* ---------------------------------------------------------------------------------------------- */

void* get_handle(JNIEnv *env, jobject handle) {
	jclass handleClass = (*env)->GetObjectClass(env, handle);
	if (handleClass == NULL) return NULL;
	jfieldID handleID = (*env)->GetFieldID(env, handleClass, HANDLE, "J");
	if (handleID == NULL) return NULL;
	intptr_t v = (*env)->GetLongField(env, handle, handleID);
	return (void*)v;
}

void set_handle(JNIEnv *env, jobject handle, const void *p) {
	jclass handleClass = (*env)->GetObjectClass(env, handle);
	if (handleClass == NULL) return;
	jfieldID handleID = (*env)->GetFieldID(env, handleClass, HANDLE, "J");
	if (handleID == NULL) return;
	jlong longValue = (jlong)p;
	(*env)->SetLongField(env, handle, handleID, longValue);
}

/* ---------------------------------------------------------------------------------------------- */

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_SysLFInitOpen
  (JNIEnv *env, jobject obj, jstring wsFileName, jobject handle) {
	const jchar *filename = (*env)->GetStringChars(env, wsFileName, JNI_FALSE);
	if (filename == NULL) return ERR;
	void *fileHandle;
	short status = SysLFInitOpenW(filename, &fileHandle);
	set_handle(env, handle, fileHandle);
	(*env)->ReleaseStringChars(env, wsFileName, filename);
    return status;
}

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_TableExist
  (JNIEnv *env, jobject obj, jobject handleObj, jstring tableName) {
  	void *fileHandle = get_handle(env, handleObj);
	if (fileHandle == NULL) return ERR_HANDLE;
 	const jchar *tableNameStr = (*env)->GetStringChars(env, tableName, JNI_FALSE);
 	if (tableNameStr == NULL) return ERR;
	short status = TableExistW(tableNameStr, fileHandle);
	(*env)->ReleaseStringChars(env, tableName, tableNameStr);
  	return status;
}

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_GetTableNew
  (JNIEnv *env, jobject obj, jobject fileHandleObj, jstring tableName, jobject tableHandleObj)
{
	void *fileHandle = get_handle(env, fileHandleObj);
    if (fileHandle == NULL) return ERR_HANDLE;

	const jchar *tableNameStr = (*env)->GetStringChars(env, tableName, JNI_FALSE);
	if (tableNameStr == NULL) return ERR;
	void *tableHandle;
	short status = Get_TableNewW(tableNameStr, fileHandle, &tableHandle);
	set_handle(env, tableHandleObj, tableHandle);
	(*env)->ReleaseStringChars(env, tableName, tableNameStr);
	return status;
}

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_LRGetObjCount
  (JNIEnv *env, jobject obj, jobject tableHandleObj)
{
	void *tableHandle = get_handle(env, tableHandleObj);
	if (tableHandle == NULL) return ERR_HANDLE;
	int count = 0;
	short status = LRGetObjCount(tableHandle, &count);

	if (status != 0) return -abs(status);
	else return count;
}

void fromCP866toUTF8(char *from, char *to, int sz) {
	memset(to, 0, sz);
	int len = strlen(from);
	iconv_t ic = iconv_open("UTF-8", "CP866");
	if (ic == (iconv_t)-1) {
		if (errno != EINVAL) perror("iconv_open");
	} else {
		iconv(ic, &from, &len, &to, &sz);
		iconv_close(ic);
	}
}

jstring newUTFString(JNIEnv *env, char *s) {
	char buf[255];
	fromCP866toUTF8(s, buf, sizeof(buf));
	return (*env)->NewStringUTF(env, buf);
}

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_getFieldDescriptions
  (JNIEnv *env, jobject obj, jobject tableHandleObj, jobject callback)
{
	void *tableHandle = get_handle(env, tableHandleObj);
	if (tableHandle == NULL) return ERR_HANDLE;
	unsigned short count;
	short s = GetTDescCounter(tableHandle, &count);
	if (s != 0) return s;

	jclass cbClass = (*env)->GetObjectClass(env, callback);
	if (cbClass == NULL) return ERR;

	jmethodID callbackMethod = (*env)->GetMethodID(env, cbClass, "add",
		"(Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V");
	if (callbackMethod == NULL) return ERR;
	int i=0;
	for (i=0; i < count; i++) {
		PCFDData data;
		memset(&data, 0, sizeof(data));
		s = GetTDescFldsI(tableHandle, i, &data);
		if (s != 0) return s;

		jstring fieldName = newUTFString(env, data.fieldName);
		jint fieldType = data.fieldType;
		jstring fieldUnit = newUTFString(env, data.fieldUnit);
		jint length = data.length;
		jint decimals = data.decimals;
		jstring fieldComment = newUTFString(env, data.fieldComment);
		(*env)->CallVoidMethod(env, callback, callbackMethod, fieldName, fieldType, fieldUnit, length, decimals, fieldComment);
	}
	return 0;
}

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_openRow
  (JNIEnv *env, jobject obj, jobject tableHandleObj, jint rowIndex, jobject rowHandleObj)
{
	void *tableHandle = get_handle(env, tableHandleObj);
	if (tableHandle == NULL) return ERR_HANDLE;
	void *rowHandle;
	short s = GetObjByIdN(tableHandle, rowIndex, &rowHandle);
	set_handle(env, rowHandleObj, rowHandle);
	if (s != 0) return s;
	return 0;
}

JNIEXPORT jstring JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_getStringNative
  (JNIEnv *env, jobject obj, jobject rowHandleObj, jint columnIndex)
{
	void *rowHandle = get_handle(env, rowHandleObj);
	if (rowHandle == NULL) return NULL;
	void *objectValueHandle = NULL;
	void *unicodeStringBuffer = malloc(UNICODE_BUFFER_SIZE);

	short s = GetObValByIndex(columnIndex, rowHandle, &objectValueHandle);
	if (s != 0) return NULL;
	s = ObValGetStrW(objectValueHandle, unicodeStringBuffer);
	if (s != 0) return NULL;

	jsize sz = (jsize)wcslen(unicodeStringBuffer);
	jstring jstr = (*env)->NewString(env, (const jchar *)unicodeStringBuffer, sz);
	free(unicodeStringBuffer);
	DoneHandle(&objectValueHandle);
	return jstr;
}

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_doneHandle
  (JNIEnv *env, jobject obj, jobject handleObj)
{
	void *handle = get_handle(env, handleObj);
	if (handle != NULL) return DoneHandle(&handle);
	else return ERR;
}

JNIEXPORT jbyteArray JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_getDateTime4b
  (JNIEnv *env, jobject obj, jobject rowHandleObj, jint columnIndex)
{
	void *rowHandle = get_handle(env, rowHandleObj);
	if (rowHandle == NULL) return NULL;
	void *objectValueHandle = NULL;
	short s = GetObValByIndex(columnIndex, rowHandle, &objectValueHandle);
	if (s != 0) return NULL;
	byte *p;
	s = ObValGetPointer(objectValueHandle, &p);
	jbyteArray byteArray = (*env)->NewByteArray(env, 4);
	(*env)->SetByteArrayRegion(env, byteArray, 0, 4, p);
	DoneHandle(&objectValueHandle);
	DoneHandle(p);
	return byteArray;
}

JNIEXPORT jlong JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_getLong
  (JNIEnv *env, jobject obj, jobject rowHandleObj, jint columnIndex)
{
	void *rowHandle = get_handle(env, rowHandleObj);
	if (rowHandle == NULL) return ERR_HANDLE;
	void *objectValueHandle = NULL;
	short s = GetObValByIndex(columnIndex, rowHandle, &objectValueHandle);
	if (s != 0) return ERR_LONG;
	long l = ObValGetLongInt(objectValueHandle);
	DoneHandle(&objectValueHandle);
	return l;
}

JNIEXPORT jfloat JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_getFloat
  (JNIEnv *env, jobject obj, jobject rowHandleObj, jint columnIndex)
{
	void *rowHandle = get_handle(env, rowHandleObj);
	if (rowHandle == NULL) return ERR_HANDLE;
	void *objectValueHandle = NULL;
	short s = GetObValByIndex(columnIndex, rowHandle, &objectValueHandle);
	if (s != 0) return ERR_FLOAT;
	float f = ObValGetSingle(objectValueHandle);
	DoneHandle(&objectValueHandle);
	return f;
}

JNIEXPORT jobjectArray JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_getMemo
  (JNIEnv *env, jobject obj, jobject rowHandleObj, jint columnIndex)
{
	void *rowHandle = get_handle(env, rowHandleObj);
	if (rowHandle == NULL) return NULL;
	void *objectValueHandle = NULL;
	short s = GetObValByIndex(columnIndex, rowHandle, &objectValueHandle);
	if (s != 0) return NULL;

	long lineCount = MemoGetLineCount(objectValueHandle);
	if (lineCount < 0) {
		DoneHandle(&objectValueHandle);
		return NULL;
	}

	jclass stringClass = (*env)->FindClass(env, "java/lang/String");
	if (stringClass == NULL) {
		DoneHandle(&objectValueHandle);
		return NULL;
	}

	jobjectArray resultArray = (*env)->NewObjectArray(env, lineCount, stringClass, NULL);
	if (resultArray == NULL) {
		DoneHandle(&objectValueHandle);
		return NULL;
	}

	int i=0;
	void *str = malloc(UNICODE_BUFFER_SIZE);
	for(i=0; i < lineCount; i++) {
		long sz = MemoGetLineW(objectValueHandle, i, str, UNICODE_BUFFER_SIZE);
		if (sz == 0) {
			jsize strSize = (jsize)wcslen(str);
            jstring jstr = (*env)->NewString(env, (const jchar *)str, strSize);
            (*env)->SetObjectArrayElement(env, resultArray, i, jstr);
		} else {
			jstring jstr = (*env)->NewStringUTF(env, "");
			(*env)->SetObjectArrayElement(env, resultArray, i, jstr);
		}
	}
	free(str);

	DoneHandle(&objectValueHandle);
	return resultArray;
}

JNIEXPORT jint JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_openArray
  (JNIEnv *env, jobject obj, jobject rowHandleObj, jint columnIndex, jobject arrayHandleObj)
{
	void *rowHandle = get_handle(env, rowHandleObj);
	if (rowHandle == NULL) return ERR_HANDLE;
	void *arrayHandle = NULL;
	short s = GetArrayByIndex(&arrayHandle, columnIndex, rowHandle);
	set_handle(env, arrayHandleObj, arrayHandle);
	if (s != 0) return s;
	return 0;
}

JNIEXPORT jfloatArray JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_getArrayFloat
  (JNIEnv *env, jobject obj, jobject arrayHandleObj, jint columnArrayIndex)
{
	void *arrayHandle = get_handle(env, arrayHandleObj);
	if (arrayHandle == NULL) return NULL;
	unsigned long totalCount = ArrayGetLen(arrayHandle);
	unsigned short count = totalCount & 0xFFFF;
	float *floatArray = malloc(sizeof(float) * count);
	if (floatArray == NULL) return NULL;
	short s = ArrayGetSingleColData2(arrayHandle, columnArrayIndex, 0, count, floatArray);
	if (s != 0) {
		free(floatArray);
		return NULL;
	}
	jfloatArray jfloatArr = (*env)->NewFloatArray(env, count);
    (*env)->SetFloatArrayRegion(env, jfloatArr, 0, count, floatArray);
    free(floatArray);
	return jfloatArr;
}

JNIEXPORT jobjectArray JNICALL Java_ru_navilab_jprime_loader_v2_JniPrimeApi_getArrayString
  (JNIEnv *env, jobject obj, jobject arrayHandleObj, jint columnArrayIndex)
{
	void *arrayHandle = get_handle(env, arrayHandleObj);
	if (arrayHandle == NULL) return NULL;

	unsigned long totalCount = ArrayGetLen(arrayHandle);
	unsigned short count = totalCount & 0xFFFF;

	jclass stringClass = (*env)->FindClass(env, "java/lang/String");
    if (stringClass == NULL) return NULL;

	jobjectArray resultArray = (*env)->NewObjectArray(env, count, stringClass, NULL);
	if (resultArray == NULL) return NULL;
	int i=0;
	void *str = malloc(UNICODE_BUFFER_SIZE);
	for (;i < count; i++) {
		short s = ArrayGetStrColumnData2W(arrayHandle, columnArrayIndex, i, str);
		if (s != 0) return NULL;
		jsize strSize = (jsize)wcslen(str);
		jstring jstr = (*env)->NewString(env, (const jchar *)str, strSize);
		if (jstr == NULL) {
			free(str);
			return NULL;
		}
		(*env)->SetObjectArrayElement(env, resultArray, i, jstr);
	}
	free(str);
	return resultArray;
}

/*
JNIEXPORT void JNICALL
JNI_OnUnload(JavaVM *vm, void *reserved) {
	printf("OnUnLoad2\n");
}

JNIEXPORT jint JNICALL
JNI_OnLoad(JavaVM *vm, void *reserved) {
    printf("OnLoad2\n");

    return JNI_VERSION_1_1;
}
*/


void main(int argc, char **argv) {}
